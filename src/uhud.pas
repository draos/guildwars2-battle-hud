 {
   Created by: draos.9574

   Implements the main window which controls the function of the overlay.
 }

unit uHUD;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, Menus, uOverlay, uOption, uButtons, uTypes, IniFiles,
  uData, Windows, uReminder, uTs3View, LCLType;

type

  { THUD }
  THUD               = class(TForm)
    ApiKey: TEdit;
    LabelReminderVolume:TLabel;
    MapBack            :TPaintBox;
    MapBox             :TPanel;
    Timer              :TTimer;
    TrackReminderVolume:TTrackBar;
    VisibleBack        :TPaintBox;
    VisibleBox         :TPanel;
    ServerList         :TComboBox;

    LabelCompassScale,
    LabelCompassSize   :TLabel;

    ColorBack,
    SettingBack,
    NaviBack,
    OptionBack,
    TimerBack,
    TopBack            :TPaintBox;

    ColorBox,
    SettingBox,
    NaviBar,
    OptionBox,
    TimerBox,
    TopPanel           :TPanel;

    TrackCompassScale,
    TrackCompassSize   :TTrackBar;

    // Event handlers
    procedure        BackPaint(Sender:TObject);
    procedure        ColorChange(Sender:TObject);
    procedure        ControlClick(Sender:TObject);
    procedure        SettingsChange(Sender:TObject);
    procedure        DragMouseLeave(Sender:TObject);
    procedure        DragMouseMove(Sender:TObject;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);
    procedure        DragMouseUp(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);
    procedure        FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure        FormCreate(Sender:TObject);
    procedure        FormDestroy(Sender:TObject);
    procedure        LanguageChange(Sender:TObject);
    procedure        MainChange(Sender:TObject);
    procedure        MapChange(Sender:TObject);
    procedure        ServerListChange(Sender: TObject);
    procedure        TimerChange(Sender:TObject);
    procedure        DragMouseDown(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;X,Y:Integer);
    procedure        TimerTimer(Sender: TObject);
    procedure        TopBackPaint(Sender:TObject);
    procedure        VisibleChange(Sender:TObject);
  private
    // The used buttons
    // Buttons to change the colors
    FColorBtn        :Array[0..2] of TColorButton;
    // Buttons to control the visible panel or HUD actions
    FIconButton      :Array[TImageType] of TImageButton;
    // Buttons to set basic options like language or enabling the help system
    FOptionBtn       :Array[0..4] of TOwnButton;
    // Buttons to set the visibility of different structure types
    FTimerBtn        :Array[0..5] of TOwnButton;
    // Controls the visibility of the different plugins
    FVisibleBtn      :Array[0..5] of TOwnButton;
    // Controls the visibility of the different 2D wvw maps
    FMapBtn          :Array[0..4] of TOwnButton;
    // Buttons to change some settings
    FSetBtn          :Array[0..1] of TOwnButton;

    // Plugin structures
    FOverlay         :TOverlay;
    FReminder        :TReminder;
    FTs3Viewer       :TTs3Viewer;

    // Used for the mouse dragging
    FMove            :record
      X,Y            :Integer;
      Drag           :Boolean;
    end;

    // Monitor metrics
    FSize            :record
      Width, Height  :Integer;
    end;

    // Procedures
    procedure        InitButtons;
    procedure        InitPanels;
    procedure        ReleaseButtons;
    procedure        SaveConfig;
    procedure        LoadConfig;
  end;


var
  HUD                : THUD;

implementation

{$R *.lfm}

{ THUD }

// Paint procedure for the Gradients
procedure THUD.BackPaint(Sender: TObject);
var rect  :TRect;
begin
  if Sender is TPaintBox then
    with Sender as TPaintBox do begin
      rect.Top:=0;
      rect.Left:=0;
      rect.Right:=Width;
      rect.Bottom:=Height;
      Canvas.GradientFill(rect, Colors.Back, Colors.Front, gdVertical);
    end;
end;

// Procedure for changing Colors
procedure THUD.ColorChange(Sender:TObject);
 procedure UpdateColors;
 begin
   FColorBtn[0].Color:=Colors.Back;
   FColorBtn[1].Color:=Colors.Front;
   FColorBtn[2].Color:=Colors.Font;

   // Update Label Colors
   LabelCompassSize.Font.Color:=Colors.Font;
   LabelCompassScale.Font.Color:=Colors.Font;
   LabelReminderVolume.Font.Color:=Colors.Font;
   FReminder.Reload;
   Refresh;
 end;

begin
  // IF Sender = Main Window, then update with default colors
  if Sender = Self then
    UpdateColors
  else begin
    with TColorDialog.Create(Self) do begin
      if Execute then begin
        if Sender = FColorBtn[0] then
          Colors.Back:=Color and $FEFEFE
        else if Sender = FColorBtn[1] then
          Colors.Front:=Color and $FEFEFE
        else if Sender = FColorBtn[2] then
          Colors.Font:=Color and $FEFEFE;
      end;
      Free;
    end;

    UpdateColors;
  end;
end;

// Procedure for the main controls like close, minimize and maximize
procedure THUD.ControlClick(Sender:TObject);
var x     :Integer;
begin
  if Sender = FIconButton[IT_EXIT] then begin
    FOverlay.Shutdown;
    Close;
  end else if (Sender = FIconButton[IT_MIN])or(Sender = FIconButton[IT_MAX]) then begin
    // Just swap sizes
    x:=FSize.Height;
    FSize.Height:=Height;
    Height:=x;
    x:=FSize.Width;
    FSize.Width:=Width;
    Width:=x;

    // Update button status
    FIconButton[IT_MIN].Visible:=Sender <> FIconButton[IT_MIN];
    FIconButton[IT_MAX].Visible:=Sender <> FIconButton[IT_MAX];
  end;
end;

// Implements a moving form
procedure THUD.DragMouseLeave(Sender: TObject);
begin
  FMove.Drag:=False;
end;

procedure THUD.DragMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if FMove.Drag then
    SetBounds(Left+X-FMove.X, Top+Y-FMove.Y, ClientWidth, ClientHeight);
end;

procedure THUD.DragMouseDown(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  FMove.Drag:=True;
  FMove.X:=X;
  FMove.Y:=Y;
end;

procedure THUD.TimerTimer(Sender: TObject);
var s     :TStrings;
begin
  s:=ServerList.Items;
  FOverlay.WebAPI.listWorldNames(s, Language);

  ServerList.Text:=FOverlay.WebAPI.WorldName[FOverlay.WebAPI.Server, Language];

  Timer.Enabled:=ServerList.Items.Count = 0;
end;

procedure THUD.DragMouseUp(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  FMove.Drag:=False;
end;

procedure THUD.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // Save settings to config.ini
  SaveConfig;
end;

// Initialize the Form
procedure THUD.FormCreate(Sender: TObject);
  // Get the maximal Size of all boxes
  function maxHeight:Integer;
  begin
    Result:=TopPanel.Height+5*NaviBar.Width+25;
    // Add other boxes
    if Result < OptionBox.Height then
      Result:=OptionBox.Height;
    if Result < ColorBox.Height then
      Result:=ColorBox.Height;
    if Result < SettingBox.Height then
      Result:=SettingBox.Height;
     if Result < TimerBox.Height then
      Result:=TimerBox.Height;
    if Result < VisibleBox.Height then
      Result:=VisibleBox.Height;
    if Result < MapBox.Height then
      Result:=MapBox.Height;
    if Result < 6*NaviBar.Width+30 then
      Result:=6*NaviBar.Width+30;
  end;

begin
  // Try to load the logger if wished as early as possible
  if FileExists('Config.ini') then
    with TIniFile.Create('Config.ini') do begin
      Logger.Enable:=ReadBool('Option', 'Log', False);
      Free;
    end;

  // Create TOverlay and show
  FOverlay:=TOverlay.Create(Self);
  FOverlay.Show;

  // Create the reminder
  FReminder:=TReminder.Create(Self);

  // Create the ts3 viewer
  FTs3Viewer:=TTs3Viewer.Create(Self);

  // Set the notifications
  Notifier.Proc:=@(FOverlay.Notify);
  Notifier.Duration:=10;

  // Set the window size and the secondary sizes for swaps
  Width:=NaviBar.Width+OptionBox.Width;
  Height:=TopPanel.Height+maxHeight;
  FSize.Width:=Width;
  FSize.Height:=TopPanel.Height;

  // Set the HUD position to the middle of the right border
  Left:=GetSystemMetrics(SM_CXSCREEN)-Width;
  Top:=(GetSystemMetrics(SM_CYSCREEN)-Height) div 2;

  FMove.Drag:=False;

  // Initialize buttons and panels
  InitButtons;
  InitPanels;

  // Load config.ini if exists
  try
    LoadConfig;
  finally
  end;

  // Set default settings
  ColorChange(Self);
  SettingsChange(Self);
  LanguageChange(Self);
  MapChange(Self);
  TimerChange(Self);
  VisibleChange(Self);

  FOverlay.Reload;
  Timer.Enabled:=True;

  Logger.log('Creates hud.');
end;

// Destructor
procedure THUD.FormDestroy(Sender: TObject);
begin
  // Release TServerList and TOverlay
  try
    FreeAndNil(FOverlay);
    FreeAndNil(FReminder);
  except
    Logger.log('Failure in freeing hud interfaces.')
  end;

  ReleaseButtons;
  Logger.log('Freed hud.');
end;

// Procedure for language change
procedure THUD.LanguageChange(Sender:TObject);
var i     :Integer;
begin
  if Sender = FOptionBtn[0] then
    Language:=L_GERMAN
  else if Sender = FOptionBtn[1] then
    Language:=L_ENGLISH
  else if Sender = FOptionBtn[2] then
    Language:=L_FRENCH
  else if Sender = FOptionBtn[3] then
    Language:=L_SPANISH;

  // Activate the button for the selected language
  FOptionBtn[0].Selected:=Language=L_GERMAN;
  FOptionBtn[1].Selected:=Language=L_ENGLISH;
  FOptionBtn[2].Selected:=Language=L_FRENCH;
  FOptionBtn[3].Selected:=Language=L_SPANISH;

  // Set all HUDString of Labels
  LabelCompassScale.Caption:=HUDString[STRING_COMPASS][Language];
  LabelCompassSize.Caption:=HUDString[STRING_COMPASS+1][Language];
  LabelReminderVolume.Caption:=HUDString[STRING_COMPASS+2][Language];

  // Set the HUDHints
  FIconButton[IT_OPTION].Hint:=HUDHints[1][Language];
  FIconButton[IT_COLOR].Hint:=HUDHints[2][Language];
  FIconButton[IT_VISIBLE].Hint:=HUDHints[3][Language];
  FIconButton[IT_SETTINGS].Hint:=HUDHints[4][Language];
  FIconButton[IT_TIMER].Hint:=HUDHints[5][Language];
  FIconButton[IT_MAP].Hint:=HUDHints[25][Language];

  for i:=0 to 6 do
    FOptionBtn[i].Hint:=HUDHints[7+i][Language];
  TrackCompassScale.Hint:=HUDHints[17][Language];
  TrackCompassSize.Hint:=HUDHints[18][Language];

  for i:=0 to 5 do
    FTimerBtn[i].Hint:=HUDHints[19+i][Language];

  for i:=0 to 5 do
    FVisibleBtn[i].Hint:=HUDHints[11+i][Language];

  ServerList.Hint:=HUDHints[6][Language];

  TimerTimer(Self);

  FOverlay.Reload;
  FReminder.Reload;

  Refresh;
end;

// Change the visible main boxes
procedure THUD.MainChange(Sender:TObject);
begin
  OptionBox.Visible:=Sender = FIconButton[IT_OPTION];
  ColorBox.Visible:=Sender = FIconButton[IT_COLOR];
  SettingBox.Visible:=Sender = FIconButton[IT_SETTINGS];
  TimerBox.Visible:=Sender = FIconButton[IT_TIMER];
  VisibleBox.Visible:=Sender = FIconButton[IT_VISIBLE];
  MapBox.Visible:=Sender = FIconButton[IT_MAP];
  Refresh;
end;

// Paints the top panel
procedure THUD.TopBackPaint(Sender: TObject);
var cx, cy       :Integer;
begin
  with TopBack do begin
    Color:=Colors.Back;
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Brush.Style:=bsSolid;
    Canvas.Pen.Color:=Colors.Back;
    Canvas.Font.Color:=Colors.Font;
    Canvas.Rectangle(0, 0, Width, Height);

    cx:=0;cy:=0;
    Canvas.GetTextSize(TITLE, cx, cy);
    Canvas.TextOut((Width-cx)div 2, (Height-cy)div 2, TITLE);
  end;
end;

// Change visible timer of wvw structures
procedure THUD.TimerChange(Sender:TObject);
begin
  if Sender = FTimerBtn[0] then
    ForceVisibleObjects:=FTimerBtn[0].Selected

  else if Sender = FTimerBtn[1] then
    FOverlay.Structures[AOT_RUINS]:=FTimerBtn[1].Selected

  else if Sender = FTimerBtn[2] then
    FOverlay.Structures[AOT_CAMP]:=FTimerBtn[2].Selected

  else if Sender = FTimerBtn[3] then
    FOverlay.Structures[AOT_TOWER]:=FTimerBtn[3].Selected

  else if Sender = FTimerBtn[4] then
    FOverlay.Structures[AOT_KEEP]:=FTimerBtn[4].Selected

  else if Sender = FTimerBtn[5] then
    FOverlay.Structures[AOT_CASTLE]:=FTimerBtn[5].Selected

  else if Sender = Self then begin
    FTimerBtn[0].Selected:=ForceVisibleObjects;
    FTimerBtn[1].Selected:=FOverlay.Structures[AOT_RUINS];
    FTimerBtn[2].Selected:=FOverlay.Structures[AOT_CAMP];
    FTimerBtn[3].Selected:=FOverlay.Structures[AOT_TOWER];
    FTimerBtn[4].Selected:=FOverlay.Structures[AOT_KEEP];
    FTimerBtn[5].Selected:=FOverlay.Structures[AOT_CASTLE];
  end;
end;

// Initialize the buttons
procedure THUD.InitButtons;
var it    :TImageType;
    i     :Integer;
begin
  // Create the control buttons
  for it in TImageType do begin
    FIconButton[it]:=TImageButton.Create(Self, it);

    with FIconButton[it] do begin
      if it in [IT_EXIT, IT_MIN, IT_MAX] then begin
        // Exit, Min and Max Button on the right corner of the top panel
        Parent:=TopPanel;
        Align:=alRight;
        Width:=TopPanel.Height;
        Top:=0;
        OnClick:=@ControlClick;
      end else begin
        // other Buttons on the navigation bar
        Parent:=NaviBar;
        Align:=alNone;
        Width:=NaviBar.Width;
        Height:=Width;
        Left:=0;
        OnClick:=@MainChange;
      end;
      // Position of each button
      case it of
        IT_EXIT:     Left:=150;
        IT_MAX:      Left:=100;
        IT_MIN:      Left:=50;
        IT_OPTION:   Top:=0;
        IT_COLOR:    Top:=Height+5;
        IT_SETTINGS: Top:=2*Height+10;
        IT_VISIBLE:  Top:=3*Height+15;
        IT_MAP:      Top:=4*Height+20;
        IT_TIMER:    Top:=5*Height+25;
      end;
      Visible:= it <> IT_MAX;
      TabStop:=False;
    end;
  end;

  // Buttons of the OptionBox
  for i:=Low(FOptionBtn) to High(FOptionBtn) do begin
    FOptionBtn[i]:=TOwnButton.Create(Self, i+STRING_LANGUAGES);
    with FOptionBtn[i] do begin
      Parent:=OptionBox;
      Height:=25;
      TabStop:=False;
      Selectable:=True;
      if (i = 1)or(i = 3) then
        Left:=105
      else
        Left:=25;
      if i < 4 then begin
        Top:=45 + 30*(i div 2);
        Width:=70;
        OnClick:=@LanguageChange;
      end else begin
        ShowSelected:=True;
        Top:=30*i-15;
        Width:=150;
        OnClick:=@VisibleChange;
      end;
    end;
  end;

  // Buttons of the VisibleBox
  for i:=Low(FVisibleBtn) to High(FVisibleBtn) do begin
    FVisibleBtn[i]:=TOwnButton.Create(Self, i+STRING_VISIBLE);
    with FVisibleBtn[i] do begin
      Parent:=VisibleBox;
      Height:=25;
      TabStop:=False;
      Left:=25;
      Selected:=False;
      ShowSelected:=True;
      Top:=30*i+15;
      Width:=150;
      Selectable:=True;
      OnClick:=@VisibleChange;
    end;
  end;

  // Buttons of the MapBox
  for i:=Low(FMapBtn) to High(FMapBtn) do begin
    FMapBtn[i]:=TOwnButton.Create(Self, i+STRING_MAPS);
    with FMapBtn[i] do begin
      Parent:=MapBox;
      Height:=25;
      TabStop:=False;
      Selected:=False;
      Left:=25;
      Top:=30*i+15;
      Width:=150;
      ShowSelected:=i = 0;
      Selectable:=True;
      OnClick:=@MapChange;
    end;
  end;

  // Buttons of the Settings
  for i:=Low(FSetBtn) to High(FSetBtn) do begin
    FSetBtn[i]:=TOwnButton.Create(Self, i+STRING_SETTINGS);
    with FSetBtn[i] do begin
      Parent:=SettingBox;
      Height:=25;
      TabStop:=False;
      Selected:=False;
      Left:=25;
      Top:=30*i+170;
      Width:=150;
      ShowSelected:=True;
      Selectable:=True;
      OnClick:=@SettingsChange;
    end;
  end;

  // Buttons of the ColorBox
  for i:=Low(FColorBtn) to High(FColorBtn) do begin
    FColorBtn[i]:=TColorButton.Create(Self, i+STRING_COLORS);
    with FColorBtn[i] do begin
      Parent:=ColorBox;
      Height:=25;
      Left:=25;
      Width:=150;
      Top:=30*i+15;
      OnClick:=@ColorChange;
      TabStop:=False;
    end;
  end;

  // Buttons of the TimerBox
  for i:=Low(FTimerBtn) to High(FTimerBtn) do begin
    FTimerBtn[i]:=TOwnButton.Create(Self, i+STRING_STRUCTURES);
    with FTimerBtn[i] do begin
      Parent:=TimerBox;
      Height:=25;
      Left:=25;
      Width:=150;
      Top:=30*i+15;
      ShowSelected:=True;
      Selectable:=True;
      OnClick:=@TimerChange;
      TabStop:=False;
    end;
  end;
end;

// Initialize the panels
procedure THUD.InitPanels;
begin
  OptionBox.Align:=alClient;
  ColorBox.Align:=alClient;
  SettingBox.Align:=alClient;
  TimerBox.Align:=alClient;
  VisibleBox.Align:=alClient;
  MapBox.Align:=alClient;

  // Set the visibility
  MainChange(FIconButton[IT_OPTION]);
end;

// Release buttons
procedure THUD.ReleaseButtons;
var it    :TImageType;
    i     :Integer;
begin
  for i:=Low(FOptionBtn) to High(FOptionBtn) do
    FreeAndNil(FOptionBtn[i]);
  for i:=Low(FColorBtn) to High(FColorBtn) do
    FreeAndNil(FColorBtn[i]);
  for i:=Low(FTimerBtn) to High(FTimerBtn) do
    FreeAndNil(FTimerBtn[i]);
  for i:=Low(FMapBtn) to High(FMapBtn) do
    FreeAndNil(FMapBtn[i]);
  for i:=Low(FSetBtn) to High(FSetBtn) do
    FreeAndNil(FSetBtn[i]);
  for it in TImageType do
    FreeAndNil(FIconButton[it]);
end;

// Change the settings of the 2D maps
procedure THUD.MapChange(Sender:TObject);
var mt    :TWvWMapType;
begin
  if Sender = FMapBtn[0] then begin
    for mt in TWvWMapType do
      if mt <> MT_NONE then
        FOverlay.Map[mt].Mouse:=FMapBtn[0].Selected;

  end else if Sender = FMapBtn[1] then
    FOverlay.Map[MT_ETERNAL].Visible:=FMapBtn[1].Selected

  else if Sender = FMapBtn[2] then
    FOverlay.Map[MT_GREEN].Visible:=FMapBtn[2].Selected

  else if Sender = FMapBtn[3] then
    FOverlay.Map[MT_BLUE].Visible:=FMapBtn[3].Selected

  else if Sender = FMapBtn[4] then
    FOverlay.Map[MT_RED].Visible:=FMapBtn[4].Selected

  else if Sender = Self then begin
    FMapBtn[0].Selected:=True;
    for mt in TWvWMapType do
      if mt <> MT_NONE then
        FMapBtn[0].Selected:=FMapBtn[0].Selected and FOverlay.Map[mt].Mouse;

    FMapBtn[1].Selected:=FOverlay.Map[MT_ETERNAL].Visible;
    FMapBtn[2].Selected:=FOverlay.Map[MT_GREEN].Visible;
    FMapBtn[3].Selected:=FOverlay.Map[MT_BLUE].Visible;
    FMapBtn[4].Selected:=FOverlay.Map[MT_RED].Visible;
    Refresh;
  end;
end;

// Change event of the server list
procedure THUD.ServerListChange(Sender: TObject);
var id    :Integer;
begin
  id:=FOverlay.WebAPI.ServerID[ServerList.Text, Language];

  if id <> 0 then
    FOverlay.Server:=id;
end;

// Set compass settings
procedure THUD.SettingsChange(Sender:TObject);
begin
  // Sender = TrackCompassScale, change TOverlay.CompassScale
  if Sender = TrackCompassScale then
    FOverlay.Compass.Scale:=TrackCompassScale.Position/TrackCompassScale.Max

  // Sender = TrackCompassSize, change TOverlay.CompassSize
  else if Sender = TrackCompassSize then
    FOverlay.Compass.Size:=TrackCompassSize.Position

  else if Sender = TrackReminderVolume then
    FReminder.Volume:=TrackReminderVolume.Position

  else if Sender = FSetBtn[0] then
    FTs3Viewer.Sorted:=FSetBtn[0].Selected

  else if Sender = FSetBtn[1] then
    FTs3Viewer.ActiveTab:=FSetBtn[1].Selected

  else if Sender = ApiKey then
    FTs3Viewer.APIKey:=ApiKey.Text

  // Read default compass options
  else if Sender = Self then begin
    TrackCompassScale.Position:=Round(FOverlay.Compass.Scale*TrackCompassScale.Max);
    TrackCompassSize.Position:=FOverlay.Compass.Size;
    TrackReminderVolume.Position:=FReminder.Volume;
    ApiKey.Text:=FTs3Viewer.APIKey;

    FSetBtn[0].Selected:=FTs3Viewer.Sorted;
    FSetBtn[1].Selected:=FTs3Viewer.ActiveTab;
  end;
end;

// Change the visibility of the timer and compass
procedure THUD.VisibleChange(Sender:TObject);
begin
  if Sender = FOptionBtn[4] then begin
    ShowHint:=FOptionBtn[4].Selected;
    FOverlay.Hints:=FOptionBtn[4].Selected;
  end

  else if Sender = FVisibleBtn[0] then
    FOverlay.Compass.Visible:=FVisibleBtn[0].Selected

  else if Sender = FVisibleBtn[1] then
    FOverlay.Timer.Visible:=FVisibleBtn[1].Selected

  else if Sender = FVisibleBtn[2] then
    FReminder.Visible:=FVisibleBtn[2].Selected

  else if Sender = FVisibleBtn[3] then
    FOverlay.Notifier.Visible:=FVisibleBtn[3].Selected

  else if Sender = FVisibleBtn[4] then
    FOverlay.Statistic.Visible:=FVisibleBtn[4].Selected

  else if Sender = FVisibleBtn[5] then
    FTs3Viewer.Enabled:=FVisibleBtn[5].Selected

  else if Sender = Self then begin
    FOptionBtn[4].Selected:=ShowHint;
    FVisibleBtn[0].Selected:=FOverlay.Compass.Visible;
    FVisibleBtn[1].Selected:=FOverlay.Timer.Visible;
    FVisibleBtn[2].Selected:=FReminder.Visible;
    FVisibleBtn[3].Selected:=FOverlay.Notifier.Visible;
    FVisibleBtn[4].Selected:=FOverlay.Statistic.Visible;
    FVisibleBtn[5].Selected:=FTs3Viewer.Visible;
    Refresh;
  end;
end;

// Save configuration
procedure THUD.SaveConfig;
var mt    :TWvWMapType;
    sec   :String;
    ini   :TIniFile;
begin
  // Open Config.ini
  ini:=TIniFile.Create('Config.ini');

  // Save the reminder
  FReminder.Save(ini);

  with ini do begin
    // Write Language
    case Language of
      L_ENGLISH:WriteString('Option', 'Language', 'English');
      L_FRENCH: WriteString('Option', 'Language', 'French');
      L_GERMAN: WriteString('Option', 'Language', 'German');
      L_SPANISH:WriteString('Option', 'Language', 'Spanish');
    end;

    // Write position
    WriteInteger('Option', 'Left', Left);
    WriteInteger('Option', 'Top', Top);

    // Write help settings
    WriteBool('Option', 'Help', ShowHint);

    // Write colors settings
    WriteInteger('Option', 'Backcolor', Colors.Back);
    WriteInteger('Option', 'Fontcolor', Colors.Font);
    WriteInteger('Option', 'Frontcolor', Colors.Front);

    // Write visibility
    WriteBool('Option', 'Compass', FOverlay.Compass.Visible);
    WriteBool('Option', 'Notifier', FVisibleBtn[3].Selected);
    WriteBool('Option', 'Reminder', FReminder.Visible);
    WriteBool('Option', 'Timer', FOverlay.Timer.Visible);
    WriteBool('Option', 'Statistic', FVisibleBtn[4].Selected);
    WriteBool('Option', 'Ts3Viewer', FVisibleBtn[5].Selected);
    WriteInteger('Option', 'Volume', FReminder.Volume);

    // Reminder settings
    WriteInteger('Reminder', 'Left', FReminder.Left);
    WriteInteger('Reminder', 'Top', FReminder.Top);

    // Notifier settings
    WriteInteger('Notifier', 'Left', FOverlay.Notifier.Left);
    WriteInteger('Notifier', 'Top', FOverlay.Notifier.Top);

    // Statistics settings
    WriteInteger('Statistic', 'Left', FOverlay.Statistic.Left);
    WriteInteger('Statistic', 'Top', FOverlay.Statistic.Top);

    // Ts3 Viewer settings
    WriteInteger('Ts3Viewer', 'Left', FTs3Viewer.Left);
    WriteInteger('Ts3Viewer', 'Top', FTs3Viewer.Top);
    WriteBool('Ts3Viewer', 'OnlyActive', FTs3Viewer.ActiveTab);
    WriteBool('Ts3Viewer', 'Sorted', FTs3Viewer.Sorted);
    WriteString('Ts3Viewer', 'APIKey', FTs3Viewer.APIKey);

    // Write current server
    WriteInteger('Option', 'Server', FOverlay.Server);

    // Write Compass settings
    WriteFloat('Compass', 'Scale', FOverlay.Compass.Scale);
    WriteInteger('Compass', 'Size', FOverlay.Compass.Size);

    // Write visibility of wvw structures
    WriteBool('Timer', 'Always', ForceVisibleObjects);
    WriteBool('Timer', 'Camp', FOverlay.Structures[AOT_CAMP]);
    WriteBool('Timer', 'Castle', FOverlay.Structures[AOT_CASTLE]);
    WriteBool('Timer', 'Keep', FOverlay.Structures[AOT_KEEP]);
    WriteBool('Timer', 'Ruin', FOverlay.Structures[AOT_RUINS]);
    WriteBool('Timer', 'Tower', FOverlay.Structures[AOT_TOWER]);

    // Write the settings of the 2D maps
    for mt in TWvWMapType do begin
      if mt = MT_NONE then
        continue;

      case mt of
        MT_ETERNAL:sec:='Eternal';
        MT_GREEN:  sec:='Green';
        MT_BLUE:   sec:='Blue';
        MT_RED:    sec:='Red';
      else
        sec:='';
      end;
      if sec <> '' then
        with FOverlay.Map[mt] do begin
          WriteFloat(sec, 'Alpha', Alpha);
          WriteInteger(sec, 'Left', Left);
          WriteBool(sec, 'Lock', Mouse);
          WriteFloat(sec, 'Size', Size);
          WriteInteger(sec, 'Top', Top);
          case mt of
            MT_ETERNAL:
              WriteBool(sec, 'Visible', FMapBtn[1].Selected);

            MT_RED:
              WriteBool(sec, 'Visible', FMapBtn[4].Selected);

            MT_BLUE:
              WriteBool(sec, 'Visible', FMapBtn[3].Selected);

            MT_GREEN:
              WriteBool(sec, 'Visible', FMapBtn[2].Selected);
          end;
        end;
     end;

    // Close file
    Free;

    Logger.log('Saved config file.');
  end;
end;

// Load configurations
procedure THUD.LoadConfig;
var mt    :TWvWMapType;
    sec   :String;
    ini   :TIniFile;
begin
  // Check existing of config.ini
  if FileExists('Config.ini') then begin
    ini :=TIniFile.Create('Config.ini');

    // Load the reminder
    FReminder.Load(ini);

    with ini do begin
      Logger.log('Loading config file');
      // if section exists, load
      if SectionExists('Option') then begin
        // Load language
        case ReadString('Option', 'Language', '') of
          'German': Language:=L_GERMAN;
          'English':Language:=L_ENGLISH;
          'French': Language:=L_FRENCH;
          'Spanish':Language:=L_SPANISH;
        end;

        // Load position
        Left:=ReadInteger('Option', 'Left', Left);
        Top:=ReadInteger('Option', 'Top', Top);

        // Load help settings
        ShowHint:=ReadBool('Option', 'Help', ShowHint);
        FOverlay.Hints:=ShowHint;

        // Load the volume of the reminder
        FReminder.Volume:=ReadInteger('Option', 'Volume', FReminder.Volume);

        // Load colors
        with Colors do begin
          Back:=TColor(ReadInteger('Option', 'Backcolor', Back) and $FEFEFE);
          Front:=TColor(ReadInteger('Option', 'Frontcolor', Front) and $FEFEFE);
          Font:=TColor(ReadInteger('Option', 'Fontcolor', Font) and $FEFEFE);
        end;

        // Load visibility and server
        with FOverlay do begin
          Notifier.Visible:=ReadBool('Option', 'Notifier', Notifier.Visible);
          Compass.Visible:=ReadBool('Option', 'Compass', Compass.Visible);
          Timer.Visible:=ReadBool('Option', 'Timer', Timer.Visible);
          FReminder.Visible:=ReadBool('Option', 'Reminder', FReminder.Visible);
          Statistic.Visible:=ReadBool('Option', 'Statistic', Statistic.Visible);
          FTs3Viewer.Enabled:=ReadBool('Option', 'Ts3Viewer', FTs3Viewer.Visible);
          Server:=ReadInteger('Option', 'Server', Server);
        end;
      end else
        Logger.log('No Option section found.');

      if SectionExists('Reminder') then begin
        FReminder.Left:=ReadInteger('Reminder', 'Left', FReminder.Left);
        FReminder.Top:=ReadInteger('Reminder', 'Top', FReminder.Top);
      end else
        Logger.log('No Reminder section found.');

      if SectionExists('Notifier') then begin
        FOverlay.Notifier.Left:=ReadInteger('Notifier', 'Left', FOverlay.Notifier.Left);
        FOverlay.Notifier.Top:=ReadInteger('Notifier', 'Top', FOverlay.Notifier.Top);
      end else
        Logger.log('No Notifier section found.');

      if SectionExists('Statistic') then begin
        FOverlay.Statistic.Left:=ReadInteger('Statistic', 'Left', FOverlay.Statistic.Left);
        FOverlay.Statistic.Top:=ReadInteger('Statistic', 'Top', FOverlay.Statistic.Top);
      end else
        Logger.log('No Statistic section found.');

      if SectionExists('Ts3Viewer') then begin
        FTs3Viewer.Left:=ReadInteger('Ts3Viewer', 'Left', FTs3Viewer.Left);
        FTs3Viewer.Top:=ReadInteger('Ts3Viewer', 'Top', FTs3Viewer.Top);
        FTs3Viewer.ActiveTab:=ReadBool('Ts3Viewer', 'OnlyActive', FTs3Viewer.ActiveTab);
        FTs3Viewer.Sorted:=ReadBool('Ts3Viewer', 'Sorted', FTs3Viewer.Sorted);
        FTs3Viewer.APIKey:=ReadString('Ts3Viewer', 'APIKey', FTs3Viewer.APIKey);
      end else
        Logger.log('No Ts3Viewer section found.');

      // Reading the settings of the 2D maps
      for mt in TWvWMapType do begin
        if mt = MT_NONE then
          Continue;

        case mt of
          MT_ETERNAL:sec:='Eternal';
          MT_GREEN:  sec:='Green';
          MT_BLUE:   sec:='Blue';
          MT_RED:    sec:='Red';
        else
          sec:='';
        end;
        if (sec<>'')and(SectionExists(sec)) then
          with FOverlay.Map[mt] do begin
            Alpha:=ReadFloat(sec, 'Alpha', Alpha);
            Left:=ReadInteger(sec, 'Left', Left);
            Mouse:=ReadBool(sec, 'Lock', Mouse);
            Size:=ReadFloat(sec, 'Size', Size);
            Top:=ReadInteger(sec, 'Top', Top);
            Visible:=ReadBool(sec, 'Visible', Visible);
          end
        else
          Logger.log('No section '+sec+' found.');
      end;

      if SectionExists('Compass') then
        // Load compass configuration
        with FOverlay.Compass do begin
          Scale:=ReadFloat('Compass', 'Scale', Scale);
          Size:=ReadInteger('Compass', 'Size', Size);
        end;

      if SectionExists('Timer') then
        // Load visibility of the timer of wvw structures
        with FOverlay do begin
          ForceVisibleObjects:=ReadBool('Timer', 'Always', ForceVisibleObjects);
          Structures[AOT_RUINS]:=ReadBool('Timer', 'Ruin', Structures[AOT_RUINS]);
          Structures[AOT_CAMP]:=ReadBool('Timer', 'Camp', Structures[AOT_CAMP]);
          Structures[AOT_TOWER]:=ReadBool('Timer', 'Tower', Structures[AOT_TOWER]);
          Structures[AOT_KEEP]:=ReadBool('Timer', 'Keep', Structures[AOT_KEEP]);
          Structures[AOT_CASTLE]:=ReadBool('Timer', 'Castle', Structures[AOT_CASTLE]);
        end
      else
        Logger.log('No timer section.');

      Free;
      Logger.log('Loaded config file.');
    end;
  end;
end;

end.

