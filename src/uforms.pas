unit uForms;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, windows, uOption;

type
  { TDragForm }
  // Implementation of an none border draggable form
  TDragForm     = class(TForm)
  private
    FMouse      :record
      X, Y      :Integer;
      Drag      :Boolean;
    end;

    // Move procedure that bounces on the virtual screen borders
    procedure   Move(X, Y:Integer);

    // Mouse event procedures
    procedure   MouseDown(Sender:TObject;Button:TMouseButton;{%H-}Shift:TShiftState;X,Y:Integer);reintroduce;
    procedure   MouseMove(Sender:TObject;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);reintroduce;
    procedure   MouseUp(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);reintroduce;
  protected
    procedure   DoShow;override;
  public
    // Constructor
    constructor Create(TheOwner:TComponent);override;
  end;

implementation

{ TDragForm }
constructor TDragForm.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  // Set the border style and form style
  BorderStyle:=bsNone;
  FormStyle:=fsSystemStayOnTop;

  // Transparent for Mouse and Color
  Color:=Colors.Transparent;

{$IF defined(win32)}
  // Set windows style to a layered window
  SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED);

  // Set up the transparent colorkey
  SetLayeredWindowAttributes(Handle, Color, 0, LWA_COLORKEY);
{$ELSE}
  // TODO: Other OS
{$ENDIF}

  // Set the mouse event handler of the form
  OnMouseDown:=@MouseDown;
  OnMouseMove:=@MouseMove;
  OnMouseUp:=@MouseUp;
end;

// Manipulate the Show event by moving the form
procedure TDragForm.DoShow;
begin
  Move(Left, Top);
  inherited;
end;

// Move procedure that bounce on the virtual screen
procedure TDragForm.Move(X, Y:Integer);
begin
{$IF defined(win32)}
  tmp:=GetSystemMetrics(SM_XVIRTUALSCREEN);
  X:=Max(tmp, Min(tmp + GetSystemMetrics(SM_CXVIRTUALSCREEN) - Width, X));

  tmp:=GetSystemMetrics(SM_YVIRTUALSCREEN);
  Y:=Max(tmp, Min(tmp + GetSystemMetrics(SM_CYVIRTUALSCREEN) - Height, Y));
{$ELSE}
  // TODO
{$ENDIF}

  // Update the position of the window
  SetBounds(X, Y, Width, Height);
end;

// Mouse down event
procedure TDragForm.MouseDown(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  // You can drag only with the left mouse button
  if Button = mbLeft then begin
    // Activate dragging and copy the current position
    FMouse.Drag:=True;
    FMouse.X:=X;
    FMouse.Y:=Y;
  end;
end;

// Mouse move event
procedure TDragForm.MouseMove(Sender:TObject;Shift:TShiftState;X,Y:Integer);
begin
  // If the dragging is activated
  if FMouse.Drag then
    // Move the form
    Move(Left + X - FMouse.X, Top + Y - FMouse.Y);
end;

// Mouse up event
procedure TDragForm.MouseUp(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  // Deactivate the dragging
  FMouse.Drag:=False;
end;

end.

