#GuildWars2 Battle HUD

##Files

**Battle HUD:** Compiled executable

**msvcr71.dll:** Microsoft Libary File

**alarm.wav:** 


##Using

**IDE:** [Lazarus 1.4.0](http://www.lazarus.freepascal.org)

**Compiler:** fpc 2.6.2

**API:** [*GuildWars 2 API*](http://wiki.guildwars2.com/wiki/API:Main), [*Mumble*](http://wiki.guildwars2.com/wiki/Mumble) and *ClientQuery Plugin*