GuildWars2 Battle HUD
  Created By draos.9574


Newest Version: https://bitbucket.org/draos/guildwars2-battle-hud


Installation:
 - Extract the archive into a separate directory
 - Set the resolution of GuildWars 2 to Windowed Fullscreen
 - Start the program


Features:
 - 2D Maps:
   - All invulnerability timers of WvW objects are projected to 2D maps
   - Uses only the GuildWars2 API
 - Compass:
   - Offeres a compass based on the Mumble API of GuildWars2
 - Timer:
   - All invulnerability timers of WvW objects lying in the field of view
   - If not setting always visible only objects with invulnerability timer are displayed
   - Uses the Mumble Interface and directly the GuildWars2 API
 - Reminder:
   - Set up timer to remind you of something
 - Notification:
   - Notify WvW objects which owner has changed
   - Based on the Timer
 - Statistic viewer:
   - Shows some information in a small form
 - TeamSpeak 3 viewer:
   - Shows talking TeamSpeak 3 clients and the active server tab.
 - Help System:
   - If activated you can see hints of mostly all buttons with short explanations
 - Multilingual:
   - Supports all languages of the GuildWars2 API (English, German, French and Spanish)


Files:
 - Battle HUD (Compiled executable)
 - msvcr71.dll (Microsoft Library File)
 - Config.ini (if none exists it will use default settings)


Used:
 - IDE: Lazarus 1.4.0 (http://www.lazarus.freepascal.org)
 - Compiler: fpc 2.6.2
 - GuildWars2 API (http://wiki.guildwars2.com/wiki/API:Main)
 - GuildWars2 Mumble (http://wiki.guildwars2.com/wiki/Mumble)
 - Internet Tools (http://wiki.lazarus.freepascal.org/Internet_Tools)
 - lNet  (http://wiki.lazarus.freepascal.org/lNet)
 - Official TeamSpeak 3 ClientQuery Plugin


GuildWars 2 API Terms of Use:
 - http://wiki.guildwars2.com/wiki/API:Main#Guild_Wars_2_API_Terms_of_Use