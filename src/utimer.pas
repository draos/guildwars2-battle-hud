{
  Created by: draos.9574

  Implements the timer overlay for the wvw objects.
}

unit uTimer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uMumble, uSymbols, uData, matrix, math, Controls,
  uOption, uTypes, uWebAPI;

type
  { TMap3DOverlay }
  TMap3DOverlay     = class
  private
    // Number of objects
    FObjects        :Array of TObjectSymbol;
    FCount          :Integer;
    FLoaded         :Boolean;
    // Field of View
    FFoV            :TFieldOfView;
    // Link to the object images
    FImageList      :TImageList;
    // Current map
    FMap            :Integer;
    // Link to the web api interface
    FWebAPI         :TWebAPI;
    // Link to mumble class
    FMumble         :TMumble;
    // Parent, here TOverlay
    FParent         :TWinControl;
    // Visibility of different types of structures
    FStructVisible  :Array[TWvWObjectiveType] of Boolean;
    // Visibility of the timers
    FVisible        :Boolean;
    // Update time, used over a callback from every object to minimized time calls
    FTime           :TDateTime;
    // Screen dimensions
    FScreen         :record
      Width, Height :Single;
    end;
    // Current Camera Information (stored for speed up)
    FCamera         :record
      Direction     ,
      Horizontal    ,
      Vertical      :Tvector3_double;
    end;

    function        convertPosition(x, y, z:Single):TCoordinate;
    function        getFoVHorizontal:Single;
    function        getFoVVertical:Single;
    function        getSVisible(Typ:TWvWObjectiveType):Boolean;
    function        getTime:TDateTime;
    function        prepareCamera:Boolean;

    procedure       changeMap;
    procedure       setFoVHorizontal(Value:Single);
    procedure       setFoVVertical(Value:Single);
    procedure       setSVisible(Typ:TWvWObjectiveType;Value:Boolean);
    procedure       setVisible(Value:Boolean);
  public
    constructor     Create(Mumble:TMumble;WebAPI:TWebAPI;ImageList:TImageList;Parent:TWinControl);
    destructor      Destroy;override;

    procedure       Refresh;
    procedure       Reset;
    procedure       ResetObjects;
    procedure       ResetMetrics;
    procedure       AddObject(Obj:TAPIMObjective);

    property        Count:Integer read FCount;
    property        FoVHorizontal:Single read getFoVHorizontal write setFoVHorizontal;
    property        FoVVertical:Single read getFoVVertical write setFoVVertical;
    property        Structures[Typ:TWvWObjectiveType]:Boolean read getSVisible write setSVisible;
    property        Visible:Boolean read FVisible write setVisible;
  end;


implementation

{ TMap3DOverlay }
constructor TMap3DOverlay.Create(Mumble:TMumble;WebAPI:TWebAPI;ImageList:TImageList;Parent:TWinControl);
var aot     :TWvWObjectiveType;
begin
  FWebAPI:=WebAPI;
  FMumble:=Mumble;
  FMap:=0;
  FParent:=Parent;
  FVisible:=True;
  FImageList:=ImageList;
  FLoaded:=False;

  // Clear objectlist
  ResetObjects;

  // Get system metrics
  FScreen.Width:=Parent.Width;
  FScreen.Height:=Parent.Height;

  // Set all except ruins to visible
  for aot in TWvWObjectiveType do
    FStructVisible[aot]:=not (aot in [AOT_RUINS, AOT_SHRINE, AOT_GENERIC]);

  // Set the field of view for different screen resolutions
  ResetMetrics;

  Logger.log('Created timer overlay.');
end;

// destructor
destructor TMap3DOverlay.Destroy;
begin
  ResetObjects;

  Logger.log('Freed timer overlay.');
  inherited;
end;

// Reset Field of Views and the overlay size
procedure TMap3DOverlay.ResetMetrics;
begin
  // Get system metrics
  FScreen.Width:=FParent.Width;
  FScreen.Height:=FParent.Height;

  // Apply metrics
  if FScreen.Height > 0 then begin
    // Calculate the field of view
    FFoV.Vertical:=tan(FMumble.FoVVertical / 2);
    FFoV.Horizontal:=FFoV.Vertical*FScreen.Width/FScreen.Height;
  end else begin
    // use default values on error
    FFoV.Vertical:=1.0;
    FFoV.Horizontal:=1.0;
    FScreen.Height:=0;
    FScreen.Width:=0;
  end;
end;

// Reset procedure, delete objects and reinitialize it
procedure TMap3DOverlay.Reset;
begin
  FMap:=0;
  changeMap;
end;

// Change map
procedure TMap3DOverlay.changeMap;
var i     :Integer;
    cmatch:TAPIMatchup;
    cmap  :TAPIMap;
begin
  if FWebAPI = Nil then
    Exit;

  cmatch:=FWebAPI.CurrentMatch;
  if cmatch = Nil then
    Exit;

  cmap:=cmatch.MapByID[FMumble.Map];
  if cmap = Nil then
    Exit;

  // Delete current objects
  ResetObjects;
  FMap:=FMumble.Map;

  for i:=0 to cmap.Objectives.Count - 1 do
    if FStructVisible[cmap.Objectives.Data[i].ObjectiveType] then
      AddObject(cmap.Objectives.Data[i]);
end;

// Refresh the timer overlay
procedure TMap3DOverlay.Refresh;
var i     :Integer;
    p     :TCoordinate;
begin
  if (FMumble <> Nil) and (FWebAPI <> Nil) then begin
    // Map changed? Then do it
    if FMap <> FMumble.Map then
      changeMap;

    // Set the current time
    FTime:=Now;

    // Update all positions of the objects and the visibility
    if prepareCamera then begin
      for i:=0 to FCount - 1 do
        if FObjects[i].Data <> Nil then
           with FObjects[i].Data do begin
             p:=convertPosition(Coordinate[0], Coordinate[1], Coordinate[2]);

             // Set position and visibility
             FObjects[i].SetPosition(p.x, p.y);
             FObjects[i].Visible:=p.Visible and FVisible;
           end;
    end;

    if not FLoaded then
      FMap:=0;
  end;
end;

// Delete all objects
procedure TMap3DOverlay.ResetObjects;
var i     :Integer;
begin
  i:=0;
  while i<FCount do begin
    FreeAndNil(FObjects[i]);
    Inc(i);
  end;
  FLoaded:=True;
  SetLength(FObjects, 0);
  FCount:=0;
end;

// Add a object
procedure TMap3DOverlay.AddObject(Obj:TAPIMObjective);
begin
  if Obj <> nil then begin
    SetLength(FObjects, FCount+1);
    FObjects[FCount]:=TObjectSymbol.Create(Nil, FImageList, @getTime, Obj);
    FObjects[FCount].WebAPI:=FWebAPI;
    FObjects[FCount].Parent:=FParent;
    FObjects[FCount].Color:=Colors.Transparent;
    Inc(FCount);
  end else
    FLoaded:=False;
end;

// Initialize the camera vectors
function TMap3DOverlay.prepareCamera:Boolean;
var up    :Tvector3_double;
begin
  Result:=True;
  ResetMetrics;
  // Initialize the vector of the camera direction and normalize it
  with FMumble.Camera do
    FCamera.Direction.init(Front[0], Front[1], Front[2]);
  if FCamera.Direction.length <> 0 then
    FCamera.Direction:=FCamera.Direction/FCamera.Direction.length
  else
    Result:=False;

  // Initialize the vertical and horizontal vectors of the camera and normalize it
  up.init(0, 1, 0);
  FCamera.Horizontal:=up><FCamera.Direction;
  FCamera.Vertical:=FCamera.Direction><FCamera.Horizontal;

  if (FCamera.Horizontal.length<>0)and(FCamera.Vertical.length<>0) then begin
    FCamera.Horizontal:=FCamera.Horizontal/FCamera.Horizontal.length;
    FCamera.Vertical:=FCamera.Vertical/FCamera.Vertical.length;
  end else
    Result:=False;
end;

// convert the 3D positions of the avatar and the objects to 2D display positions
function TMap3DOverlay.convertPosition(x, y, z:Single):TCoordinate;
var up,eye,p            :Tvector3_double;
    dhor,dvert,d        :Double;
begin
  // Default value
  Result.x:=0;
  Result.y:=0;
  Result.Visible:=False;

  if FMumble <> Nil then begin
    // Initialize the point p, the camera eye and the camera direction n
    p.init(x,y,z);

    with FMumble.Camera do
      eye.init(Position[0], Position[1], Position[2]);

    // Calculate the vector between point and camera
    up:=p-eye;

    d:=FCamera.Direction**up;
    if d > 0.0 then begin
    // Calculate the horizontal and vertical distances from the point to the
    //  line through the eye and p and transform via field of view
      dhor:=FCamera.Horizontal**(up/d/FFoV.Horizontal);
      dvert:=FCamera.Vertical**(up/d/FFoV.Vertical);

      // convert the vectors to a pixel on the screen
      Result.x:=Round(FScreen.Width/2*(1+dhor));
      Result.y:=Round(FScreen.Height/2*(1-dvert));
      Result.Visible:=True;
    end;
  end;
end;

// Getter
function TMap3DOverlay.getFoVHorizontal:Single;
begin
  Result:=arctan(FFoV.Horizontal)*90/PI;
  if Result < 0 then
    Result:=Result+90;
end;

function TMap3DOverlay.getFoVVertical:Single;
begin
  Result:=arctan(FFoV.Vertical)*90/PI;
  if Result < 0 then
    Result:=Result+90;
end;

function TMap3DOverlay.getTime:TDateTime;
begin
  Result:=FTime;
end;

function TMap3DOverlay.getSVisible(Typ:TWvWObjectiveType):Boolean;
begin
  Result:=FStructVisible[Typ];
end;

// Setter
procedure TMap3DOverlay.setSVisible(Typ:TWvWObjectiveType;Value:Boolean);
begin
  FStructVisible[Typ]:=Value;

  changeMap;
end;

procedure TMap3DOverlay.setFoVHorizontal(Value:Single);
begin
  if (Value>=60)and(Value<90) then
    FFoV.Horizontal:=tan(Value*PI/90);
end;

procedure TMap3DOverlay.setFoVVertical(Value:Single);
begin
  if (Value>=60)and(Value<90) then
    FFoV.Vertical:=tan(Value*PI/90);
end;

procedure TMap3DOverlay.setVisible(Value:Boolean);
var i     :Integer;
begin
  FVisible:=Value;
  i:=0;
  while i < FCount do begin
    FObjects[i].Visible:=Value;
    Inc(i);
  end;
end;

end.

