{
  Created by: draos.9574

  Implements the overlay.
}

unit uOverlay;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  ExtCtrls, Windows, uMumble, uCompass, uTimer, uOption, uTypes, u2DMap,
  uStatistic, uWebAPI, uNotification;

type
  { TOverlay }
  TOverlay          = class(TForm)
    Shield: TImage;
    ObjectImages    :TImageList;
    Clock           : TTimer;
    procedure       FormCreate(Sender: TObject);
    procedure       FormDestroy(Sender: TObject);
    procedure       ClockTimer(Sender: TObject);
  private
    FNotify         :TNotification;
    FServer         :Integer;

    FCompass        :TCompassOverlay;
    FTimer          :TMap3DOverlay;
    FStatistics     :TStatistic;

    FWebAPI         :TWebAPI;
    FMumble         :TMumble;

    F2DMaps         :Array[TWvWMapType] of TMap2DOverlay;

    // Getter
    function        getMap(Map:TWvWMapType):TMap2DOverlay;
    function        getStructVisible(Typ:TWvWObjectiveType):Boolean;

    // Update Callbacks
    procedure       MumbleRefresh;

    // Setter
    procedure       setShowHint(Value:Boolean);
    procedure       setServer(Value:Integer);
    procedure       setStructVisible(Typ:TWvWObjectiveType;Value:Boolean);

    // Events
    procedure       ClaimedObject(const Obj:TAPIMObjective);
    procedure       FlippedObject(const Obj:TAPIMObjective);
  public
    procedure       Notify(const Msg:String;Ico:Integer=-1);
    procedure       Reload;
    procedure       Shutdown;
    procedure       ResetPosition;

    // Properties
    property        Compass:TCompassOverlay read FCompass;
    property        Hints:Boolean write setShowHint;
    property        Map[MapType:TWvWMapType]:TMap2DOverlay read getMap;
    property        Mumble:TMumble read FMumble;
    property        Notifier:TNotification read FNotify write FNotify;
    property        Server:Integer read FServer write setServer;
    property        Statistic:TStatistic read FStatistics write FStatistics;
    property        Structures[Typ:TWvWObjectiveType]:Boolean read getStructVisible write setStructVisible;
    property        Timer:TMap3DOverlay read FTimer;
    property        WebAPI:TWebAPI read FWebAPI;
  end;

implementation

{$R *.lfm}

{ TOverlay }
procedure TOverlay.FormCreate(Sender: TObject);
var mt    :TWvWMapType;
begin
  Color:=Colors.Transparent;
  FServer:=2202;

  // Create the interfaces
  FWebAPI:=TWebAPI.Create;
  FWebAPI.Server:=Server;
  FWebAPI.OnClaimObjective:=@ClaimedObject;
  FWebAPI.OnFlipObjective:=@FlippedObject;

  FMumble:=TMumble.Create(@MumbleRefresh, 10);

  // Set default values
  ResetPosition;
  BorderStyle:=bsNone;
  WindowState:=wsFullScreen;
  FormStyle:=fsSystemStayOnTop;

  // Transparent for Mouse and Color
  SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED or WS_EX_TRANSPARENT);
  SetLayeredWindowAttributes(Handle, Color, 0, LWA_COLORKEY);

  // Create the Overlay Plugins
  FCompass:=TCompassOverlay.Create(FMumble, Self);
  FTimer:=TMap3DOverlay.Create(FMumble, FWebAPI, ObjectImages, Self);

  // Create 2D Maps
  for mt in TWvWMapType do
    if mt <> MT_NONE then begin
      F2DMaps[mt]:=TMap2DOverlay.Create(Self);
      F2DMaps[mt].Guild:=Shield;
      F2DMaps[mt].Settings(mt, FWebAPI, ObjectImages);
    end;

  // Create the statistics
  FStatistics:=TStatistic.Create(Self);
  FStatistics.WebAPI:=FWebAPI;

  // Create notification system
  FNotify:=TNotification.Create(Self);
  FNotify.Icons:=ObjectImages;
  FNotify.Guild:=Shield;

  // Activate Mumble
  FMumble.Enabled:=True;
  Clock.Enabled:=True;

  Logger.log('Created overlay.');
end;

// Refreshes the position and size of the overlay
procedure TOverlay.ResetPosition;
var hGW2             :HWND;
    rWin, rClient    :Windows.TRECT;
    Border           :Longint;
begin
  hGW2:=FindWindow(PChar('ArenaNet_Dx_Window_Class'), PChar('Guild Wars 2'));
  if hGW2 <> 0 then begin
    GetWindowRect(hGW2, @rWin);
    Windows.GetClientRect(hGW2, @rClient);
    Border:=(rWin.Right-rWin.Left-rClient.Right+rClient.Left) div 2;
    SetBounds(rWin.Left+Border, rWin.Bottom-rClient.Bottom-Border, rClient.Right, rClient.Bottom);
  end else
    SetBounds(0, 0, 0, 0);
end;

// destructor
procedure TOverlay.FormDestroy(Sender: TObject);
var mt    :TWvWMapType;
begin
  Mumble.Enabled:=False;

  FreeAndNil(FMumble);
  FreeAndNil(FWebAPI);

  for mt in TWvWMapType do
    FreeAndNil(F2DMaps[mt]);

  FreeAndNil(FCompass);
  FreeAndNil(FTimer);
  FreeAndNil(FStatistics);
  FreeAndNil(FNotify);

  Logger.log('Freed overlay.');
end;

procedure TOverlay.ClockTimer(Sender: TObject);
var mt    :TWvWMapType;
begin
  FWebAPI.Update;
  FStatistics.Reload;
  FNotify.Clock;

  for mt in TWvWMapType do
    if mt <> MT_NONE then
      F2DMaps[mt].Reload;
end;

// Refresh or Reload procedures
procedure TOverlay.Reload;
var mt    :TWvWMapType;
begin
  FCompass.Refresh;
  FTimer.Refresh;
  FNotify.Reload;
  FStatistics.Reload;


  for mt in TWvWMapType do
    if mt <> MT_NONE then
      F2DMaps[mt].Reload;
end;

procedure TOverlay.MumbleRefresh;
begin
  // Code if Mumble refreshed
  ResetPosition;

  if Mumble.FPS >= MinimalFPS then begin
    Visible:=True;

    FTimer.Refresh;
    FCompass.Refresh;
  end else
    Visible:=False;
end;

// Events
procedure TOverlay.ClaimedObject(const Obj:TAPIMObjective);
var data  :TAPIObjective;
    guild :TAPIGuild;
    cmatch:TAPIMatchup;
    gname :String;
    oname :String;
    names :Array[TServerColor] of String;
    sc    :TServerColor;
    k     :Integer;
begin
  if (Obj = Nil) or (FWebAPI.MatchID <> Obj.MatchID) then
    Exit;

  // Only show visible objectives
  if not FTimer.Structures[Obj.ObjectiveType] then
    Exit;

  // Get more informations
  data:=FWebAPI.Objective[Obj.ID];
  if (data = Nil) or (data.MapID <> FMumble.Map) then
    Exit;

  // Get informations about the claiming Shield
  guild:=FWebAPI.GuildByID[obj.ClaimedBy];
  if guild <> Nil then
    gname:=Format('%s [%s]', [Guild.Name, Guild.Tag])
  else
    gname:='';

  // Get the icon index
  case Obj.Owner of
    OC_NONE: k:=4 * Obj.Offset + 2;
    OC_GREEN:k:=4 * Obj.Offset + 1;
    OC_BLUE: k:=4 * Obj.Offset;
    OC_RED:  k:=4 * Obj.Offset + 3;
  else
    k:=-1;
  end;
  if k < 0 then k:=-1;

  // Get the object name
  oname:=data.Names[Language];

  // Load the current matchup
  cmatch:=FWebAPI.CurrentMatch;
  for sc in TServerColor do
    if cmatch <> Nil then
      names[sc]:=FWebAPI.WorldName[cmatch.Worlds[sc][0], Language]
    else
      names[sc]:='';

  // Replace the strings
  if cmatch <> Nil then begin
    oname:=StringReplace(oname, '<RED>', names[SC_RED], [rfReplaceAll]);
    oname:=StringReplace(oname, '<BLUE>', names[SC_BLUE], [rfReplaceAll]);
    oname:=StringReplace(oname, '<GREEN>', names[SC_GREEN], [rfReplaceAll]);
  end;

  FNotify.Notify(oname, gname, k);
end;

procedure TOverlay.FlippedObject(const Obj:TAPIMObjective);
var data  :TAPIObjective;
    k     :Integer;
    cmatch:TAPIMatchup;
    oname :String;
    names :Array[TServerColor] of String;
    sc    :TServerColor;
begin
  if (Obj = Nil) or (FWebAPI.MatchID <> Obj.MatchID) then
    Exit;

  // Only show visible objectives
  if not FTimer.Structures[Obj.ObjectiveType] then
    Exit;

  // Get more informations
  data:=FWebAPI.Objective[Obj.ID];
  if (data = Nil) or (data.MapID <> FMumble.Map) then
    Exit;

  // Get the icon index
  case Obj.Owner of
    OC_NONE: k:=4 * Obj.Offset + 2;
    OC_GREEN:k:=4 * Obj.Offset + 1;
    OC_BLUE: k:=4 * Obj.Offset;
    OC_RED:  k:=4 * Obj.Offset + 3;
  else
    k:=-1;
  end;
  if k < 0 then k:=-1;

  // Get the object name
  oname:=data.Names[Language];

  // Load the current matchup
  cmatch:=FWebAPI.CurrentMatch;

  for sc in TServerColor do
    if cmatch <> Nil then
      names[sc]:=FWebAPI.WorldName[cmatch.Worlds[sc][0], Language]
    else
      names[sc]:='';

  // Replace the strings
  if cmatch <> Nil then begin
    oname:=StringReplace(oname, '<RED>', names[SC_RED], [rfReplaceAll]);
    oname:=StringReplace(oname, '<BLUE>', names[SC_BLUE], [rfReplaceAll]);
    oname:=StringReplace(oname, '<GREEN>', names[SC_GREEN], [rfReplaceAll]);
  end;

  FNotify.Notify(oname, k);
end;

procedure TOverlay.Notify(const Msg:String;Ico:Integer=-1);
begin
  FNotify.Notify(Msg, Ico);
end;

// Shutdown the overlay and plugins if there are extra windows
procedure TOverlay.Shutdown;
var mt    :TWvWMapType;
begin
  FStatistics.Close;
  FNotify.Close;
  for mt in TWvWMapType do
    if mt <> MT_NONE then
      F2DMaps[mt].Close;
end;

{ TOverlay - Getter }
function TOverlay.getMap(Map:TWvWMapType):TMap2DOverlay;
begin
  Result:=F2DMaps[Map];
end;

function TOverlay.getStructVisible(Typ:TWvWObjectiveType):Boolean;
begin
  Result:=FTimer.Structures[Typ];
end;

{ TOverlay - Setter }
procedure TOverlay.setShowHint(Value:Boolean);
begin
  FNotify.ShowHint:=Value;
  FStatistics.ShowHint:=Value;
end;

procedure TOverlay.setServer(Value:Integer);
begin
  FServer:=Value;
  FWebAPI.Server:=Value;
  FTimer.Reset;
end;

procedure TOverlay.setStructVisible(Typ:TWvWObjectiveType;Value:Boolean);
var mt    :TWvWMapType;
begin
  FTimer.Structures[Typ]:=Value;
  for mt in TWvWMapType do
    if mt <> MT_NONE then
      F2DMaps[mt].Structures[Typ]:=Value;
end;


end.

