{
  Created by: draos.9574

  Implements the symbols on the overlay.
}

unit uSymbols;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Controls, dateutils, uData, Windows,
  ExtCtrls, uOption, uTypes, LMessages, uWebAPI, fgl;

type
  { TSymbol }
  // Basic class for the symbols
  TSymbol           = class(TPaintbox)
  public
    constructor     Create(TheOwner:TComponent);override;
    destructor      Destroy;override;
    procedure       SetSize(Size:Integer);virtual;
    procedure       SetPosition(x, y:Single);virtual;
  protected
    procedure       PaintProc(Sender:TObject);virtual;
    procedure       CMHitTest(var Message: TCMHittest); message CM_HITTEST;
  end;

  { TDirectionSymbol }
  // Implements the direction of the compass
  TDirectionSymbol  = class(TSymbol)
  private
    FDirection      :TDirection;
    procedure       setDirection(dir:TDirection);
  protected
    procedure       PaintProc(Sender:TObject);override;
  public
    constructor     Create(TheOwner:TComponent);override;
    destructor      Destroy;override;
    property        Direction:TDirection read FDirection write setDirection;
  end;

  { TObjectSymbol }
  // Implements the symbols of the wvw objects
  TObjectSymbol     = class(TSymbol)
  private
    FList           :TImageList;
    FTime           :PTimeCallback;
    FMObj           :TAPIMObjective;
    FObj            :TAPIObjective;
    FGuild          :TAPIGuild;
    FForce          :Boolean;
    FShield         :TImage;
    FChanges        :Integer;
    FWebAPI         :TWebAPI;
    FName           :String;

    procedure       SetWebAPI(WebAPI:TWebAPI);
    procedure       SetForce(Value:Boolean);
    procedure       CheckData;
    procedure       CheckName;
  protected
    procedure       PaintProc(Sender:TObject);override;
    procedure       CMHitTest(var Message: TCMHittest); message CM_HITTEST;reintroduce;
  public
    constructor     Create(TheOwner:TComponent;ImageList:TImageList;TimeCallback:PTimeCallback;Obj:TAPIMObjective);overload;
    destructor      Destroy;override;

    procedure       Reload;
    procedure       SetSize(Size:Integer);override;

    property        ForceVisible:Boolean read FForce write SetForce;
    property        Objective:TAPIMObjective read FMObj;
    property        Data:TAPIObjective read FObj;
    property        Shield:TImage write FShield;
    property        WebAPI:TWebAPI write setWebAPI;
  end;

  TObjSymList       = specialize TFPGMap<String, TObjectSymbol>;

implementation

{ TSymbol }
constructor TSymbol.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  // Set painting procedure
  OnPaint:=@PaintProc;

  // Set standard settings of form
  Left:=0;
  Top:=0;
  Width:=64;
  Height:=64;
  Visible:=False;
end;

destructor TSymbol.Destroy;
begin
  inherited;
end;

procedure TSymbol.CMHitTest(var Message: TCMHittest);
begin
  inherited;
  Message.Result:=HTNOWHERE;
end;

procedure TSymbol.SetSize(Size:Integer);
begin
  SetBounds(Left-(Size-Width) div 2,Top-(Size-Height)div 2, Size, Size);
  Refresh;
end;

procedure TSymbol.SetPosition(x, y:Single);
begin
  SetBounds(Round(x-Width/2),Round(y-Height/2), Width, Height);
  Refresh;
end;

procedure TSymbol.PaintProc(Sender:TObject);
begin
  Canvas.Brush.Style:=bsClear;
  Canvas.Pen.Style:=psClear;
  Canvas.Rectangle(0, 0, Width, Height);
end;


{ TDirectionSymbol }
constructor TDirectionSymbol.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);
  Visible:=True;
  FDirection:=D_NONE;
end;

destructor TDirectionSymbol.Destroy;
begin
  inherited;
end;

procedure TDirectionSymbol.setDirection(dir:TDirection);
begin
  if dir <> D_NONE then
    FDirection:=dir;
  Refresh;
end;

procedure TDirectionSymbol.PaintProc(Sender:TObject);
var poly   :Array[0..3]of TPoint;
    cx, cy :Integer;
begin
  inherited PaintProc(Sender);

  cx:=min(Width, Height) div 2;
  cy:=cx div 2;

  Canvas.Pen.Color:=Colors.Back;
  Canvas.Brush.Color:=Colors.Back;
  Canvas.Brush.Style:=bsSolid;
  Canvas.EllipseC(Width div 2, Height div 2, cx, cx);

  Canvas.Brush.Color:=Colors.Front;
  Canvas.Pen.Color:=Colors.Front;
  poly[0].x:=Width div 2;    poly[0].y:=Height div 2-cx;
  poly[1].x:=Width div 2+cy; poly[1].y:=Height div 2;
  poly[2].x:=Width div 2;    poly[2].y:=Height div 2+cx;
  poly[3].x:=Width div 2-cy; poly[3].y:=Height div 2;
  Canvas.Polygon(poly);

  poly[0].x:=Width div 2-cx; poly[0].y:=Height div 2;
  poly[1].x:=Width div 2;    poly[1].y:=Height div 2+cy;
  poly[2].x:=Width div 2+cx; poly[2].y:=Height div 2;
  poly[3].x:=Width div 2;    poly[3].y:=Height div 2-cy;
  Canvas.Polygon(poly);

  Canvas.Rectangle(Width div 2-cy, Height div 2-cy, Width div 2+cy, Height div 2+cy);

  Canvas.Font.Color:=Colors.Font;
  Canvas.Font.Name:='Arial Black';
  Canvas.Brush.Style:=bsClear;
  Canvas.Font.Size:=cx;
  cx:=0;cy:=0;
  Canvas.GetTextSize(DirectionString[FDirection][Language], cx, cy);
  Canvas.TextOut(Width div 2-(cx div 2), Height div 2-(cy div 2),
                    DirectionString[FDirection][Language]);
end;


{ TObjectSymbol }
constructor TObjectSymbol.Create(TheOwner:TComponent;ImageList:TImageList;TimeCallback:PTimeCallback;Obj:TAPIMObjective);
begin
  inherited Create(TheOwner);
  FList:=ImageList;
  FTime:=TimeCallback;
  Canvas.Font.Size:=16;
  FName:='';
  FMObj:=Obj;
  FObj:=Nil;
  FGuild:=Nil;
  FShield:=Nil;
  FWebAPI:=Nil;
  ShowHint:=True;
  FChanges:=0;
end;

destructor TObjectSymbol.Destroy;
begin
  inherited;
end;

procedure TObjectSymbol.Reload;
begin
  FName:='';
  Refresh;
end;

procedure TObjectSymbol.CheckData;
begin
  if (FMObj = Nil) or (FWebAPI = Nil) then
    Exit;

  if ((FObj = Nil) or (FMObj.ID <> FObj.ID)) and (FMObj.ID <> '') then
    FObj:=FWebAPI.Objective[FMObj.ID];

  if FShield = Nil then
    Exit;

  if ((FGuild = Nil) or (FMObj.ClaimedBy <> FGuild.ID)) and (FMObj.ClaimedBy <> '') then
    FGuild:=FWebAPI.GuildByID[FMObj.ClaimedBy];
end;

procedure TObjectSymbol.CheckName;
  function check(const n, w:String; sc:TServerColor):String;inline;
  var tmp  :String;
  begin
    if Pos(w, n) > 0 then begin
      tmp:=FWebAPI.WorldName[FWebAPI.CurrentMatch.Worlds[sc, 0], Language];
      Result:=StringReplace(n, w, tmp, [rfReplaceAll]);
    end else
      Result:=n;
  end;

begin
  // Check the data object
  if FObj = Nil then
    Exit;

  // Load the name
  if FName = '' then
    FName:=FObj.Names[Language];

  // Check the WebAPI binding
  if FWebAPI = Nil then
    Exit;

  // Check for Red world tag
  FName:=check(FName, '<RED>', SC_RED);
  FName:=check(FName, '<BLUE>', SC_BLUE);
  FName:=check(FName, '<GREEN>', SC_GREEN);
end;

procedure TObjectSymbol.CMHitTest(var Message: TCMHittest);
var x    :Integer;
begin
  inherited CMHitTest(Message);

  x:=(Width - FList.Width) div 2;

  if (x <= Message.XPos) and (Message.XPos <= x + FList.Width) and
     (0 <= Message.YPos) and (Message.YPos <= FList.Height) then
    Message.Result:=HTCLIENT
  else
    Message.Result:=HTNOWHERE;
end;

procedure TObjectSymbol.SetSize(Size:Integer);
var x, y  :Integer;
begin
  x:=Max(Size, Canvas.GetTextWidth('0:00'));
  y:=Size + Canvas.Font.Size;
  SetBounds(Left - (x - Width) div 2, Top - (y - Height) div 2, x, y);
  Refresh;
end;

procedure TObjectSymbol.PaintProc(Sender:TObject);
var t     :TDateTime;
    secs  :Int64;
    index :Integer;
    cx, cy:Integer;
    str   :String;

  function clamp(const x, size, a, b:Integer):Integer;
  begin
    if x < a then
      Result:=a
    else if b < x + size then
      Result:=b - size
    else
      Result:=x;
  end;

begin
  inherited PaintProc(Sender);

  CheckData;

  if (FTime = Nil) or (FList = Nil) or (FMObj = Nil) then
    Exit;

  t:=FTime();
  secs:=300 - SecondsBetween(FMObj.LastFlipped, t);

  if (secs < 0) and (not ForceVisibleObjects) and (not FForce) then
    Exit;

  index:=4 * FMObj.Offset;
  if index < 0 then
    Exit;

  case FMObj.Owner of
    OC_NONE: index:=index+2;
    OC_GREEN:index:=index+1;
    OC_BLUE: index:=index;
    OC_RED:  index:=index+3;
  end;

  // Draw the icon
  if (0 <= index) and (index < FList.Count) then
    FList.Draw(Canvas, (Width - FList.Width) div 2, 0, index);

  // Check the name data
  CheckName;

  // Create the hint
  Hint:='';
  if FObj <> Nil then begin
    str:=StringReplace(FName, '<RED>', '', [rfReplaceAll]);
    str:=StringReplace(str, '<BLUE>', '', [rfReplaceAll]);
    Hint:=StringReplace(str, '<GREEN>', '', [rfReplaceAll]);
    Hint:=Format('%s%s %s: ', [Hint, LineEnding, HintStrings[2][Language]]);
    Hint:=Hint + FormatDateTime('hh:nn dd-mm-yyyy', FMObj.LastFlipped);
  end;

  if (FShield <> Nil) and (FMObj.ClaimedBy <> '') then begin
    Canvas.Draw((Width - FList.Width - 16) div 2, 0, FShield.Picture.Graphic);

    if FGuild <> Nil then begin
      Hint:=Hint + LineEnding;
      Hint:=Format('%s %s: %s [%s]', [Hint, HintStrings[1][Language], FGuild.Name, FGuild.Tag]);
      Hint:=Hint + LineEnding;
      Hint:=Format('%s %s: ', [Hint, HintStrings[2][Language]]);
      Hint:=Hint + FormatDateTime('hh:nn:ss dd-mm-yyyy', FMObj.ClaimedAt);
    end;
  end;

  if (FMObj.ObjectiveType <> AOT_RUINS) and (secs >= 0) then begin
    str:=Format('%d:%.2d', [secs div 60, secs mod 60]);

    // Setup the style
    Canvas.Brush.Style:=bsSolid;
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Font.Color:=Colors.Font;

    // Get the size of the text
    cx:=0; cy:=0;
    Canvas.GetTextSize(str, cx, cy);
    // Force to be inside the viewable area
    cx:=clamp(Left + (Width - cx) div 2, cx, 0, Parent.ClientWidth);
    cy:=clamp(Top + (Height - cy) div 2, cy, 0, Parent.ClientHeight);

    // Show the text
    Canvas.TextOut(cx - Left, cy - Top, str);
  end;
end;

// Setter
procedure TObjectSymbol.SetForce(Value:Boolean);
begin
  FForce:=Value;
  Refresh;
end;

procedure TObjectSymbol.SetWebAPI(WebAPI:TWebAPI);
begin
  FWebAPI:=WebAPI;
  CheckData;
  Refresh;
end;

end.

