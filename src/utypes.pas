{
  Created by: draos.9574
}

unit uTypes;

{$mode objfpc}{$H+}

interface

uses Sockets, dateutils, sysutils;

type
  { Enumerations }
  // Compass directions
  TDirection        = (D_NORTH, D_EAST, D_SOUTH, D_WEST, D_NONE);

  // Image button types
  TImageType        = (IT_EXIT, IT_MIN, IT_MAX, IT_OPTION, IT_COLOR,
                       IT_VISIBLE, IT_MAP, IT_TIMER, IT_SETTINGS);

  // Reminder button types
  TReminderType     = (RT_START, RT_PAUSE, RT_STOP, RT_LOOP, RT_ADD, RT_DELETE);

  // API languages
  TLanguage         = (L_GERMAN, L_ENGLISH, L_FRENCH, L_SPANISH);
  TLanguages        = Array[TLanguage] of String;

  // Types of name databases (map names, world names, objectiv names)
  TNameDataType     = (ND_MAP, ND_WORLD, ND_OBJECTIV);

  // Notifier behaviour of multiple notifications
  TNotifyType       = (NT_DOWN, NT_UP);

  // bonus types
  TMatchBonusType   = (MBT_NONE, MBT_BLOODLUST);

  // server color
  TServerColor      = (SC_RED=0, SC_BLUE, SC_GREEN);

  // colors of wvw structures
  TMatchColor       = (MC_NONE, MC_GREEN, MC_BLUE, MC_RED);

  // colors of wvw structures
  TObjectiveColor   = (OC_NONE, OC_GREEN, OC_BLUE, OC_RED);

  // map types
  TWvWMapType       = (MT_GREEN=0, MT_BLUE=1, MT_RED=2, MT_ETERNAL=3, MT_NONE);

  // Objective types
  //   battle, bauer, carver, orchard and temple are the 5 ruins
  TObjectType       = (OT_BATTLE,OT_BAUER,OT_CAMP,OT_CARVER,OT_CASTLE, OT_KEEP, OT_ORCHARD,OT_TEMPLE,OT_TOWER);
  // API v2
  TWvWObjectiveType = (AOT_CAMP, AOT_TOWER, AOT_RUINS, AOT_KEEP, AOT_CASTLE,
                       AOT_SHRINE, AOT_GENERIC, AOT_NIL);

  // Professions
  TProfession       = (PF_UNKNOWN, PF_GUARDIAN, PF_WARRIOR, PF_ENGINEER,
                       PF_RANGER, PF_THIEF, PF_ELEMENTALIST, PF_MESMER,
                       PF_NECROMANCER, PF_REVENANT);

  // Race
  TRace             = (R_ASURA, R_CHARR, R_HUMAN, R_NORN, R_SYLVARI, R_UNKNOWN);

  { Callbacks }
  TProcCallback     = procedure of object;
  PTimeCallback     = function:TDateTime of object;

  { TObjectivData }
  TObjectivData     = record
    ID              :Integer;
    German          :String;
    English         :String;
    French          :String;
    Spanish         :String;
    Position        :Array[0..2] of Single;
    Map             :TWvWMapType;
    Typ             :TObjectType;
  end;

  TObjectiveData   = record
    ID              :Integer;
    Position        :Array[0..2] of Single;
  end;

  { TFieldOfView }
  TFieldOfView      = record
    Horizontal      :Single;
    Vertical        :Single;
  end;

  { TCoordinate }
  TCoordinate       = record
    x, y            :Integer;
    Visible         :Boolean;
  end;

  { TSRect }
  TSRect            = record
    Left, Top       :Single;
    Right, Bottom   :Single;
  end;


  { TMumbleInfo }
  // packed vector type, (x, y and z)
  TVector           = packed Array[0..2] of Single;

  // packed record for avatar data, position and direction
  TAvatar           = packed record
    Position        : TVector;
    Front           : TVector;
    Top             : TVector;
  end;
  TCamera           = TAvatar;

  // packed record for the context data
  TContextData      = packed record
    Address         : packed record
      case Boolean of
        True: (IPv4 : sockaddr_in);
        False:(IPv6 : sockaddr_in6);
    end;
    MapID           : DWORD;
    MapType         : DWORD;
    ShardID         : DWORD;
    Instance        : DWORD;
    BuildID         : DWORD;
  end;

  // Mumble names, identity and description
  TNameMem          = packed Array[0..255] of Widechar;
  TIdentityMem      = TNameMem;
  TDescriptionMem   = packed Array[0..2047] of Widechar;

  // mumble definition of the linked memory
  TLinkedMem        = packed record
    uiVersion       : DWORD;
    uiTick          : DWORD;
    Avatar          : TAvatar;
    Name            : TNameMem;
    Camera          : TCamera;
    Identity        : TIdentityMem;
    ContextLen      : DWORD;
    Context         : packed Array[0..255] of Char;
    Description     : TDescriptionMem;
  end;

  // internal mumble info
  TMumbleInfo       = record
    Name            : AnsiString;
    Profession      : TProfession;
    TeamColor       : Integer;
    Commander       : Boolean;
    FoVVertical     : Single;
    Race            : TRace;
    Map             : Integer;
    World           : Integer;
    Avatar          : TAvatar;
    Camera          : TCamera;
    Context         : TContextData;
  end;

  // definition of names
  TNames            = record
    ID              : Integer;
    Name            : TLanguages;
  end;

  { Pointer }
  PLinkedMem        = ^TLinkedMem;
  PAvatar           = ^TAvatar;
  PCamera           = ^TCamera;


  // Convert the string to a objective type or color
  function strToObjType(const s:String):TWvWObjectiveType;
  function strToObjColor(const s:String):TObjectiveColor;
  // Convert the string to a map type
  function strToMapType(const s:String):TWvWMapType;

  // Language conversions
  function langToStr(Lang:TLanguage):String;
  function strToLang(const s:String):TLanguage;

  // Conversion from string to timestamp
  function strToDateTime(const s:String):TDateTime;

  // Compare functions
  function CompareInt(const A, B:Integer):Integer;inline;

  function objTypeToPoints(obj:TWvWObjectiveType):Integer;inline;


implementation

function objTypeToPoints(obj:TWvWObjectiveType):Integer;inline;
begin
  case obj of
    AOT_CAMP:  Result:=5;
    AOT_TOWER: Result:=10;
    AOT_KEEP:  Result:=25;
    AOT_CASTLE:Result:=35;
  else
    Result:=0;
  end;
end;

function CompareInt(const A, B:Integer):Integer;inline;
begin
  Result:=A - B;
end;

// Conversion from string to timestamp
function strToDateTime(const s:String):TDateTime;
var tmp  :String;
begin
  if s = '' then begin
    Result:=0.0;
    Exit;
  end;

  // ToDo: Conversion of time zone
  tmp:=StringReplace(s, 'T', ' ', [rfReplaceAll]);
  tmp:=StringReplace(tmp, 'Z', '', [rfReplaceAll]);

  try
    Result:=UniversalTimeToLocal(ScanDateTime('yyyy-mm-dd hh:mm:ss', tmp));
  except
    Result:=0.0;
  end;
end;

// Conversion of the api language enum to string
function langToStr(Lang:TLanguage):String;
begin
  // Converts the api language to strings
  case Lang of
    L_ENGLISH: Result:='en';
    L_GERMAN:  Result:='de';
    L_SPANISH: Result:='es';
    L_FRENCH:  Result:='fr';
  else
    Result:='';
  end;
end;

function strToLang(const s:String):TLanguage;
begin
  case s of
    'en': Result:=L_ENGLISH;
    'de': Result:=L_GERMAN;
    'es': Result:=L_SPANISH;
    'fr': Result:=L_FRENCH;
  else
    Result:=L_ENGLISH;
  end;
end;

// Convert the string to a objective type or color
function strToObjType(const s:String):TWvWObjectiveType;
begin
  case s of
    'Camp': Result:=AOT_CAMP;
    'Tower': Result:=AOT_TOWER;
    'Ruins': Result:=AOT_RUINS;
    'Keep': Result:=AOT_KEEP;
    'Castle': Result:=AOT_CASTLE;
    'Shrine': Result:=AOT_SHRINE; // <- Only a guess
    'Generic': Result:=AOT_GENERIC;
  else
    Result:=AOT_NIL;
  end;
end;

function strToObjColor(const s:String):TObjectiveColor;
begin
  case s of
    'Red': Result:=OC_RED;
    'Blue': Result:=OC_BLUE;
    'Green': Result:=OC_GREEN;
  else
    Result:=OC_NONE;
  end;
end;

// Convert the string to a map type
function strToMapType(const s:String):TWvWMapType;
begin
  case s of
    'Center': Result:=MT_ETERNAL;
    'GreenHome': Result:=MT_GREEN;
    'BlueHome': Result:=MT_BLUE;
    'RedHome': Result:=MT_RED;
  else
    Result:=MT_ETERNAL;
  end;
end;

end.

