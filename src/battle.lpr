{
  Created by: draos.9574
}

program battle;

{$mode objfpc}{$H+}

uses
  Interfaces, // this includes the LCL widgetset
  Forms, uHUD, lnetbase;

{$R *.res}

begin
  Application.Title:='Battle HUD';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(THUD, HUD);
  Application.Run;
end.

