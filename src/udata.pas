{
  Created by: draos.9574

  Includes constant data.
}

unit uData;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uTypes;

const
  // title of the hud
  TITLE             :String='GuildWars2 Battle HUD';

  // alarm duration
  ALARM_DURATION    = 10;

  // Maximal shown clients in the TS3 viewer
  CLIENT_MAXIMUM = 10;
  // Fading time of the TS3 viewer
  FADE_TIME      = 3;

  // direction strings
  DirectionString   : Array[TDirection] of TLanguages = (
    ('N', 'N', 'N', 'N'),
    ('O', 'E', 'E', 'E'),
    ('S', 'S', 'S', 'S'),
    ('W', 'W', 'O', 'O'),
    ('-', '-', '-', '-')
  );

  // strings for boolean
  BoolString        : Array[Boolean] of TLanguages =(
    ('Aus', 'Off', 'Off', 'Off'),
    ('An', 'On', 'On', 'On')
  );

  // Map rectangles
  // TODO: Change metrics of new borderlands
  INCH_TO_METER     = 39.730;
  AlpineBorderlands : uTypes.TSRect = (
    Left:  -30720 / INCH_TO_METER;
    Top:   -43008 / INCH_TO_METER;
    Right:  30720 / INCH_TO_METER;
    Bottom: 43008 / INCH_TO_METER
  );
  DesertBorderlands : uTypes.TSRect = (
    Left:  -36854 / INCH_TO_METER;
    Top:   -36854 / INCH_TO_METER;
    Right:  36854 / INCH_TO_METER;
    Bottom: 36854 / INCH_TO_METER
  );

  EternalBattleground: uTypes.TSRect = (
    Left:  -36864 / INCH_TO_METER;
    Top:   -36864 / INCH_TO_METER;
    Right:  36864 / INCH_TO_METER;
    Bottom: 36864 / INCH_TO_METER
  );

  // Names of the profession
  SA_PROFESSION     :Array[TProfession] of TLanguages=(
    ('Unbekannt', 'Unknown', 'Inconnu', 'Desconocido'),
    ('Wächter', 'Guardian', 'Gardien', 'Guardián'),
    ('Krieger', 'Warrior', 'Guerrier', 'Guerrero'),
    ('Ingenieur', 'Engineer', 'Ingénieur', 'Ingeniero'),
    ('Waldläufer', 'Ranger', 'Rôdeur', 'Guardabosques'),
    ('Dieb', 'Thief', 'Voleur', 'Ladrón'),
    ('Elementarmagier', 'Elementalist', 'Élémentaliste', 'Elementalista'),
    ('Mesmer', 'Mesmer', 'Envoûteur', 'Hipnotizador'),
    ('Nekromant', 'Necromancer', 'Nécromant', 'Nigromante'),
    ('Widergänger', 'Revenant', 'Revenant', 'Retornado')
  );

  // Strings for the statistic module
  StatisticString   :Array[1..5] of TLanguages = (
    ('Welt', 'World', 'Serveur', 'Mundo'),
    ('Punkte', 'Points', 'Points', 'Puntos'),
    ('Tick', 'Tick', 'Tique', 'Garrapata'),
    ('KD', 'KD', 'KD', 'KD'),
    ('Siege', 'Victories', 'Victoire', 'Victorias')
  );

  // Strings for the 2d map module
  HintStrings       :Array[1..2] of TLanguages = (
    ('Gehalten von', 'Hold by', 'Tenir par', 'Mantenga por'),
    ('seit', 'since', 'depuis', 'desde')
  );

  // Strings for the user interface
  STRING_LANGUAGES   = 1;
  STRING_HELP_SYSTEM = 5;
  STRING_VISIBLE     = 6;
  STRING_MAPS        = 12;
  STRING_SETTINGS    = 17;
  STRING_COLORS      = 19;
  STRING_STRUCTURES  = 22;
  STRING_COMPASS     = 28;

  HUDString         :Array[1..30] of TLanguages = (
                    ('Deutsch', 'German', 'Allemand', 'Alemán'),
                    ('Englisch', 'English', 'Anglais', 'Inglés'),
                    ('Französisch', 'French', 'Français', 'Francés'),
                    ('Spanisch', 'Spanish', 'Espagnol', 'Español'),
                    // Index: 5
                    ('Hilfesystem', 'Help System', 'Système d''aide', 'Sistema de ayuda'),
                    // Index: 6
                    ('Kompass', 'Compass', 'Boussole', 'Brújula'),
                    ('Timer', 'Timer', 'Horloge', 'Reloj'),
                    ('Erinnerung', 'Reminder', 'Rappel', 'Recordatorio'),
                    ('Mitteilung', 'Notifications', 'Notifications', 'Notificaciones'),
                    ('Statistik', 'Statistic', 'Statistiques', 'Estadística'),
                    ('TeamSpeak 3', 'TeamSpeak 3', 'TeamSpeak 3', 'TeamSpeak 3'),
                    // Index: 12
                    ('Sperren', 'Lock', 'Bloquer', 'Cerrar'),
                    ('Ewige Schlachtfelder', 'Eternal Battleground', 'Champs de bataille éternels', 'Campos de batalla eternos'),
                    ('Grünes Grenzland', 'Green Borderland', 'Territoires frontaliers vert', 'Tierra fronteriza verde'),
                    ('Blaues Grenzland', 'Blue Borderland', 'Territoires frontaliers bleu', 'Tierra fronteriza azul'),
                    ('Rotes Grenzland', 'Red Borderland', 'Territoires frontaliers rouge', 'Tierra fronteriza rojo'),
                    // Index: 17
                    ('Sortiere TS3-Clients', 'Sort TS3 clients', 'Trier les clients de TS3', 'Filtrer les clients de TS3'),
                    ('Filtere TS3-Clients', 'Filter TS3 clients', 'Filtrer les clients de TS3', 'Filtrar los clientes de TS3'),
                    // Index: 19
                    ('Farbe 1 ändern', 'Change Color 1', 'Changer couleur 1', 'Cambiar de color 1'),
                    ('Farbe 2 ändern', 'Change Color 2', 'Changer couleur 2', 'Cambiar de color 2'),
                    ('Schriftfarbe ändern', 'Change Color of Font', 'Changer la coleur de la police', 'Cambiar el color de la fuente'),
                    // Index: 22
                    ('Immer sichtbar', 'Always visible', 'Toujours visible', 'Siempre visible'),
                    ('Ruinen', 'Ruins', 'Ruines', 'Ruinas'),
                    ('Lager', 'Camps', 'Camps', 'Campamentos'),
                    ('Türme', 'Towers', 'Tours', 'Torres'),
                    ('Festungen', 'Keeps', 'Forts', 'Fortalezas'),
                    ('Schloss', 'Castle', 'Château', 'Castillo'),
                    // Index: 28
                    ('Größe des Kompass', 'Size of the Compass', 'Taille de la boussole', 'Tamaño de la brújula'),
                    ('Größe der Richtungen', 'Size of the Directions', 'Taille des directions', 'Tamaño de las direcciones'),
                    ('Lautstärke der Erinnerung', 'Volume of the Reminder', 'Volume de le Rappel', 'Volumen del Recordatorio'));

  // Strings of the hints in all languages
  HUDHints          :Array[1..25] of TLanguages = (
                    ('Grundeinstellungen, wie Server oder Sprache',
                     'Basic options such as server or language',
                     'Paramètres de base, tels que les serveurs ou la langue',
                     'Ajustes básicos, tales como servidores o idioma'),

                    ('Farbeinstellungen',
                     'Color settings',
                     'Les paramètres de couleur',
                     'Ajustes de color'),

                    ('Angezeigte Werkzeuge',
                     'Displayed Tools',
                     'Outils affichées',
                     'Opciones de visualización'),

                    ('Allgemeine Einstellungen',
                     'General Settings',
                     'Réglages généraux',
                     'Configuración general'),

                    ('Einstellungen der Timer',
                     'Settings of the timer',
                     'Réglages de la minuterie',
                     'Configuración del temporizador'),

                    ('Wähle deinen Server.',
                     'Choose your server.',
                     'Choisissez votre serveur.',
                     'Elija el servidor.'),
                    // Index: 6
                    ('Wechselt die Programmsprache auf Deutsch.',
                     'Changes the program language to German.',
                     'Change la langue du programme d''allemand.',
                     'Cambia el idioma del programa al Alemán.'),

                    ('Wechselt die Programmsprache auf Englisch.',
                     'Changes the programm language to English.',
                     'Change la langue du programme d''anglais.',
                     'Cambia el idioma del programa al Inglés.'),

                    ('Wechselt die Programmsprache auf Französisch.',
                     'Changes the programm language to French.',
                     'Change la langue du programme de français.',
                     'Cambia el idioma del programa al Francés.'),

                    ('Wechselt die Programmsprache auf Spanisch.',
                     'Changes the programm language to spanish.',
                     'Change le programme de langue en espagnol.',
                     'Cambia el programa de lenguaje al español.'),
                    // Index: 10
                    ('Setzt die Sichtbarkeit des Kompasses.',
                     'Sets the visibility of the compass.',
                     'Définit la visibilité de la boussole.',
                     'Establece la visibilidad de la brújula.'),

                    ('Setzt die allgemeine Sichtbarkeit der Timer. Genauere Einstellungen unter Einstellungen der Timer.',
                     'Sets the overall visibility of the timer. Find more settings under settings the timer.',
                     'Définit la visibilité globale de la minuterie. Trouver plus de paramètres dans les paramètres de la minuterie.',
                     'Establece la visibilidad global del temporizador. Encuentra más en la configuración del temporizador.'),

                     ('Setzt die Sichtbarkeit für die Erinnerung.',
                     'Sets the visibility of the reminder.',
                     'Définit la visibilité de la rappel.',
                     'Establece la visibilidad de la recordatorio.'),

                    ('Schaltet das Mitteilungssystem ein oder aus.',
                     'Turns the notification system on or off.',
                     'Met le système de notification ou désactiver.',
                     'Enciende el sistema de notificación de encendido o apagado.'),

                    ('Schaltet das Statistiksystem ein oder aus.',
                     'Turns the statistic system on or off.',
                     'Met le système de statistiques ou désactiver.',
                     'Enciende el sistema de estadísticas de encendido o apagado.'),

                    ('', '', '', ''),
                    // Index: 16
                    ('Ändert den Abstand der Himmelsrichtungen vom Bildschirmmittelpunkt.',
                     'Changes the distance of the points of the compass from the center screen.',
                     'Modifie la distance entre les points de la boussole du centre de l''écran.',
                     'Cambia la distancia de los puntos de la brújula en la pantalla central.'),

                    ('Ändert die Größe der Himmelsrichtungen.',
                     'Changes the size of the compass.',
                     'Modifie la taille de la boussole.',
                     'Cambia el tamaño de la brújula.'),
                    // Index: 18
                    ('Zeigt WvW-Objekte immer an oder nur, wenn ein Timer bei diesen läuft.',
                     'Displays WvW objects always on or only when a timer is running in these.',
                     'Affiche WvW objets toujours ou seulement quand une minuterie est en marche dans ces.',
                     'Muestra los objetos de MvM  siempre o sólo cuando un temporizador está en marcha en estos.'),

                    ('Bestimmt die Sichtbarkeit der Ruinen der Grenzländer.',
                     'Determines the visibility of the ruins of the borderlands.',
                     'Détermine la visibilité des ruines des confins.',
                     'Determina la visibilidad de las ruinas de las tierras fronterizas.'),

                     ('Bestimmt die Sichtbarkeit der Lager.',
                      'Determines the visibility of the camps.',
                      'Détermine la visibilité du camps.',
                      'Determina la visibilidad de las Campamentos.'),

                     ('Bestimmt die Sichtbarkeit der Türme.',
                      'Determines the visibility of the towers.',
                      'Détermine la visibilité des tours.',
                      'Determina la visibilidad de las torres.'),

                     ('Bestimmt die Sichtbarkeit der Festungen.',
                      'Determines the visibility of the keeps.',
                      'Détermine la visibilité des forts.',
                      'Determina la visibilidad de los fuertes.'),

                     ('Bestimmt die Sichtbarkeit des Schlosses.',
                      'Determines the visibility of the castle.',
                      'Détermine la visibilité du château.',
                      'Determina la visibilidad del castillo.'),
                     // Index 25
                     ('Angezeigte 2D Karten',
                      'Displayed 2D maps',
                      'Cartes 2D affichés',
                      'Mostrar mapas 2D')
                    );

  ObjectiveCount = 89;
  ObjectiveData : Array[0.. ObjectiveCount - 1] of TObjectiveData = (
        // EternalBattleground Overlook
  	(ID:   1;Position: ( 165.6860,  58.5352,  413.1134)),
        // EternalBattleground Valley
  	(ID:   2;Position: ( 608.0992,  41.3291, -479.9063)),
        // EternalBattleground Lowland
  	(ID:   3;Position: (-543.5331,  19.4476, -484.1871)),
        // EternalBattleground Golanta Clearing
  	(ID:   4;Position: (-193.9530,   1.3293, -653.3503)),
        // EternalBattleground Pangloss Rise
  	(ID:   5;Position: ( 482.5245,  21.4973,  361.4861)),
        // EternalBattleground Speldan  Clearcut
  	(ID:   6;Position: (-397.7152,  12.9922,  479.5736)),
        // EternalBattleground Danelon Passage
  	(ID:   7;Position: ( 332.3159,  12.3847, -748.7082)),
        // EternalBattleground Umberglade Woods
  	(ID:   8;Position: ( 648.6434,   7.7752,  -82.0938)),
        // EternalBattleground Stonemist Castle
  	(ID:   9;Position: (  69.5003,  39.0033, -154.2469)),
        // EternalBattleground Rogue's Quarry
  	(ID:  10;Position: (-556.6607,  10.2369,  -72.1619)),
        // EternalBattleground Aldon's Ledge
  	(ID:  11;Position: (-657.7943,  33.3851, -278.8340)),
        // EternalBattleground Wildcreek Run
  	(ID:  12;Position: (-358.7660,  23.3952, -176.5565)),
        // EternalBattleground Jerrifer's Slough
  	(ID:  13;Position: (-419.5713,  38.1861, -653.9320)),
        // EternalBattleground Klovan Gully
  	(ID:  14;Position: (-198.1809,  12.6179, -455.7820)),
        // EternalBattleground Langor Gulch
  	(ID:  15;Position: ( 586.4061,  57.0831, -704.8352);),
        // EternalBattleground Quentin Lake
  	(ID:  16;Position: ( 217.3233,  26.7535, -600.0000)),
        // EternalBattleground Mendon's Gap
  	(ID:  17;Position: (-146.1044,  51.2163,  500.5023)),
        // EternalBattleground Anzalias Pass
  	(ID:  18;Position: (-185.5399,  42.1382,  152.1314)),
        // EternalBattleground Ogrewatch Cut
    (ID: 19; Position: ( 287.0618,  45.0179,  169.8036)),
        // EternalBattleground Veloka Slope
  	(ID:  20;Position: ( 363.2497,  63.2918,  517.6612)),
        // EternalBattleground Durious Gulch
  	(ID:  21;Position: ( 404.7101,  41.2506, -118.4968)),
        // EternalBattleground Bravost Escarpment
  	(ID:  22;Position: ( 777.6835,  54.2144, -279.8669);),
        // Blue Garrison
  	(ID:  23;Position: ( -12.8006,  71.1365,  148.7078)),
        // Blue Champion's demense
  	(ID:  24;Position: (   0.3187,   8.1183, -842.5349)),
        // Blue Redbriar
  	(ID:  25;Position: (-236.0173,  46.8504, -408.1160)),
        // Blue Greenlake
  	(ID:  26;Position: ( 307.3000,  40.0931, -450.9625)),
        // Blue Ascension Bay
  	(ID:  27;Position: (-636.3212,   7.6686, -175.3795)),
        // Blue Dawn's Eyrie
  	(ID:  28;Position: ( 368.8560, 116.7060,  390.3757)),
        // Blue The Spiritholme
  	(ID:  29;Position: (   3.7797,  93.3888,  878.3418)),
        // Blue Woodhaven
  	(ID:  30;Position: (-382.7714,  93.7735,  360.0314)),
        // Blue Askalion Hills
  	(ID:  31;Position: ( 718.3614,  79.0202, -126.4932)),
        // Red Etherion Hills
  	(ID:  32;Position: ( 718.3614,  79.0202, -126.4932)),
        // Red Dreaming Bay
  	(ID:  33;Position: (-636.3212,   7.6686, -175.3795)),
        // Red Victor's Lodge
  	(ID:  34;Position: (   0.3187,   8.1183, -842.5349)),
        // Red Greenbriar
  	(ID:  35;Position: (-236.0173,  46.8504, -408.1160)),
        // Red Bluelake
  	(ID:  36;Position: ( 307.3000,  40.0931, -450.9625)),
        // Red Garrison
  	(ID:  37;Position: ( -12.8006,  71.1365,  148.7078)),
        // Red Longview
  	(ID:  38;Position: (-382.7714,  93.7735,  360.0314)),
        // Red The Godsword
  	(ID:  39;Position: (   3.7797,  93.3888,  878.3418)),
        // Red Cliffside
  	(ID:  40;Position: ( 368.8560, 116.7060,  390.3757)),
        // Green Shadaran Hills
  	(ID:  41;Position: ( 718.3614,  79.0202, -126.4932)),
        // Green Redlake
  	(ID:  42;Position: ( 307.3000,  40.0931, -450.9625)),
        // Green Hero's Lodge
  	(ID:  43;Position: (   0.3187,   8.1183, -842.5349)),
        // Green Deadfall Bay
  	(ID:  44;Position: (-636.3212,   7.6686, -175.3795)),
        // Green Bluebriar
  	(ID:  45;Position: (-236.0173,  46.8504, -408.1160)),
        // Green Garrision
  	(ID:  46;Position: ( -12.8006,  71.1365,  148.7078)),
        // Green Sunnyhill
  	(ID:  47;Position: (-382.7714,  93.7735,  360.0314)),
        // Green Faithleap
  	(ID:  48;Position: (-518.0585,   0.7835,  283.0114)),
        // Green Bluevale Refuge
  	(ID:  49;Position: (-490.7298,  15.8117, -479.1939)),
        // Red Bluewater Lowlands
  	(ID:  50;Position: ( 578.7808,   0.2753, -509.4251)),
        // Red Astralholme
  	(ID:  51;Position: ( 584.3019,  37.9332,  319.1331)),
        // Red Arah's Hope
  	(ID:  52;Position: (-518.0585,   0.7835,  283.0114)),
        // Red Greenvale Refuge
  	(ID:  53;Position: (-490.7298,  15.8117, -479.1939)),
        // Green Foghaven
  	(ID:  54;Position: ( 584.3019,  37.9332,  319.1331)),
        // Green Redwater Lowlands
  	(ID:  55;Position: ( 578.7808,   0.2753, -509.4251)),
        // Green Titanpaw
  	(ID:  56;Position: (   3.7797,  93.3888,  878.3418)),
        // Green Cragtop
  	(ID:  57;Position: ( 368.8560, 116.7060,  390.3757)),
        // Blue Godslore
  	(ID:  58;Position: (-518.0585,   0.7835,  283.0114)),
        // Blue Redvale Refuge
  	(ID:  59;Position: (-490.7298,  15.8117, -479.1939)),
        // Blue Stargrove
  	(ID:  60;Position: ( 584.3019,  37.9332,  319.1331)),
        // Blue Greenwater Lowlands
  	(ID:  61;Position: ( 578.7808,   0.2753, -509.4251)),
        // Red Temple of Lost Prayers
  	(ID:  62;Position: (  -6.4569,  25.1960, -407.1718)),
        // Red Battle's Hollow
  	(ID:  63;Position: (-192.7919,   0.0989, -246.8664)),
        // Red Bauer's Estate
  	(ID:  64;Position: (-133.6674,  10.0566,  -20.2866)),
        // Red Orchard Overlook
  	(ID:  65;Position: ( 151.1097,  18.2448,  -54.0537)),
        // Red Carver's Ascent
  	(ID:  66;Position: ( 173.6754,  69.7258, -268.8283)),
        // Blue Carver's Ascent
  	(ID:  67;Position: ( 173.6754,  69.7258, -268.8283)),
        // Blue Orchard Overlook
  	(ID:  68;Position: ( 151.1097,  18.2448,  -54.0537)),
        // Blue Bauer's Estate
  	(ID:  69;Position: (-133.6674,  10.0566,  -20.2866)),
        // Blue Battle's Hollow
  	(ID:  70;Position: (-192.7919,   0.0989, -246.8664)),
        // Blue Temple of Lost Prayers
  	(ID:  71;Position: (  -6.4569,  25.1960, -407.1718)),
        // Green Carver's Ascent
  	(ID:  72;Position: ( 173.6754,  69.7258, -268.8283)),
        // Green Orchard Overlook
  	(ID:  73;Position: ( 151.1097,  18.2448,  -54.0537)),
        // Green Bauer's Estate
  	(ID:  74;Position: (-133.6674,  10.0566,  -20.2866)),
        // Green Battle's Hollow
  	(ID:  75;Position: (-192.7919,   0.0989, -246.8664)),
        // Green Temple of Lost Prayers
  	(ID:  76;Position: (  -6.4569,  25.1960, -407.1718)),
        // Edge of the Mist Inferno's Needle 77
        // Edge of the Mist Overgrown Fane 78
        // Edge of the Mist Thunder Hollow 79
        // Edge of the Mist Overgrown Fane Reactor 80
        // Edge of the Mist Arid Fortress Reactor 81
        // Edge of the Mist Thunder Hollow Reactor 82
        // Edge of the Mist Stonegaze Spire Reactor 83
        // Edge of the Mist Inferno's Needle Reactor 84
        // Edge of the Mist Tytone Perch Reactor 85
        // Edge of the Mist Stonegaze Spire 87
        // Edge of the Mist Arid Fortress 88
        // Edge of the Mist Tytone Perch 89
        // Edge of the Mist Altar 90
        // Edge of the Mist Observatory 91
        // Edge of the Mist Statuary 92
        // Edge of the Mist Forge 93
        // Edge of the Mist Shrine 94
        // Edge of the Mist Bell Tower 95
        // Edge of the Mist Airport 96
        // Edge of the Mist Workshop 97
        // Edge of the Mist Wurm Tunnel 98

        // Desert AlpineBorderlands
        // Camp Lab
        (ID: 99; Position: (  -4.7512,  73.0868,  616.6914)),
        // Camp Farmstead
        (ID: 100; Position: ( 693.8485, 119.8737, -485.7555)),
        // Camp Encampment
        (ID: 101; Position: (-710.8052,  98.1379, -488.1484)),
        // Tower Academy
        (ID: 102; Position: (-560.8146,  73.3163,  601.6830)),
        // Tower Necropolis
        (ID: 104; Position: ( 607.5590, 113.3530,  516.0849)),
        // Tower Depot
        (ID: 105; Position: ( 309.2567, 132.5272, -644.2277)),
        // Keep Fire
        (ID: 106; Position: (-867.1624,  94.5123,  -85.4783)),
        // Camp Roy's Refuge
        (ID: 109; Position: ( 823.1452,  26.1139,  289.2109)),
        // Tower Outpost
        (ID: 110; Position: (-309.9934, 140.4455, -509.3016)),
        // Keep Earth
        (ID: 113; Position: (  15.8286, 104.8925,  227.8764)),
        // Keep Air
        (ID: 114; Position: ( 887.1151, 108.6787, -129.1450)),
        // Camp Hideaway
        (ID: 115; Position: (-876.1656,  32.6799,  304.6629)),
        // Camp  Well
        (ID: 116; Position: (  -1.3416,  71.1203, -828.7597))

        // Shrine North Earth
        // (ID: xxx; Position: (  -6.5908,  34.6200,  487.4876)),
        // Shrine West Earth
        // (ID: xxx; Position: (-483.0930,  34.6537,  279.1079)),
        // Shrine East Earth
        // (ID: xxx; Position: ( 462.2623,  35.5670,  305.8841)),

        // Shrine North Fire
        // (ID: xxx; Position: (-479.8706,   9.3598,  101.9139)),
        // Shrine West Fire
        // (ID: xxx; Position: (-630.4661, 136.0139, -121.4350)),
        // Shrine East Fire
        // (ID: xxx; Position: (-454.6108,  27.4332, -252.7233)),


        // Shrine West Air
        // (ID: xxx; Position: ( 493.6552, 104.7927, -237.6593)),
        // Shrine South Air
        // (ID: xxx; Position: ( 845.3126,  76.1499, -408.3287)),
        // Shrine North Air
        // (ID: xxx; Position: ( 883.2534,  42.8008,  130.6469)),
    );


implementation

end.

