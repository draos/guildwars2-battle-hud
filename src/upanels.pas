unit uPanels;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, ExtCtrls, types, StdCtrls, Graphics, int, uOption,
  uButtons, uTypes, uData;

type
  TSpecialPanel = class(TPanel)
  private
    FRemovable  :Boolean;
    FPriority   :Integer;
    FOnDelete   :TNotifyEvent;

    procedure   PaintProc(Sender:TObject);virtual;
  public
    constructor Create(TheOwner:TComponent);override;

    procedure   DecPriority(Delta:Integer=1);virtual;
    procedure   IncPriority(Delta:Integer=1);virtual;
    procedure   UpdateColors;virtual;

    property    OnDelete:TNotifyEvent write FOnDelete;
    property    Priority:Integer read FPriority write FPriority;
    property    Removable:Boolean read FRemovable write FRemovable;
  end;

  { TAddPanel }
  TAddPanel     = class(TSpecialPanel)
  private
    procedure   {%H-}DecPriority({%H-}Delta:Integer=1);override;
    procedure   {%H-}IncPriority({%H-}Delta:Integer=1);override;
    procedure   PaintProc(Sender:TObject);override;
  public
    constructor Create(TheOwner:TComponent);override;
  end;

  { TNotifyPanel }
  TNotifyType   = (NT_FLIP, NT_CLAIM);
  TNotifyPanel  = class(TSpecialPanel)
  private
    FBitmap     :TBitmap;
    FIcoSize    :Integer;
    FMessage    :String;
    FGuild      :String;
    FShield     :TImage;
    FType       :TNotifyType;

    procedure   PaintProc(Sender:TObject);override;
  public
    constructor Create(TheOwner:TComponent;const Message:String;const Duration:Integer=ALARM_DURATION);overload;
    constructor Create(TheOwner:TComponent;const Message, Guild:String;const Duration:Integer=ALARM_DURATION);overload;
    destructor  Destroy;override;

    procedure   UpdateColors;override;
    procedure   DecPriority(Delta:Integer=1);override;
    procedure   IncPriority(Delta:Integer=1);override;

    property    IconSize:Integer read FIcoSize write FIcoSize;
    property    Graphic:TBitmap read FBitmap;
    property    Guild:TImage write FShield;
    property    Message:String read FMessage;
  end;

  { TSpeakerPanel }
  TSpeakerPanel = class(TSpecialPanel)
  private
    FName       :TLabel;
    FServer     :TLabel;
    FServerID   :Integer;
    FClientID   :Integer;
    FFadeOut    :Integer;

    function    getName:String;
    function    getServer:String;
    procedure   setClientName(Value:String);
    procedure   setFading(Value:Integer);
    procedure   setServer(Value:String);
  public
    constructor Create(TheOwner:TComponent;AName, AServer:String; ClientID, ServerID:Integer);overload;
    destructor  Destroy;override;

    procedure   UpdateColors;override;
    property    ClientID:Integer read FClientID;
    property    ClientName:String read getName write setClientName;
    property    Fading:Integer read FFadeOut write setFading;
    property    ServerName:String read getServer write setServer;
    property    ServerID:Integer read FServerID;
  end;


  TReminderState= (RS_EDITABLE, RS_RUNNING, RS_PAUSE);
  TReminderAlarm= procedure of object;

  { TReminderPanel }
  TReminderPanel=class(TSpecialPanel)
  private
    FAlarm      :TReminderAlarm;
    FDuration   :Integer;
    FState      :TReminderState;
    FName       :TEdit;
    FLName      :TLabel;
    FTime       :TEdit;
    FLTime      :TLabel;
    FButtons    :Array[TReminderType] of TReminderButton;

    function    getTimerName:String;

    procedure   setDuration(Value:Integer);
    procedure   setTimerName(Value:String);
    procedure   setState(State:TReminderState);

    procedure   KeyFilter(Sender:TObject;var Key:Char);

    procedure   ControlClick(Sender:TObject);
    procedure   Reset(State:TReminderState);
    procedure   UpdateTime;
    procedure   UpdateVisibility;
  public
    constructor Create(TheOwner:TComponent;Alarm:TReminderAlarm);overload;
    destructor  Free;

    procedure   DecPriority(Delta:Integer=1);override;
    procedure   IncPriority(Delta:Integer=1);override;

    procedure   UpdateColors;override;

    property    Duration:Integer read FDuration write setDuration;
    property    Name:String read getTimerName write setTimerName;
    property    State:TReminderState read FState write setState;
  end;

implementation

{ TAddPanel }
constructor TAddPanel.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  FPriority:=3600000;
  Height:=25;
end;

procedure TAddPanel.DecPriority(Delta:Integer);
begin
end;

procedure TAddPanel.IncPriority(Delta:Integer);
begin
end;

procedure TAddPanel.PaintProc(Sender:TObject);
var size  :TSize;
begin
  inherited PaintProc(Sender);

  Canvas.Brush.Style:=bsClear;
  Canvas.Font.Color:=Colors.Font;
  Canvas.Font.Name:='Lucida Console';

  size:=Canvas.TextExtent('+');
  Canvas.TextOut((Width - size.cx) div 2, (Height - size.cy) div 2, '+');
end;

{ TSpeakerPanel }
constructor TSpeakerPanel.Create(TheOwner:TComponent;AName, AServer:String; ClientID, ServerID:Integer);
begin
  inherited Create(TheOwner);
  Height:=40;
  FClientID:=ClientID;
  FServerID:=ServerID;
  FFadeOut:=-1;

  FName:=TLabel.Create(Self);
  with FName do begin
    SetBounds(5, 2, 0, 0);
    Font.Style:=[fsBold];
    Caption:=AName;
    Parent:=Self;
  end;

  FServer:=TLabel.Create(Self);
  with FServer do begin
    SetBounds(15, 22, 0, 0);
    Caption:=AServer;
    Parent:=Self;
  end;
end;

destructor TSpeakerPanel.Destroy;
begin
  FreeAndNil(FName);
  FreeAndNil(FServer);

  inherited Destroy;
end;

function TSpeakerPanel.getName:String;
begin
  Result:=FName.Caption;
end;

function TSpeakerPanel.getServer:String;
begin
  Result:=FServer.Caption;
end;

procedure TSpeakerPanel.setFading(Value:Integer);
begin
  FFadeOut:=Value;

  if FFadeOut = -1 then
    FName.Font.Style:=[fsBold]
  else
    FName.Font.Style:=[fsItalic];
end;

procedure TSpeakerPanel.setClientName(Value:String);
begin
  FName.Caption:=Value;
end;

procedure TSpeakerPanel.setServer(Value:String);
begin
  FServer.Caption:=Value;
end;

procedure TSpeakerPanel.UpdateColors;
begin
  Color:=Colors.Back;
  Font.Color:=Colors.Font;

  FName.Font.Color:=Colors.Font;

  inherited UpdateColors;
end;

{ TReminderPanel }
constructor TReminderPanel.Create(TheOwner:TComponent;Alarm:TReminderAlarm);
var rtb     :TReminderType;
begin
  inherited Create(TheOwner);

  FAlarm:=Alarm;
  FDuration:=10;
  Font.Name:='Lucida Console';

  FName:=TEdit.Create(TheOwner);
  with FName do begin
    Parent:=Self;
    SetBounds(5, 3, 100, 25);
    MaxLength:=10;
    AutoSize:=False;
    Color:=Colors.Back;
    ParentFont:=True;
    ParentColor:=True;
    Text:='Test';
  end;

  FTime:=TEdit.Create(TheOwner);
  with FTime do begin
    Parent:=Self;
    SetBounds(5, 30, 100, 20);
    AutoSize:=False;
    MaxLength:=8;
    Text:='00:00:10';
    OnKeyPress:=@KeyFilter;
    ParentColor:=True;
    ParentFont:=True;
  end;

  FLName:=TLabel.Create(TheOwner);
  with FLName do begin
    Parent:=Self;
    SetBounds(10, 7, 100, 28);
    Caption:='';
    Visible:=False;
  end;

  FLTime:=TLabel.Create(TheOwner);
  with FLTime do begin
    Parent:=Self;
    SetBounds(10, 32, 100, 32);
    Caption:='00:00:10';
    Visible:=False;
  end;

  for rtb in TReminderType do begin
    FButtons[rtb]:=TReminderButton.Create(Self, rtb);
    with FButtons[rtb] do begin
      Parent:=Self;
      case rtb of
        RT_START: begin
          Left:=Self.Width - 77;
          Top:=3;
        end;
        RT_PAUSE: begin
          Left:=Self.Width - 77;
          Top:=3;
        end;
        RT_STOP:begin
          Left:=Self.Width - 52;
          Top:=3;
        end;
        RT_LOOP:begin
          Left:=Self.Width - 27;
          Top:=3;
        end;
        RT_ADD:begin
          Left:=Self.Width - 52;
          Top:=Self.Height - 3 - Height;
        end;
        RT_DELETE:begin
          Left:=Self.Width - 27;
          Top:=Self.Height - 3 - Height;
        end;
      end;
      Height:=22;
      Width:=21;
      Selectable:=rtb = RT_LOOP;
      if rtb <> RT_LOOP then
        OnClick:=@ControlClick;
    end;
  end;

  Reset(RS_EDITABLE);
  ControlClick(Self);
  UpdateColors;
  FPriority:=-100;
end;

destructor TReminderPanel.Free;
var rtb    :TReminderType;
begin
  FreeAndNil(FTime);

  for rtb in TReminderType do
    FreeAndNil(FButtons[rtb]);

  FreeAndNil(FName);
end;

function TReminderPanel.getTimerName:String;
begin
  if FName <> Nil then
    Result:=FName.Text
  else
    Result:='';
end;

procedure TReminderPanel.ControlClick(Sender:TObject);
var i     :Integer;
    tmp   :String;
begin
  if Sender = FButtons[RT_START] then begin
    FState:=RS_RUNNING;
    UpdateVisibility;
  end else if Sender = FButtons[RT_PAUSE] then begin
    FState:=RS_PAUSE;
    UpdateVisibility;
  end else if Sender = FButtons[RT_STOP] then begin
    FState:=RS_PAUSE;
    FPriority:=FDuration;
    UpdateVisibility;
  end else if (Sender = FButtons[RT_DELETE]) and (FOnDelete <> Nil) then
    FOnDelete(Self)
  else if Sender = FButtons[RT_ADD] then begin
    FDuration:=0;
    for i:=0 to 2 do begin
      tmp:=Copy(FTime.Text, 3 * i + 1, 2);
      FDuration:=FDuration * 60 + StrToInt(tmp);
    end;

    Reset(RS_PAUSE);

    FTime.Visible:=False;
    FLTime.Visible:=True;
    FName.Visible:=False;
    FLName.Visible:=True;
    FLName.Caption:=FName.Text;
    UpdateVisibility;
  end;
end;

procedure TReminderPanel.setDuration(Value:Integer);
begin
  // Cap on 1 day
  FDuration:=Value mod 86400;

  FTime.Text:=Format('%.2D:%.2D:%.2D', [FDuration div 3600, (FDuration div 60) mod 60, FDuration mod 60]);
  FLTime.Caption:=FTime.Text;
end;

procedure TReminderPanel.setTimerName(Value:String);
begin
  FName.Text:=Value;
  FLName.Caption:=Value;
end;

procedure TReminderPanel.setState(State:TReminderState);
begin
  if State = RS_PAUSE then
    ControlClick(FButtons[RT_ADD]);
end;

procedure TReminderPanel.KeyFilter(Sender:TObject;var Key:Char);
begin
  if Sender = FTime then begin
    if FTime.SelStart in [2, 5] then
      Key:=#0
    else if FTime.SelStart = 0 then begin
      if not (Key in ['0'..'2']) then
        Key:=#0;
    end else if (FTime.SelStart = 1) and (FTime.Text[1] = '2') then begin
      if not (Key in ['0'..'3']) then
        Key:=#0;
    end else if FTime.SelStart mod 3 = 0 then begin
      if not (Key in ['0'..'5']) then
        Key:=#0;
    end else if not (Key in ['0'..'9']) then
      Key:=#0;

    if Key <> #0 then
      FTime.SelLength:=1
    else
      FTime.SelStart:=FTime.SelStart+1;
  end;
end;

procedure TReminderPanel.DecPriority(Delta:Integer);
begin
  if (Delta > 0) and (FState = RS_RUNNING) then begin
    Dec(FPriority, Delta);
    UpdateTime;
  end;

  if (FState = RS_RUNNING) and (FPriority < -ALARM_DURATION) then begin
    if not FButtons[RT_LOOP].Selected then
      Reset(RS_PAUSE)
    else
      Reset(RS_RUNNING);
  end else if (FPriority = 0) and (FAlarm <> Nil) and (FState = RS_RUNNING) then
    FAlarm();
end;

procedure TReminderPanel.IncPriority(Delta:Integer);
begin
  if (Delta > 0) and (FState = RS_RUNNING) then
    Inc(FPriority, Delta);
end;

procedure TReminderPanel.Reset(State:TReminderState);
begin
  FState:=State;
  FPriority:=FDuration;
  UpdateTime;
  UpdateVisibility;
end;

procedure TReminderPanel.UpdateTime;
var tmp   :Integer;
begin
  if FPriority < 0 then
    tmp:=0
  else
    tmp:=FPriority;

  FLTime.Caption:=Format('%0.2d:%0.2d:%0.2d', [
    (tmp div 3600) mod 100,
    (tmp div 60) mod 60,
    tmp mod 60
  ]);
end;

procedure TReminderPanel.UpdateVisibility;
begin
  FButtons[RT_START].Visible:=FState = RS_PAUSE;
  FButtons[RT_PAUSE].Visible:=FState = RS_RUNNING;
  FButtons[RT_STOP].Visible:=FState <> RS_EDITABLE;
  FButtons[RT_ADD].Visible:=FState = RS_EDITABLE;
  FButtons[RT_DELETE].Visible:=FState <> RS_EDITABLE;
end;

procedure TReminderPanel.UpdateColors;
begin
  Color:=Colors.Back;
  Font.Color:=Colors.Font;

  inherited UpdateColors;
end;

{ TNotifyPanel }
constructor TNotifyPanel.Create(TheOwner:TComponent;const Message:String;const Duration:Integer);
begin
  inherited Create(TheOwner);

  FType:=NT_FLIP;
  FGuild:='';
  FBitmap:=TBitmap.Create;
  FMessage:=Message;
  FIcoSize:=32;
  Priority:=Duration;
end;

constructor TNotifyPanel.Create(TheOwner:TComponent;const Message, Guild:String;const Duration:Integer=ALARM_DURATION);
begin
  inherited Create(TheOwner);

  FType:=NT_CLAIM;
  FBitmap:=TBitmap.Create;
  FGuild:=Guild;
  FMessage:=Message;
  FIcoSize:=32;
  Priority:=Duration;
end;

destructor TNotifyPanel.Destroy;
begin
  FreeAndNil(FBitmap);
  inherited Destroy;
end;

procedure TNotifyPanel.DecPriority(Delta:Integer);
begin
  if Delta > 0 then
    Dec(FPriority, Delta);

  if FPriority < 0 then
    FRemovable:=True;
end;

procedure TNotifyPanel.IncPriority(Delta:Integer);
begin
  if Delta > 0 then
    Inc(FPriority, Delta);
end;

procedure TNotifyPanel.PaintProc(Sender:TObject);
var rect  :types.TRect;
    size  :TSize;
begin
  inherited PaintProc(Sender);

  rect.Left:=5;
  rect.Top:=(Height - FIcoSize) div 2;
  rect.Right:=5 + FIcoSize;
  rect.Bottom:=(Height + FIcoSize) div 2;

  if (FBitmap.Width <> 0) and (FBitmap.Height <> 0) then
    Canvas.StretchDraw(rect, FBitmap);

  if (FShield <> Nil) and (FType = NT_CLAIM) then begin
    rect.Left:=2;
    rect.Top:=2;
    rect.Right:=rect.Left + FShield.Width;
    rect.Bottom:=rect.Top + FShield.Height;

    Canvas.StretchDraw(rect, FShield.Picture.Graphic);
  end;

  size:=Canvas.TextExtent(FMessage);
  Canvas.Font.Height:=-13;
  Canvas.Font.Color:=Colors.Font;
  Canvas.Brush.Style:=bsClear;
  if FType = NT_FLIP then
    Canvas.TextOut(10 + FIcoSize, (Height - size.cy) div 2, FMessage)
  else begin
    Canvas.Font.Height:=-11;
    Canvas.TextOut(10 + FIcoSize, (Height div 2 - size.cy) div 2, FMessage);
    size:=Canvas.TextExtent(FGuild);
    Canvas.TextOut(15 + FIcoSize, Height div 2 + (Height div 2 - size.cy) div 2, FGuild);
  end;
end;

procedure TNotifyPanel.UpdateColors;
begin
  inherited UpdateColors;
end;

 { TSpecialPanel}
constructor TSpecialPanel.Create(TheOwner:TComponent);
begin
  inherited Create(TheOwner);

  BorderStyle:=bsNone;
  FRemovable:=False;
  Height:=54;
  Width:=200;
  FPriority:=0;
  FOnDelete:=Nil;
  OnPaint:=@PaintProc;
end;

procedure TSpecialPanel.DecPriority(Delta:Integer=1);
begin
  if Delta > 0 then
    Dec(FPriority, Delta);
end;

procedure TSpecialPanel.IncPriority(Delta:Integer=1);
begin
  if Delta > 0 then
    Inc(FPriority, Delta);
end;

procedure TSpecialPanel.PaintProc(Sender:TObject);
begin
  Canvas.Brush.Style:=bsSolid;
  Canvas.Brush.Color:=Colors.Back;
  Canvas.Pen.Color:=Colors.Front;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
end;

procedure TSpecialPanel.UpdateColors;
begin
  Refresh;
end;

end.

