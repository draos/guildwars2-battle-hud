{
  Created by: draos.9574

  Implements the buttons used by the hud.
}

unit uButtons;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Buttons, uOption, Graphics, uTypes, uData;

type
  { TOwnButton }
  // Simple toogle buttons with use of the color theme
  TOwnButton        = class(TSpeedButton)
  private
    FToogle         :Boolean;
    FShowToogle     :Boolean;
    FSelectable     :Boolean;
    FText           :Integer;
  protected
    procedure       Paint;override;
    procedure       setToogle(Toogle:Boolean);
    procedure       setShowToogle(Value:Boolean);
  public
    constructor     Create(TheOwner: TComponent;Value:Integer);overload;
    procedure       Toogle;
    procedure       Click;override;
    property        ShowSelected:Boolean read FShowToogle write setShowToogle;
    property        Selected:Boolean read FToogle write setToogle;
    property        Selectable:Boolean read FSelectable write FSelectable;
  end;

  { TColorButton }
  // Normal button with spezific background color and inverted font color
  TColorButton      = class(TOwnButton)
  private
    FColor          :TColor;
  protected
    procedure       Paint;override;
    procedure       setColors(Value:TColor);
  public
    constructor     Create(TheOwner:TComponent;Value:Integer);overload;
    property        Color:TColor read FColor write setColors;
  end;

  { TImageButton }
  // Button with images specific by TImageType
  TImageButton      = class(TSpeedButton)
  private
    FType           :TImageType;
    FColor          :TColor;
  protected
    procedure       Paint;override;
  public
    constructor     Create(TheOwner:TComponent;Value:TImageType);overload;

    property        Color:TColor read FColor write FColor;
  end;

  // Button with images specific by TReminderButton
  TReminderButton   = class(TOwnButton)
  private
    FType           :TReminderType;
  protected
    procedure       Paint;override;
  public
    constructor     Create(TheOwner:TComponent;Value:TReminderType);overload;
  end;

implementation

function min(a, b:Integer):Integer;
begin
  if a < b then
    Result:=a
  else
    Result:=b;
end;

{ TReminderButton }
constructor TReminderButton.Create(TheOwner:TComponent;Value:TReminderType);
begin
  FType:=Value;
  inherited Create(TheOwner);
end;

procedure TReminderButton.Paint;
var size  :Integer;
    poly  :TPoint;
    c     :UTF8String;
begin
  size:=min(Width, Height)-4;

  if Selected then begin
    Canvas.Brush.Color:=Colors.Front;
    Canvas.Pen.Color:=Colors.Back;
    Canvas.Font.Color:=Colors.Back;
  end else begin
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Pen.Color:=Colors.Front;
    Canvas.Font.Color:=Colors.Front;
  end;

  poly.x:=0;
  poly.y:=0;

  Canvas.Brush.Style:=bsSolid;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
  Canvas.Pen.Width:=2;
  Canvas.Font.Size:=3*Size div 4;

  {$IF defined(win32)}
  Canvas.Font.Name:='Webdings';

  case FType of
    RT_START:
      c:='4';
    RT_PAUSE:
      c:=';';
    RT_STOP:
      c:='<';
    RT_DELETE:
      c:='r';
    RT_ADD:
      c:='a';
    RT_LOOP:
      c:='`';
  end;
  {$ELSE}
  case FType of
    RT_START:
      c:='▶';
    RT_PAUSE:
      c:='▮▮';
    RT_STOP:
      c:='◾';
    RT_DELETE:
      c:='✘';
    RT_ADD:
      c:='✔';
    RT_LOOP:
      c:='⟲';
  end;
  {$ENDIF}

  Canvas.GetTextSize(c, poly.x, poly.y);
  Canvas.TextOut((Width-poly.x) div 2, (Height-poly.y-1) div 2, c);

  Canvas.Brush.Style:=bsClear;
  Canvas.Pen.Color:=Colors.Front;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
end;

{ TImageButton }
constructor TImageButton.Create(TheOwner:TComponent;Value:TImageType);
begin
  FType:=Value;
  FColor:=Colors.Transparent;
  inherited Create(TheOwner);
end;

procedure TImageButton.Paint;
var size  :Integer;
    poly  :Array[0..3]of TPoint;
begin
  size:=min(Width, Height)-4;
  Canvas.Brush.Color:=Colors.Back;
  Canvas.Pen.Color:=Colors.Front;

  Canvas.Brush.Style:=bsSolid;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
  Canvas.Pen.Width:=2;
  if FColor <> Colors.Transparent then
    Canvas.Font.Color:=FColor
  else
    Canvas.Font.Color:=Colors.Front;
  Canvas.Font.Size:=3*Size div 4;
  Canvas.Font.Name:='Webdings';
  poly[0].x:=0;poly[0].y:=0;

  case FType of
    IT_EXIT:begin
      Canvas.GetTextSize('r', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'r');
    end;
    IT_MAX:begin
      Canvas.GetTextSize('1', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, '1');
    end;
    IT_MIN:begin
      Canvas.GetTextSize('2', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, '2');
    end;
    IT_OPTION:begin
      Canvas.GetTextSize(#$69, poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, #$69);
    end;
    IT_COLOR:begin
      Canvas.Pen.Style:=psClear;
      Canvas.Brush.Color:=Colors.Back;
      Canvas.Rectangle((Width-Size)div 2, (Height-Size)div 2, (Width+Size)div 2, (Height+Size)div 2);
      Canvas.Brush.Color:=Colors.Front;
      Canvas.Rectangle((Width div 2)-(Size div 6),(Height-Size)div 2, (Width+Size)div 2, (Height+Size)div 2);
      Canvas.Brush.Color:=Colors.Font;
      Canvas.Rectangle((Width div 2)+(Size div 6),(Height-Size)div 2, (Width+Size)div 2, (Height+Size)div 2);
    end;
    IT_TIMER:begin
      Canvas.GetTextSize('N', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'N');
    end;
    IT_VISIBLE:begin
      Canvas.GetTextSize('L', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'L');
    end;
    IT_SETTINGS:begin
      Canvas.GetTextSize(#$40, poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, #$40);
    end;
    IT_MAP:begin
      Canvas.GetTextSize('ü', poly[0].x, poly[0].y);
      Canvas.TextOut((Width-poly[0].x)div 2, (Height-poly[0].y)div 2, 'ü');
    end;
  end;

  Canvas.Brush.Style:=bsClear;
  Canvas.Pen.Color:=Colors.Front;
  Canvas.Pen.Style:=psSolid;
  Canvas.Rectangle(0, 0, Width, Height);
end;

{ TColorButton }
constructor TColorButton.Create(TheOwner:TComponent;Value:Integer);
begin
  FColor:=clWhite;
  inherited Create(TheOwner, Value);
  FShowToogle:=False;
end;

procedure TColorButton.setColors(Value:TColor);
begin
  FColor:=Value;
  Invalidate;
end;

procedure TColorButton.Paint;
var cx, cy     :Integer;
    s          :String;
begin
  cx:=0;cy:=0;
  Canvas.Brush.Color:=FColor;
  Canvas.Pen.Color:=FColor xor $FFFFFF;
  Canvas.Font.Color:=FColor xor $FFFFFF;
  Canvas.Rectangle(0, 0, Width, Height);

  if FShowToogle then
    s:=HUDString[FText][Language]+' '+BoolString[FToogle][Language]
  else
    s:=HUDString[FText][Language];
  Canvas.GetTextSize(s, cx, cy);
  Canvas.TextOut((Width-cx)div 2, (Height-cy)div 2, s);
end;

{ TOwnButton }
constructor TOwnButton.Create(TheOwner:TComponent;Value:Integer);
begin
  FToogle:=True;
  FText:=Value;
  FSelectable:=False;
  inherited Create(TheOwner);
end;

procedure TOwnButton.Toogle;
begin
  FToogle:=(not FToogle) and FSelectable;
end;

procedure TOwnButton.setToogle(Toogle:Boolean);
begin
  FToogle:=Toogle;
  Invalidate;
end;

procedure TOwnButton.setShowToogle(Value:Boolean);
begin
  FShowToogle:=Value;
  Invalidate;
end;

procedure TOwnButton.Click;
begin
  Toogle;
  inherited Click;
end;

procedure TOwnButton.Paint;
var cx, cy     :Integer;
    s          :String;
begin
  cx:=0;cy:=0;
  if FToogle then begin
    Canvas.Brush.Color:=Colors.Front;
    Canvas.Pen.Color:=Colors.Back;
  end else begin
    Canvas.Brush.Color:=Colors.Back;
    Canvas.Pen.Color:=Colors.Front;
  end;
  Canvas.Font.Color:=Colors.Font;
  Canvas.Rectangle(0, 0, Width, Height);

  if FShowToogle then
    s:=HUDString[FText][Language]+' '+BoolString[FToogle][Language]
  else
    s:=HUDString[FText][Language];
  Canvas.GetTextSize(s, cx, cy);
  Canvas.TextOut((Width-cx)div 2, (Height-cy)div 2, s);
end;

end.

