{
  Created by: draos.9574
}

unit uOption;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, uTypes;

type
  { TColors }
  // Background color: back
  // Front color: front
  // Font color: font
  TColors          =record
    Back,Front     ,
    Transparent    ,
    Font           ,
    Red            ,
    Green          ,
    Blue           ,
    Eternal        ,
    None           :TColor;
  end;

  TNotifyOption    =record
    Duration       :Integer;
    Proc           :procedure(const Msg:String;Ico:Integer=-1)of object;
  end;

  TLogger          =class
    FEnable        :Boolean;
    FFile          :TFileStream;
    FLines         :TStrings;
  public
    constructor    Create;
    destructor     Free;

    procedure      Log(s:String);

    property       Enable:Boolean read FEnable write FEnable;
    property       Lines:TStrings write FLines;
  end;

var
  // Minimal frame per seconds
  MinimalFPS           :Integer = 10;

  // Default language is german
  Language             :TLanguage = L_ENGLISH;

  // Force visible objects, hide timer if no invulnerable buff on object
  ForceVisibleObjects  :Boolean   = False;

  // Set the default colors
  Colors               :TColors = (
    // Back is Black
    Back        :$000000;
    // Front is Deep Pine (from GuildWars2 color api)
    Front       :$1A1A80;
    // Transparent color, all other colors should be clear the bits setted in transparent
    Transparent :$010101;
    // Font color is a light gray
    Font        :$EEEEEE;
    // Map Colors
    Red         :$2A28FF;
    Green       :$61Ef24;
    Blue        :$FFA804;
    Eternal     :$22AAAA;
    None        :$999999;
  );

  Notifier         :TNotifyOption = (Duration:5;Proc:Nil);

  Logger           :TLogger;


implementation

constructor TLogger.Create;
begin
  FFile:=Nil;
  FLines:=Nil;
  FEnable:=False;
end;

destructor TLogger.Free;
begin
  if FFile <> Nil then
    FreeAndNil(FFile);
end;

procedure TLogger.Log(s:String);
begin
  if Assigned(FLines) then
      FLines.Append(s);

  if FEnable then begin
    if FFile = Nil then
      FFile:=TFileStream.Create('battle_hud.log', fmCreate or fmOpenWrite);

    s:=s + LineEnding;
    FFile.Write(s[1], length(s));
  end;
end;

initialization
  Logger:=TLogger.Create;

end.

