unit uTs3View;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, uForms,
  ExtCtrls, uOption, uPanels, fgl, uTs3, uData;

type
  { TTs3Panels }
  TTs3Panels = specialize TFPGObjectList<TSpeakerPanel>;

  { TTs3Viewer }
  TTs3Viewer = class(TDragForm)
    Clock: TTimer;
    TopBox: TPanel;
    MainBox: TPanel;
    procedure ClockTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FList     :TTs3Panels;
    FPlugin   :TTs3Plugin;
    // Settings
    FEnabled  :Boolean;
    FActiveTab:Boolean;
    FSorted   :Boolean;
    FFades    :Integer;
    function  getAPIKey:String;
    // Setter
    procedure setAPIKey(Value:String);
    procedure setEnable(Value:Boolean);
    procedure setActiveTab(Value:Boolean);
    procedure setSorted(Value:Boolean);
    // Change events for active server tab
    procedure ChangeActiveServer(const AName:String; const ID:Integer);
    // Change events for client and server name
    procedure ChangeClientName(const AName:String; const ClientID, ServerID:Integer);
    procedure ChangeServerName(const AName:String; const ID:Integer);
    // Add and remove events
    procedure Add(const AName, AServer:String; const ClientID, ServerID:Integer);
    procedure Remove(const AName, AServer:String; const ClientID, ServerID:Integer);
    // Reorders the list by apply the sorting or filters
    procedure Reorder(UpdateServer:Boolean=True);
  public
    // Reconnect the plugin
    procedure Reconnect;
    // Update the color scheme
    procedure UpdateColors;

    property  APIKey:String read getAPIKey write setAPIKey;
    property  Enabled:Boolean read FEnabled write setEnable;
    property  ActiveTab:Boolean read FActiveTab write setActiveTab;
    property  Sorted:Boolean read FSorted write setSorted;
  end;

var
  Ts3Viewer: TTs3Viewer;

implementation

{$R *.lfm}

const
  // Default panel size
  PANEL_SIZE     = 40;

function CmpPanel(const p1, p2:TSpeakerPanel):Integer;
begin
  // Compare by priority
  Result:=p1.Priority - p2.Priority;

  // Compare by fading
  if Result = 0 then
    Result:=p1.Fading - p2.Fading;

  // Compare by name if equal
  if Result = 0 then
    Result:=CompareStr(p1.ClientName, p2.ClientName);

  // Compare by server if name and priority are equal
  if Result = 0 then
    Result:=CompareStr(p1.ServerName, p2.ServerName);
end;

function min(a, b:Integer):Integer;inline;
begin
  if a < b then
    Result:=a
  else
    Result:=b;
end;

{ TTs3Viewer }
procedure TTs3Viewer.FormCreate(Sender: TObject);
begin
  // Default settings
  FActiveTab:=False;
  FSorted:=False;
  FEnabled:=False;
  FFades:=0;
  // The TopBox is for drag and drop
  TopBox.OnMouseDown:=OnMouseDown;
  TopBox.OnMouseUp:=OnMouseUp;
  TopBox.OnMouseMove:=OnMouseMove;

  // Create the panel list
  FList:=TTs3Panels.Create;

  // Create the TS3 plugin
  FPlugin:=TTs3Plugin.Create;
  // Set the events of the plugin
  FPlugin.OnTalk:=@Add;
  FPlugin.OnMute:=@Remove;
  FPlugin.OnActiveChange:=@ChangeActiveServer;
  FPlugin.OnChangeClientName:=@ChangeClientName;
  FPlugin.OnChangeServerName:=@ChangeServerName;

  // Log
  Logger.log('Ts3Viewer created.');
end;

// Timer to fade out the speaking people
procedure TTs3Viewer.ClockTimer(Sender: TObject);
begin
  if FFades > 0 then
    Reorder(False);
end;

procedure TTs3Viewer.FormDestroy(Sender: TObject);
begin
  // Free the plugin and the panel list
  FreeAndNil(FPlugin);
  FreeAndNil(FList);

  // Log
  Logger.log('Ts3Viewer freed.');
end;

function TTs3Viewer.getAPIKey:String;
begin
  Result:=FPlugin.APIKey;
end;

// Setter
procedure TTs3Viewer.setAPIKey(Value:String);
begin
  FPlugin.APIKey:=Value;
  FPlugin.Reconnect;
end;

procedure TTs3Viewer.setEnable(Value:Boolean);
begin
  Visible:=Value;

  if not FEnabled then begin
    FEnabled:=Value;
    FPlugin.Enable:=Value;
  end;
end;

procedure TTs3Viewer.setActiveTab(Value:Boolean);
begin
  FActiveTab:=Value;
  // Reorder the list
  Reorder;
end;

procedure TTs3Viewer.setSorted(Value:Boolean);
begin
  FSorted:=Value;
  // Reorder the list
  Reorder;
end;

// Reconnect the plugin
procedure TTs3Viewer.Reconnect;
begin
  FPlugin.Reconnect;
end;

// Update the color scheme
procedure TTs3Viewer.UpdateColors;
begin
  // Reset the colors of the TopBox
  TopBox.Color:=Colors.Back;
  TopBox.Font.Color:=Colors.Font;

  // Refresh the form
  Refresh;
end;

// Change events for active server tab
procedure TTs3Viewer.ChangeActiveServer(const AName:String; const ID:Integer);
begin
  TopBox.Caption:=AName;

  Reorder(False);
end;

// Event if a client name has changed
procedure TTs3Viewer.ChangeClientName(const AName:String; const ClientID, ServerID:Integer);
var i     :Integer;
begin
  // Check all panels and modify the name
  for i:=0 to FList.Count-1 do
    if (ClientID = FList[i].ClientID)and(ServerID = FList[i].ServerID)and(AName <> FList[i].ClientName) then
      FList[i].ClientName:=AName;
end;

// Event if a server name has changed
procedure TTs3Viewer.ChangeServerName(const AName:String; const ID:Integer);
var i     :Integer;
begin
  // Check all panels and modify the name
  for i:=0 to FList.Count-1 do
    if (ID = FList[i].ServerID) and (FList[i].ServerName <> AName) then
      FList[i].ServerName:=AName;

  // The TopBox shows the current active server
  TopBox.Caption:=FPlugin.CurrentServer;
end;

// Event if a client begins to talk
procedure TTs3Viewer.Add(const AName, AServer:String; const ClientID, ServerID:Integer);
var i     :Integer;
    tmp   :TSpeakerPanel;
begin
  // Check if the client is currently speaking
  for i:=0 to FList.Count-1 do
    if (ClientID = FList[i].ClientID) and (ServerID = FList[i].ServerID) then begin
      // Update the client and server name if possible
      if AServer <> FList[i].ServerName then
        FList[i].ServerName:=AServer;
      if AName <> FList[i].ClientName then
        FList[i].ClientName:=AName;
      // Set the fading to the default -1
      FList[i].Fading:=-1;

      Exit;
    end;

  // Add the new panel with the speaking client
  tmp:=TSpeakerPanel.Create(Self, AName, AServer, ClientID, ServerID);
  // Update the colors and set the parent
  tmp.UpdateColors;
  tmp.Parent:=MainBox;
  // Add to the panel list
  FList.Add(tmp);
  // Reorder the list
  Reorder;
end;

// Event if a client stops talking
procedure TTs3Viewer.Remove(const AName, AServer:String; const ClientID, ServerID:Integer);
var i     :Integer;
begin
  // Iterate over all panels and remove the speaking client
  for i:=FList.Count-1 downto 0 do
    if (ClientID = FList[i].ClientID) and (ServerID = FList[i].ServerID) then begin
      if FList[i].Fading = -1 then
        Inc(FFades);
      FList[i].Fading:=FADE_TIME;
    end;

  // Reorder the list
  Reorder;
end;

// Reorder procedure
procedure TTs3Viewer.Reorder(UpdateServer:Boolean);
var i, t  :Integer;
begin
  // Control the fading
  for i:=FList.Count - 1 downto 0 do
    if FList[i].Fading = 0 then begin
      FList.Delete(i);
      Dec(FFades);
    end else if FList[i].Fading > 0 then
      FList[i].Fading:=FList[i].Fading - 1;

  // Sorts the list after priority, client name and server name
  if FSorted then
    FList.Sort(@CmpPanel);

  // Iterate over the list and set the new positions
  t:=0;
  for i:=0 to FList.Count - 1 do begin
    // Apply the visibility filter
    FList[i].Visible:=(not FActiveTab) or (FList[i].ServerID = FPlugin.CurrentServerID);

    // Set the bounds of the panel
    if FList[i].Visible then
      if FActiveTab then begin
        FList[i].SetBounds(0, t, MainBox.ClientWidth, PANEL_SIZE div 2);
        Inc(t, PANEL_SIZE div 2);
      end else begin
        FList[i].SetBounds(0, t, MainBox.ClientWidth, PANEL_SIZE);
        Inc(t, PANEL_SIZE);
      end;
  end;

  // Apply the new height to the form (current maximum are 6 visible speaking clients)
  if FList.Count > 0 then
    Height:=TopBox.Height + min(t, CLIENT_MAXIMUM * FList[0].Height)
  else
    Height:=TopBox.Height;

  // UpdateColors TopBox Caption
  if UpdateServer then
    TopBox.Caption:=FPlugin.CurrentServer;
  Self.DoShow;
  Refresh;
end;

end.

