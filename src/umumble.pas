{
  Created by: draos.9574

  Implements the mumble api with internal timer and callback.
}

unit uMumble;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, jsonparser, fpjson, uTypes, uOption,
  Windows, jsonscanner;

type
  // Event callbacks
  TMCommanderEvent  = procedure(Value:Boolean) of object;
  TMFoVEvent        = procedure(Value:Single) of object;
  TMNameEvent       = procedure(Value:AnsiString) of object;
  TMMapEvent        = procedure(Value:Integer) of object;
  TMProfessionEvent = procedure(Value:TProfession) of object;
  TMRaceEvent       = procedure(Value:TRace) of object;
  TMTeamColorEvent  = procedure(Value:Integer) of object;
  TMWorldEvent      = procedure(Value:Integer) of object;

{ TMumble }
  TMumble           = class
  private
    FFPSTimer       :TTimer;
    FTimer          :TIdleTimer;
    FData           :TMumbleInfo;
    FLinkedMem      :PLinkedMem;
    FLastTick       :DWORD;
    FFPS            :Integer;
    FMemFile        :Handle;
    FCallback       :TProcCallback;
    FError          :String;
    FLoaded         :Boolean;
    // Event handler
    FOnCommander    :TMCommanderEvent;
    FOnFoV          :TMFoVEvent;
    FOnName         :TMNameEvent;
    FOnMap          :TMMapEvent;
    FOnProfession   :TMProfessionEvent;
    FOnRace         :TMRaceEvent;
    FOnTeamColor    :TMTeamColorEvent;
    FOnWorld        :TMWorldEvent;

    // Getter
    function        getAvatar:TAvatar;
    function        getCamera:TCamera;
    function        getCommander:Boolean;
    function        getContext:TContextData;
    function        getEnable:Boolean;
    function        getFoV:Single;
    function        getName:AnsiString;
    function        getMap:Integer;
    function        getProfession:TProfession;
    function        getProfessionName:String;
    function        getRace:TRace;
    function        getRaceName:String;
    function        getTeamColor:Integer;
    function        getWorld:Integer;

    // Setter
    procedure       setCommander(Value:Boolean);
    procedure       setEnable(Value:Boolean);
    procedure       setFoV(Value:Single);
    procedure       setName(Value:AnsiString);
    procedure       setMap(Value:Integer);
    procedure       setProfession(Value:TProfession);
    procedure       setRace(Value:TRace);
    procedure       setTeamColor(Value:Integer);
    procedure       setWorld(Value:Integer);

    procedure       ConvertIdentity(Mem:TIdentityMem);
    procedure       Init;
    procedure       RefreshFPS(Sender:TObject);
    procedure       Refresh(Sender:TObject);
  public
    constructor     Create(Callback:TProcCallback;Period:Integer=25);
    destructor      Destroy;override;
    property        LastError:String read FError;
    property        Avatar:TAvatar read getAvatar;
    property        Camera:TCamera read getCamera;
    property        Commander:Boolean read getCommander;
    property        Context:TContextData read getContext;
    property        Enabled:Boolean read getEnable write setEnable;
    property        FoVVertical:Single read getFoV;
    property        FPS:Integer read FFPS;
    property        Name:AnsiString read getName;
    property        Map:Integer read getMap;
    property        Profession:TProfession read getProfession;
    property        ProfessionName:String read getProfessionName;
    property        Race:TRace read getRace;
    property        RaceName:String read getRaceName;
    property        TeamColor:Integer read getTeamColor;
    property        World:Integer read getWorld;
  end;


implementation

function Max(a, b:Longint):Longint;
begin
  if a < b then
    Result:=b
  else
    Result:=a;
end;

procedure memcpy(dest: Pointer; const source: Pointer; size: Integer);
var i      :Integer;
    s, d   :PByte;
begin
  s := PByte(source);
  d := PByte(dest);

  for i:=0 to size-1 do begin
    d^:=s^;
    Inc(d);
    Inc(s);
  end;
end;

{ TMumble}
// Constructor
constructor TMumble.Create(Callback:TProcCallback;Period:Integer);
begin
  inherited Create;

  // Create TTimer
  FTimer:=TIdleTimer.Create(Nil);
  FTimer.Enabled:=False;
  FTimer.Interval:=Max(0, Period);
  FTimer.OnTimer:=@Refresh;

  // Create the Timer fot the fps calculation
  FFPSTimer:=TTimer.Create(Nil);
  FFPSTimer.Enabled:=False;
  FFPSTimer.Interval:=1000;
  FFPSTimer.OnTimer:=@RefreshFPS;

  // Default handlers
  FOnCommander:=Nil;
  FOnFoV:=Nil;
  FOnName:=Nil;
  FOnMap:=Nil;
  FOnProfession:=Nil;
  FOnRace:=Nil;
  FOnTeamColor:=Nil;
  FOnWorld:=Nil;

  FLastTick:=0;
  FFPS:=0;

  // Set callback
  FCallback:=Callback;

  // Set default values
  FMemFile:=0;
  FLinkedMem:=Nil;
  FLoaded:=False;

  // Initialize
  Init;
end;

// Destructor
destructor TMumble.Destroy;
begin
  // Free timer
  FreeAndNil(FTimer);

  // Unmap linked memory
  if FLinkedMem <> nil then
     UnmapViewOfFile(FLinkedMem);

  // Close memory file handle
  if FMemFile <> 0 then
     CloseHandle(FMemFile);

  Logger.Log('Freed mumble.');
end;

// Initialize mumble
procedure TMumble.Init;
begin
  // Try to open file mapping
  if FMemFile = 0 then begin
    FMemFile := OpenFileMapping(FILE_MAP_ALL_ACCESS, False, PChar('MumbleLink'));
    Logger.log('Open file mapping '+BoolToStr(FMemFile <> 0, True)+'.');
  end;

  // else try to create file mapping
  if FMemFile = 0 then begin
    FMemFile:= CreateFileMapping(INVALID_HANDLE_VALUE, nil, PAGE_READWRITE, 0, sizeof(TLinkedMem), PChar('MumbleLink'));
    Logger.log('Create file mapping '+BoolToStr(FMemFile <> 0, True)+'.');
  end;

  // Map the memory file
  if FMemFile <> 0 then begin
    FLinkedMem:=PLinkedMem(MapViewOfFile(FMemFile, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(TLinkedMem)));

    // Update memory
    if FLinkedMem = nil then begin
      CloseHandle(FMemFile);
      FMemFile:=0;
      Logger.log('Failed linking memory file.');
    end else begin
      Refresh(Nil);
      FLoaded:=True;
      Logger.log('Loaded mumble.');
    end;
  end;
end;

// Convert mumble identity and parse data
procedure TMumble.ConvertIdentity(Mem:TIdentityMem);
  function cmp(A,B:String):Boolean;
  begin
    Result:=(Pos(A,B)=1) and (Pos(B,A)=1);
  end;

var Key       :AnsiString;
    i          :Integer;
    json       :TJSONParser;
    j          :TJSONData;
begin
  json:=TJSONParser.Create(Mem, [joUTF8,joStrict]);
  j:=json.Parse;
  if (j<>Nil)and(not j.isNull)and(j.JSONType = jtObject) then begin
    for i:=0 to j.Count-1 do begin
      Key:=TJSONObject(j).Names[i];

      // Read avatar name
      if cmp('name', Key) and (j.Items[i].JSONType = jtString) then
         setName(j.Items[i].AsString)

      // Read avatar profession
      else if cmp('profession', Key) and (j.Items[i].JSONType = jtNumber) then
         setProfession(TProfession(j.Items[i].AsInteger))

      // Read avatar race
      else if cmp('race', Key) and (j.Items[i].JSONType = jtNumber) then
         setRace(TRace(j.Items[i].AsInteger))

      // Read current map id
      else if cmp('map_id', Key) and (j.Items[i].JSONType = jtNumber) then
         setMap(j.Items[i].AsInteger)

      // Read current world id
      else if cmp('world_id', Key) and (j.Items[i].JSONType = jtNumber) then
         setWorld(j.Items[i].AsInteger)

      // Read field of view
      else if cmp('fov', Key) and (j.Items[i].JSONType = jtNumber) then
         setFoV(j.Items[i].AsFloat)

      // Read team color
      else if cmp('team_color_id', Key) and (j.Items[i].JSONType = jtNumber) then
         setTeamColor(j.Items[i].AsInteger)

      // Read commander status
      else if cmp('commander', Key) and (j.Items[i].JSONType = jtBoolean) then
         setCommander(j.Items[i].AsBoolean)
    end;
    j.Free;
  end;
  json.Free;
end;

// Mumble refresh procedure, called by timer
procedure TMumble.Refresh(Sender:TObject);
var fcheck :Boolean;
begin
  fcheck:=FMemFile<>0;
  if (fcheck)and(FLinkedMem <> nil)and(FLinkedMem^.uiVersion=2) then begin
    // Fill mumble structure
    ConvertIdentity(FLinkedMem^.Identity);
    Memcpy(@(FData.Avatar), @(FLinkedMem^.Avatar), sizeof(TAvatar));
    Memcpy(@(FData.Camera), @(FLinkedMem^.Camera), sizeof(TCamera));
    Memcpy(@(FData.Context), @(FLinkedMem^.Context), sizeof(TContextData));

    // if loaded, run callback
    if (FCallback <> Nil)and(FLoaded) then
      FCallback;
  end else if (not fcheck)or(FLinkedMem=Nil) then
    // No linked memory, than reinitalize
    Init;
end;

// Mumble refresh fps procedure, called by timer
procedure TMumble.RefreshFPS(Sender:TObject);
var fcheck :Boolean;
begin
  fcheck:=FMemFile<>0;
  if (fcheck)and(FLinkedMem <> nil)and(FLinkedMem^.uiVersion=2) then begin
    // Only update if the last tick is valid
    if FLastTick <> 0 then
      FFPS:=(FLinkedMem^.uiTick - FLastTick);
    FLastTick:=FLinkedMem^.uiTick;
  end;
end;

// Getter
function TMumble.getAvatar:TAvatar;
begin
  if FLoaded then
    Result:=FData.Avatar;
end;

function TMumble.getCamera:TCamera;
begin
  if FLoaded then
    Result:=FData.Camera;
end;

function TMumble.getCommander:Boolean;
begin
  if FLoaded then
    Result:=FData.Commander
  else
    Result:=False;
end;

function TMumble.getContext:TContextData;
begin
  if FLoaded then
    Result:=FData.Context;
end;

function TMumble.getEnable:Boolean;
begin
  Result:=FTimer.Enabled;
end;

function TMumble.getFoV:Single;
begin
  Result:=FData.FoVVertical;
end;

function TMumble.getName:AnsiString;
begin
  if FLoaded then
    Result:=FData.Name
  else
    Result:='';
end;

function TMumble.getMap:Integer;
begin
  if FLoaded then
    Result:=FData.Map
  else
    Result:=0;
end;

function TMumble.getProfession:TProfession;
begin
  if FLoaded then
    Result:=FData.Profession
  else
    Result:=PF_UNKNOWN;
end;

function TMumble.getProfessionName:String;
begin
  case FData.Profession of
    PF_ELEMENTALIST:
      Result:='Elementalist';
    PF_ENGINEER:
      Result:='Engineer';
    PF_GUARDIAN:
      Result:='Guardian';
    PF_MESMER:
      Result:='Mesmer';
    PF_NECROMANCER:
      Result:='Necromancer';
    PF_RANGER:
      Result:='Ranger';
    PF_REVENANT:
      Result:='Revenant';
    PF_THIEF:
      Result:='Thief';
    PF_WARRIOR:
      Result:='Warrior';
  else
    Result:='';
  end;
end;

function TMumble.getRace:TRace;
begin
  Result:=FData.Race;
end;

function TMumble.getRaceName:String;
begin
  case FData.Race of
    R_ASURA:
      Result:='Asura';
    R_CHARR:
      Result:='Charr';
    R_HUMAN:
      Result:='Human';
    R_NORN:
      Result:='Norn';
    R_SYLVARI:
      Result:='Sylvari';
  else
    Result:='';
  end;
end;

function TMumble.getTeamColor:Integer;
begin
  if FLoaded then
    Result:=FData.TeamColor
  else
    Result:=0;
end;

function TMumble.getWorld:Integer;
begin
  if FLoaded then
    Result:=FData.World
  else
    Result:=0;
end;

// Setter

// Setter
procedure TMumble.setCommander(Value:Boolean);
begin
  if (FData.Commander <> Value) and Assigned(FOnCommander) then
    FOnCommander(Value);

  FData.Commander:=Value;
end;

procedure TMumble.setEnable(Value:Boolean);
begin
  FTimer.Enabled:=Value;
  FFPSTimer.Enabled:=Value;
end;

procedure TMumble.setFoV(Value:Single);
begin
  if (FData.FoVVertical <> Value) and Assigned(FOnFoV) then
    FOnFoV(Value);

  FData.FoVVertical:=Value;
end;

procedure TMumble.setName(Value:AnsiString);
begin
  if (FData.Name <> Value) and Assigned(FOnName) then
    FOnName(Value);

  FData.Name:=Value;
end;

procedure TMumble.setMap(Value:Integer);
begin
  if (FData.Map <> Value) and Assigned(FOnMap) then
    FOnMap(Value);

  FData.Map:=Value;
end;

procedure TMumble.setProfession(Value:TProfession);
begin
  if (FData.Profession <> Value) and Assigned(FOnProfession) then
    FOnProfession(Value);

  FData.Profession:=Value;
end;

procedure TMumble.setRace(Value:TRace);
begin
  if (FData.Race <> Value) and Assigned(FOnRace) then
    FOnRace(Value);

  FData.Race:=Value;
end;

procedure TMumble.setTeamColor(Value:Integer);
begin
  if (FData.TeamColor <> Value) and Assigned(FOnTeamColor) then
    FOnTeamColor(Value);

  FData.TeamColor:=Value;
end;

procedure TMumble.setWorld(Value:Integer);
begin
  if (FData.World <> Value) and Assigned(FOnWorld) then
    FOnWorld(Value);

  FData.World:=Value;
end;


end.


