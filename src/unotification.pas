unit uNotification;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  fgl, uPanels, uForms, uOption, uTypes, uData;

type
  TNotifierList         = specialize TFPGObjectList<TNotifyPanel>;

  { TNotification }
  TNotification = class(TDragForm)
    MainBox     : TPanel;
    TopBox      : TPanel;
    procedure   FormCreate(Sender: TObject);
    procedure   FormDestroy(Sender: TObject);
  private
    // The current notification type. Controls the direction of the list
    FType       :TNotifyType;
    // The icon list
    FIcons      :TImageList;
    // The claimed icon
    FGuild      :TImage;
    // The panel list
    FList       :TNotifierList;

    // Change the type of the notification system
    procedure   setType(Value:TNotifyType);
    // Sort and reorder the current notifications
    procedure   Sort;
    // Update the colors
    procedure   UpdateColors;
  public
    // Need an external timer
    procedure   Clock;
    // Create a new notificiation panel with the message and icon id
    procedure   Notify(const Msg:String;IconID:Integer=-1);
    // Create a new notificiation panel with the message, guild and icon id
    procedure   Notify(const Msg, Guild:String;IconID:Integer=-1);
    // Reload everything
    procedure   Reload;

    // The Icon List
    property    Guild:TImage write FGuild;
    property    Icons:TImageList write FIcons;
  end;

implementation

{$R *.lfm}

const
  NOTIFY_SIZE = 32;

// Compare function for TNotifyPanel in ascending priority
function CmpAsc(const p1, p2:TNotifyPanel):Integer;
begin
  // Sort with priority
  Result:=p1.Priority - p2.Priority;

  // If equal sort with message
  if Result = 0 then
    Result:=CompareStr(p1.Message, p2.Message);
end;

// Compare function for TNotifyPanel in descending priority
function CmpDesc(const p1, p2:TNotifyPanel):Integer;
begin
  // Sort with priority
  Result:=p2.Priority - p1.Priority;

  // If equal sort with message
  if Result = 0 then
    Result:=-CompareStr(p1.Message, p2.Message);
end;

{ TNotification }
procedure TNotification.FormCreate(Sender: TObject);
begin
  // Redirect the event handler to the TDragForm events
  TopBox.OnMouseDown:=OnMouseDown;
  TopBox.OnMouseMove:=OnMouseMove;
  TopBox.OnMouseUp:=OnMouseUp;

  // Create the panel list
  FList:=TNotifierList.Create;

  // Update the colors to the system colors
  UpdateColors;

  // Set the type to down
  setType(NT_DOWN);

  // Reload the panels
  Reload;

  // Logging entry
  Logger.log('Created Notification.');
end;

// Update the priorities of the panels and delete if possible
procedure TNotification.Clock;
var i, j  :Integer;
begin
  // Clean up the panels or decrease the priority
  i:=0;
  j:=0;
  while i < FList.Count do begin
    // If the priority is negative delete it
    if FList[i].Priority < 0 then begin
      FList.Delete(i);
      Inc(j);
    end else begin
      FList[i].DecPriority;
      Inc(i);
    end;
  end;

  // If changes are done resort
  if j > 0 then
    Sort;
end;

// Destructor of the form
procedure TNotification.FormDestroy(Sender: TObject);
begin
  // Free the list. The list will free all panels.
  FreeAndNil(FList);

  // Logger entry
  Logger.log('Freed Notification.');
end;

// Change the type of the notification system and resort
procedure TNotification.setType(Value:TNotifyType);
begin
  // Save the type
  FType:=Value;

  // Set the alignment of the TopBox
  case FType of
    NT_DOWN: TopBox.Align:=alTop;
    NT_UP:   TopBox.Align:=alBottom;
  end;

  // Resort the panels
  Sort;
end;

// Notification function. Call to add a new panel.
procedure TNotification.Notify(const Msg:String;IconID:Integer=-1);
var panel :TNotifyPanel;
    img   :TCustomBitmap;
begin
  if Msg = '' then
    Exit;

  // Create the new panel
  panel:=TNotifyPanel.Create(Self, Msg);
  // Set the parent to the MainBox
  panel.Parent:=MainBox;
  // Set the panel size and icon size
  panel.Height:=NOTIFY_SIZE;
  panel.IconSize:=24;

  // Set the alignment
  case FType of
    NT_DOWN: panel.Align:=alTop;
    NT_UP:   panel.Align:=alBottom;
  end;

  // Get the graphic of the panel and update if possible
  if (FIcons <> Nil)and(0 <= IconID)and(IconID < FIcons.Count) then begin
    // Get the image as TCustomBitmap
    img:=panel.Graphic as TCustomBitmap;
    // Read the icon
    FIcons.GetBitmap(IconID, img);
  end;

  // Add the panel to the list
  FList.Add(panel);

  // Resort
  Sort;
end;

// Notification function. Call to add a new panel.
procedure TNotification.Notify(const Msg, Guild:String;IconID:Integer=-1);
var panel :TNotifyPanel;
    img   :TCustomBitmap;
begin
  if Msg = '' then
    Exit;

  // Create the new panel
  panel:=TNotifyPanel.Create(Self, Msg, Guild);
  // Set the parent to the MainBox
  panel.Parent:=MainBox;
  // Set the panel size and icon size
  panel.Height:=NOTIFY_SIZE;
  panel.IconSize:=24;
  panel.Guild:=FGuild;

  // Set the alignment
  case FType of
    NT_DOWN: panel.Align:=alTop;
    NT_UP:   panel.Align:=alBottom;
  end;

  // Get the graphic of the panel and update if possible
  if (FIcons <> Nil)and(0 <= IconID)and(IconID < FIcons.Count) then begin
    // Get the image as TCustomBitmap
    img:=panel.Graphic as TCustomBitmap;
    // Read the icon
    FIcons.GetBitmap(IconID, img);
  end;

  // Add the panel to the list
  FList.Add(panel);

  // Resort
  Sort;
end;

// Sort the notifications and realign it
procedure TNotification.Sort;
var i     :Integer;
begin
  // Sort
  case FType of
    NT_DOWN: FList.Sort(@CmpAsc);
    NT_UP:   FList.Sort(@CmpDesc);
  end;

  // Reorder
  i:=0;
  while i < FList.Count do begin
    FList[i].SendToBack;

    Inc(i);
  end;

  // Update the window size (maximal 6 notifications are visible)
  if FList.Count > 6 then
    ClientHeight:=TopBox.Height + 6 * NOTIFY_SIZE
  else
    ClientHeight:=TopBox.Height + FList.Count * NOTIFY_SIZE;

  // Repaint the form
  Refresh;
end;

// Update the colors of the window
procedure TNotification.UpdateColors;
begin
  TopBox.Color:=Colors.Back;
  TopBox.Font.Color:=Colors.Font;

  MainBox.Color:=Colors.Transparent;

  // Repaint the form
  Refresh;
end;

// Reload the system
procedure TNotification.Reload;
begin
  // Update the title
  TopBox.Caption:=HUDString[STRING_VISIBLE+3][Language];

  // Update the colors
  UpdateColors;

  // Resort
  Sort;
end;

end.

