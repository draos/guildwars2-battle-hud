unit uTs3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, lNet, fgl, ExtCtrls, uOption, strutils;

type
  // States of the ts3 plugin
  TTs3State     = (tsConnected, tsConnecting, tsDisconnected);
  // Callback to a send function
  TSendCallback = function(const Msg:String):Boolean of object;
  // Event types of the plugin
  TTalkEvent    = procedure(const Name, Server:String; const ClientID, ServerID:Integer) of object;
  TServerEvent = procedure(const AName:String; const ID:Integer) of object;
  TClientEvent = procedure(const AName:String; const ClientID, ServerID:Integer) of object;

  { TTs3Client }
  TClientID     = Integer;
  TTs3Client    = class
  private
    FID         :Integer;
    FName       :String;
    FTalking    :Integer;
    FWhispering :Integer;
  public
    constructor Create(ClientID:Integer);

    property    ID:Integer read FID;
    property    Name:String read FName write FName;
    property    Talking:Integer read FTalking write FTalking;
    property    Whispering:Integer read FWhispering write FWhispering;
  end;

  TTs3Clients   = specialize TFPGMap<TClientID, TTs3Client>;

  { TTs3Server }
  TServerID     = Integer;
  TTs3Server    = class
  private
    FClients    :TTs3Clients;
    FID         :TServerID;
    FName       :String;
    FSend       :TSendCallback;

    FOnStart,
    FOnEnd      :TTalkEvent;
    FOnName     :TServerEvent;
    FOnCName    :TClientEvent;

    procedure   setName(Value:String);
    procedure   SendServername;
    procedure   SendClientList;
    procedure   SendClientVariable(ClID:Integer);
  public
    constructor Create(ID:TServerID; Send:TSendCallback; Talk:TTalkEvent=Nil; Mute:TTalkEvent=Nil;
                       CName:TClientEvent=Nil; SName:TServerEvent=Nil);
    destructor  Destroy; override;

    procedure   ChangeNickname(clid:Integer; Nick:String);
    procedure   ChangeFlags(clid, tflag, wflag:Integer);

    procedure   handleClientlist(Msg:String);

    procedure   Reload;
    procedure   Update(Lines:TStrings);

    property    Name:String read FName write setName;

    property    OnChangedClientName:TClientEvent write FOnCName;
    property    OnChangedServerName:TServerEvent write FOnName;
    property    OnTalk:TTalkEvent write FOnStart;
    property    OnMute:TTalkEvent write FOnEnd;
  end;

  TTs3Servers   = specialize TFPGMap<Integer, TTs3Server>;

  { TTs3Plugin }
  TTs3Plugin    = class
  private
    // Internal timer
    FUpdate     :TTimer;
    FTimer      :TTimer;
    // TCP socket
    FSock       :TLTcp;
    // Current state of the conenction
    FState      :TTs3State;
    // Active server handler ID
    FCurServer  :Integer;
    // Used server handler ID
    FUsedServer :Integer;
    // List of all TS3 servers
    FServers    :TTs3Servers;
    FEnable     :Boolean;
    // Last message
    FLast       :String;
    // Events if somebody starts or ends talking
    FOnStart,
    FOnEnd      :TTalkEvent;
    // API key
    FAPIKey     :String;

    // Other events
    FOnActive   :TServerEvent;
    FOnSName    :TServerEvent;
    FOnCName    :TClientEvent;

    // Getter
    function    getCurrentServer:String;

    // Setter
    procedure   setCNChange(Value:TClientEvent);
    procedure   setSNChange(Value:TServerEvent);
    procedure   setStart(Event:TTalkEvent);
    procedure   setEnd(Event:TTalkEvent);

    // Update functions
    procedure   UpdateServer(Sender:TObject);
    procedure   Reload(Sender:TObject);

    // Connect procedure
    procedure   DoConnect;
    // Connection event handler
    procedure   Connected(ASocket:TLSocket);
    procedure   Disconnected(ASocket:TLSocket);
    // Handler if the active server tab was changed
    procedure   ActiveServerChange(ServerID:Integer);
    // Handler if the connection status changed
    procedure   ConnectStatusChange(SID:Integer; Status:String; Error:Integer);

    // Handle the different input types
    procedure   handleClientlist(Msg:String);
    procedure   handleNotification(Msg:String);
    procedure   handleSelected(Msg:String);
    procedure   handleServer(Msg:String);
    procedure   handleServerlist(Msg:String);

    // Handler if a message or error is received
    procedure   Received(ASocket:TLSocket);
    procedure   Error(const Msg:String; ASocket:TLSocket);

    // General send function
    function    Send(const Msg:String):Boolean;
    // Specific send function for the server list
    procedure   SendServerList;
  public
    constructor Create(RefreshRate:cardinal=100);
    destructor  Destroy; override;

    // Force a reconnect
    procedure   Reconnect;

    // General properties
    property    APIKey:String read FAPIKey write FAPIKey;
    property    CurrentServer:String read getCurrentServer;
    property    CurrentServerID:Integer read FCurServer;
    property    Enable:Boolean read FEnable write FEnable;

    // Event properties
    property    OnActiveChange:TServerEvent write FOnActive;
    property    OnChangeClientName:TClientEvent write setCNChange;
    property    OnChangeServerName:TServerEvent write setSNChange;
    property    OnTalk:TTalkEvent write setStart;
    property    OnMute:TTalkEvent write setEnd;
  end;

implementation

const
  // Standard ts3 client query port
  TS3_PORT      = 25639;
  // Interval in ms for the server list update
  TS3_UPDATE    = 5000;

// Check if the string s ends with the substring with a given starting position
function endswith(const s, substr:String):Boolean;
var i, j :Integer;
begin
  Result:=Length(s) >= Length(substr);
  i:=Length(s);
  j:=Length(substr);

  while (0 < i) and (0 < j) do begin
    if s[i] <> substr[j] then begin
      Result:=False;
      Exit;
    end;

    Dec(i);
    Dec(j);
  end;
end;

// Check if the string s starts with the substring with a given starting position
function startswith(const s, substr:String):Boolean;
var i    :Integer;
begin
  Result:=Length(s) >= Length(substr);
  if not Result then
    Exit;

  i:=1;
  while (i <= Length(s)) and (i <= Length(substr)) do begin
    if s[i] <> substr[i] then
      Result:=False;
    Inc(i);
  end;
end;

function replaceLiterals(const s:String):String;
begin
  Result:=StringReplace(s, '\s', ' ', [rfReplaceAll]);
  Result:=StringReplace(Result, '\p', '|', [rfReplaceAll]);
  Result:=StringReplace(Result, '\\', '\', [rfReplaceAll]);
  Result:=StringReplace(Result, '\/', '/', [rfReplaceAll]);
end;

{ TTs3Plugin }
constructor TTs3Plugin.Create(RefreshRate:Cardinal);
begin
  // Default events
  FOnActive:=Nil;
  FOnCName:=Nil;
  FOnSName:=Nil;
  FOnStart:=Nil;
  FOnEnd:=Nil;

  // Default variables
  FEnable:=False;
  // Default state
  FState:=tsDisconnected;

  // Server Handler ID
  FCurServer:=0;
  FUsedServer:=0;

  FLast:='';

  // Create the server list
  FServers:=TTs3Servers.Create;
  FServers.Sorted:=True;

  // Open the ts socketwith event handler
  FSock:=TLTcp.Create(Nil);
  FSock.OnConnect:=@Connected;
  FSock.OnDisconnect:=@Disconnected;
  FSock.OnReceive:=@Received;
  FSock.OnError:=@Error;

  Logger.Log('Created TS3 Plugin.');

  // Create the timer
  FTimer:=TTimer.Create(Nil);
  FTimer.Interval:=RefreshRate;
  FTimer.OnTimer:=@Reload;

  // Create the update timer for the server list
  FUpdate:=TTimer.Create(Nil);
  FUpdate.Interval:=TS3_UPDATE;
  FUpdate.OnTimer:=@UpdateServer;

  // Connect and activate timer
  FUpdate.Enabled:=True;
  FTimer.Enabled:=True;
end;

// Destructor
destructor TTs3Plugin.Destroy;
var i      :Integer;
begin
  FreeAndNil(FSock);
  FreeAndNil(FTimer);

  for i:=FServers.Count-1 downto 0 do
    FServers.Data[i].Free;

  FreeAndNil(FServers);
end;

// Getter
function TTs3Plugin.getCurrentServer:String;
var k    :Integer;
begin
  // Search the current server handler ID
  if FServers.Find(FCurServer, k) then
    Result:=FServers.Data[k].Name
  else
    Result:='';
end;

// Function if the active server has changed
procedure TTs3Plugin.ActiveServerChange(ServerID:Integer);
var k     :Integer;
begin
  if FServers.Find(ServerID, k) then begin
    // Change the internal ID
    FCurServer:=k;
    // Call the event
    if Assigned(FOnActive) then
      FOnActive(FServers.Data[k].Name, ServerID);
  end;
end;

// Function if the connection status has changed
procedure TTs3Plugin.ConnectStatusChange(SID:Integer; Status:String; Error:Integer);
var k     :Integer;
begin
  // Check the current state
  if (Status = 'disconnected') and (FServers.Find(SID, k)) then begin
    // Delete the disconnected
    FServers.Data[k].Free;
    FServers.Delete(k);
  end;
end;

// Setter
procedure TTs3Plugin.setCNChange(Value:TClientEvent);
var i     :Integer;
begin
  FOnCName:=Value;
  for i:=0 to FServers.Count-1 do
    FServers.Data[i].OnChangedClientName:=Value;
end;

procedure TTs3Plugin.setSNChange(Value:TServerEvent);
var i     :Integer;
begin
  FOnSName:=Value;
  for i:=0 to FServers.Count-1 do
    FServers.Data[i].OnChangedServerName:=Value;
end;

procedure TTs3Plugin.setStart(Event:TTalkEvent);
var i     :Integer;
begin
  FOnStart:=Event;
  for i:=0 to FServers.Count-1 do
    FServers.Data[i].OnTalk:=Event;
end;

procedure TTs3Plugin.setEnd(Event:TTalkEvent);
var i     :Integer;
begin
  FOnEnd:=Event;
  for i:=0 to FServers.Count-1 do
    FServers.Data[i].OnMute:=Event;
end;

// Reload and check for actions on the socket
procedure TTs3Plugin.Reload(Sender:TObject);
begin
  // Call the socket actions
  FSock.CallAction;
end;

// Update timer for the server list (if connected)
procedure TTs3Plugin.UpdateServer(Sender:TObject);
var i     :Integer;
begin
  // Request the server handler list
  if FState = tsConnected then begin
    SendServerList;

    // Update the server names if unknown
    for i:=0 to FServers.Count-1 do
      if FServers.Data[i].Name = '' then
        FServers.Data[i].SendServername;
  end;

  // Reconnect if enabled
  if FEnable and (FState = tsDisconnected) then
    DoConnect;
end;

// Force a disconnect and connect again
procedure TTs3Plugin.Reconnect;
begin
  // Log it
  Logger.Log('TS3: Forced disconnect.');
  // Disconnect the socket
  FSock.Disconnect(True);
end;

// Connect to the ClientQuery
procedure TTs3Plugin.DoConnect;
begin
  FState:=tsConnecting;
  // Connect to the local TS3 Client Query
  FSock.Connect('127.0.0.1', TS3_PORT);
end;

// Connected event
procedure TTs3Plugin.Connected(ASocket:TLSocket);
begin
  Logger.Log('TS3: Connected to Client Query.');
  FState:=tsConnected;

  if FAPIKey <> '' then
    Send('auth apikey=' + FAPIKey);
  // List all servers
  SendServerList;
end;

// Error event
procedure TTs3Plugin.Error(const Msg:String;ASocket:TLSocket);
begin
  // Log it and disconnect
  Logger.Log('TS3: Disconnect because of error.');
  FState:=tsDisconnected;
end;

// Disconnected event
procedure TTs3Plugin.Disconnected(ASocket:TLSocket);
begin
  Logger.Log('TS3: Disconnected.');
  FState:=tsDisconnected;
end;

// Handle the selected server message
procedure TTs3Plugin.handleSelected(Msg:String);
var k     :Integer;
begin
  if SScanf(Msg, 'selected schandlerid=%d', [@k]) = 1 then begin
    FUsedServer:=k;

    if FCurServer = 0 then
      FCurServer:=k;
  end;
end;

// Handle the client lists
procedure TTs3Plugin.handleClientlist(Msg:String);
var k     :Integer;
begin
  if FServers.Find(FUsedServer, k) then
    FServers.Data[k].handleClientlist(Msg);
end;

// Handle notifications
procedure TTs3Plugin.handleNotification(Msg:String);
var clid, server, status, whisp:Integer;
    conn :String;
    k    :Integer;
// Known notifications
const
  talk    = 'notifytalkstatuschange schandlerid=%d status=%d isreceivedwhisper=%d clid=%d';
  connect = 'notifyconnectstatuschange schandlerid=%d status=%s error=%d';
  active  = 'notifycurrentserverconnectionchanged schandlerid=%d';
begin
  if SScanf(Msg, talk, [@server, @status, @whisp, @clid]) = 4 then begin
    if FServers.Find(server, k) then
      FServers.Data[k].ChangeFlags(clid, status, whisp);
  end else if SScanf(Msg, connect, [@server, @conn, @status]) = 3 then
    ConnectStatusChange(Server, conn, status)
  else if SScanf(Msg, active, [@server]) = 1 then
    ActiveServerChange(Server);
end;

// Handle the server name
procedure TTs3Plugin.handleServer(Msg:String);
var k     :Integer;
    n     :String;
const
  vname = 'virtualserver_name=%s';
begin
  if (SScanf(Msg, vname, [@n]) = 1) and FServers.Find(FUsedServer, k) then
    // Update the server name
    FServers.Data[k].Name:=replaceLiterals(n);
end;

// Handle the server list
procedure TTs3Plugin.handleServerlist(Msg:String);
var i, k, j :Integer;
    s       :TStrings;
begin
  s:=TStringList.Create;
  s.Delimiter:='|';
  s.StrictDelimiter:=True;
  s.DelimitedText:=Msg;
  k:=-1;

  for i:=0 to s.Count - 1 do
    if (SScanf(s[i], 'schandlerid=%d', [@k]) = 1) and (not FServers.Find(k, j)) then
      FServers.Add(k, TTs3Server.Create(k, @Send, FOnStart, FOnEnd, FOnCName, FOnSName));

  s.Free;
end;

// Received a message on the socket
procedure TTs3Plugin.Received(ASocket:TLSocket);
var msg   :String;
    k     :Integer;
    s     :TStrings;
begin
  if ASocket.GetMessage(msg) < 1 then
    Exit;

  // Handle fragmented messages
  Msg:=FLast + Msg;
  k:=RPos(#10#13, Msg);
  if 0 < k then begin
    FLast:=Copy(Msg, k + 2, Length(Msg) - k - 1);
    Msg:=Copy(Msg, 1, k - 1);
  end else begin
    FLast:=Msg;
    Exit;
  end;

  // Split the message into lines for better parsing results
  s:=TStringList.Create;
  s.AddText(StringReplace(Msg, '|', LineEnding, [rfReplaceAll]));

  for k:=0 to s.Count - 1 do begin
    if Length(s[k]) = 0 then
      Continue;

    // Log the messages
    Logger.Log('TS3:' + s[k]);

    // Handle the messages
    if startswith(s[k], 'notify') then
      handleNotification(s[k])
    else if startswith(s[k], 'schandlerid') then
      handleServerlist(s[k])
    else if startswith(s[k], 'clid') then
      handleClientlist(s[k])
    else if startswith(s[k], 'virtualserver') then
      handleServer(s[k])
    else if startswith(s[k], 'selected') then
      handleSelected(s[k]);
  end;

  s.Free;
end;

// Send a command
function TTs3Plugin.Send(const Msg:String):Boolean;
begin
  Result:=FState = tsConnected;
  if not Result then
    Exit;

  // Log the command
  Logger.log('TS3: >>>' + Msg);
  // Send the command
  FSock.SendMessage(Msg + LineEnding);
end;

// Request a server handler list
procedure TTs3Plugin.SendServerList;
begin
  Send('serverconnectionhandlerlist');
end;

{ TTs3Client }
constructor TTs3Client.Create(ClientID:Integer);
begin
  FID:=ClientID;
  FName:='';
  FTalking:=0;
  FWhispering:=0;
end;

{ TTs3Server }
constructor TTs3Server.Create(ID:TServerID; Send:TSendCallback; Talk:TTalkEvent;
                   Mute:TTalkEvent; CName:TClientEvent;SName:TServerEvent);
const FMT = 'clientnotifyregister schandlerid=%d event=%s';
begin
  FID:=ID;
  FSend:=Send;
  FOnStart:=Talk;
  FOnEnd:=Mute;
  FOnCName:=CName;
  FOnName:=SName;
  FClients:=TTs3Clients.Create;
  FClients.Sorted:=True;

  // Register talk and connection status changes
  FSend(Format(FMT, [FID, 'notifytalkstatuschange']));
  FSend(Format(FMT, [FID, 'notifyconnectstatuschange']));
  FSend(Format(FMT, [FID, 'notifycurrentserverconnectionchanged']));
  // The next line is only for debugging purposes
  // FSend(Format('clientnotifyregister schandlerid=%d event=any', [FID]));

  // Request the name of the server and the client list
  SendClientList;
  SendServername;
end;

// Destructor
destructor TTs3Server.Destroy;
var k      :Integer;
begin
  // Destroy all clients
  for k:=FClients.Count - 1 downto 0 do
    FClients.Data[k].Free;

  FreeAndNil(FClients);
end;

// Reload function
procedure TTs3Server.Reload;
begin
  // Currently only reload the client list
  SendClientList;
end;

procedure TTs3Server.Update(Lines:TStrings);
var i     :Integer;
begin
  for i:=0 to FClients.Count - 1 do
    if FClients.Data[i].Talking <> 0 then
      Lines.Append(FClients.Data[i].Name);
end;

procedure TTs3Server.ChangeNickname(clid:Integer; Nick:String);
var k     :Integer;
begin
  Nick:=ReplaceLiterals(Nick);

  if not FClients.Find(clid, k) then
    FClients.Add(clid, TTs3Client.Create(clid));

  FClients.Data[k].Name:=Nick;

  if Assigned(FOnCName) and (FClients.Data[k].Talking <> 0) then
    FOnCName(FClients.Data[k].Name, FClients.Data[k].ID, FID);
end;

procedure TTs3Server.ChangeFlags(clid, tflag, wflag:Integer);
var k     :Integer;
begin
  if FClients.Find(clid, k) then begin
    if FClients.Data[k].Name = '' then
      SendClientVariable(clid);

    if FClients.Data[k].Talking <> tflag then begin
      // Call the event for begin of talking
      if Assigned(FOnStart) and (tflag <> 0) then
        FOnStart(FClients.Data[k].Name, Name, FClients.Data[k].ID, FID);

      // Call the event for the end of talking
      if Assigned(FOnEnd) and (tflag = 0) then
        FOnEnd(FClients.Data[k].Name, Name, FClients.Data[k].ID, FID);
    end;
    FClients.Data[k].Talking:=tflag;
    FClients.Data[k].Whispering:=wFlag;
  end else begin
    FClients.Add(clid, TTs3Client.Create(clid));

    SendClientVariable(clid);
  end;
end;

procedure TTs3Server.handleClientlist(Msg:String);
var s     :TStrings;
    i, k  :Integer;
    clid  :Integer;
    nick  :String;
begin
  s:=TStringList.Create;
  s.Delimiter:='|';
  s.StrictDelimiter:=True;
  s.DelimitedText:=Msg;

  clid:=-1;
  for i:=0 to s.Count - 1 do begin
    if SScanf(s[i], 'clid=%d', [@k]) > 0 then
      clid:=k;

    if (clid >= 0) and (SScanf(s[i], 'client_nickname=%s', [@nick]) > 0) then
      ChangeNickname(clid, nick);

    if (clid >= 0) and (SScanf(s[i], 'client_flag_talking=%d', [@k]) > 0) then
      ChangeFlags(clid, k, 0);
  end;
  s.Free;
end;

procedure TTs3Server.setName(Value:String);
begin
  if (FName <> Value) and Assigned(FOnName) then
    FOnName(Value, FID);

  FName:=Value;
end;

procedure TTs3Server.SendClientList;
begin
  FSend(Format('use %d', [FID]));
  FSend('clientlist -voice');
end;

procedure TTs3Server.SendClientVariable(ClID:Integer);
begin
  FSend(Format('use %d', [FID]));
  FSend(Format('clientvariable clid=%d client_flag_talking client_nickname', [ClID]));
end;

procedure TTs3Server.SendServername;
begin
  FSend(Format('use %d', [FID]));
  FSend('servervariable virtualserver_name');
end;

end.

