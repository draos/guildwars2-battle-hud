unit uStatistic;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,ExtCtrls,
  Grids, uOption, uForms, uTypes, uWebAPI, uButtons, uData;

type
  // State of the Statistic modul
  TStatisticStatus  = (SS_NONE, SS_ETERNAL, SS_RED, SS_BLUE, SS_GREEN);

  { TStatistic }
  TStatistic        = class(TDragForm)
    Grid: TStringGrid;
    TopBox          : TPanel;
    // Form event handler
    procedure       FormCreate(Sender: TObject);
    procedure       FormDestroy(Sender: TObject);
    procedure       FormResize(Sender: TObject);
    procedure       FormShow(Sender: TObject);
    procedure       GridDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect; aState: TGridDrawState);
    procedure       GridGetCellHint(Sender: TObject; ACol, ARow: Integer; var HintText: String);
    procedure       GridSelectCell(Sender: TObject; aCol, aRow: Integer; var CanSelect: Boolean);
  private
    // The reference to a TWebAPI
    FWebAPI         :TWebAPI;
    // All buttons
    FButtons        :Array[0..4] of TImageButton;
    // Current status
    FStatus         :TStatisticStatus;

    // Procedure to handle map changes
    procedure       MapChange(Sender:TObject);
    procedure       ReloadGrid;
    // Set up the web api and reload the modul
    procedure       Settings(WebAPI:TWebAPI);
  public
    // Reload the statistic
    procedure       Reload;

    property        WebAPI:TWebAPI write Settings;
  end;

implementation

{$R *.lfm}

function Max(a, b:Integer):Integer;
begin
  if a < b then
    Result:=b
  else
    Result:=a;
end;

{ TStatistic }
// Procedure to handle map changes
procedure TStatistic.MapChange(Sender:TObject);
begin
  // Change the current state
  if Sender = FButtons[0] then
    FStatus:=SS_NONE
  else if Sender = FButtons[1] then
    FStatus:=SS_ETERNAL
  else if Sender = FButtons[2] then
    FStatus:=SS_GREEN
  else if Sender = FButtons[3] then
    FStatus:=SS_BLUE
  else if Sender = FButtons[4] then
    FStatus:=SS_RED
  else
    FStatus:=SS_NONE;

  // Reload the modul and show the informations
  Reload;
end;

// Constructor handler
procedure TStatistic.FormCreate(Sender: TObject);
var i     :Integer;
begin
  // Enable double buffering
  DoubleBuffered:=True;

  // Set the default values
  FStatus:=SS_NONE;
  FWebAPI:=Nil;

  // Set the font
  Font.Size:=10;
  Font.Style:=[fsBold];

  // Redirect the mouse events to the form
  with TopBox do begin
    OnMouseDown:=Self.OnMouseDown;
    OnMouseMove:=Self.OnMouseMove;
    OnMouseUp:=Self.OnMouseUp;
  end;

  with Grid do begin
    OnMouseDown:=Self.OnMouseDown;
    OnMouseMove:=Self.OnMouseMove;
    OnMouseUp:=Self.OnMouseUp;
  end;

  // Create the state changing buttons
  for i:=Low(FButtons) to High(FButtons) do begin
    // Create the button with the type IT_MAP
    FButtons[i]:=TImageButton.Create(Self, IT_MAP);
    with FButtons[i] do begin
      Parent:=TopBox;
      SetBounds(i * 30, 0, 25, 25);
      OnClick:=@MapChange;
    end;
  end;

  // Colorize the buttons
  FButtons[0].Color:=Colors.None;
  FButtons[1].Color:=Colors.Eternal;
  FButtons[2].Color:=Colors.Green;
  FButtons[3].Color:=Colors.Blue;
  FButtons[4].Color:=Colors.Red;

  // Reload it
  Reload;

  Logger.log('Created statistic.');
end;

// Destructor
procedure TStatistic.FormDestroy(Sender: TObject);
var i     :Integer;
begin
  // Free all buttons
  for i:=Low(FButtons) to High(FButtons) do
    FreeAndNil(FButtons[i]);

  // Logging entry
  Logger.log('Freed statistic.');
end;

// Resize event of the form
procedure TStatistic.FormResize(Sender: TObject);
var i, l   :Integer;
begin
  l:=(TopBox.Width - 145) div 2;

  for i:=Low(FButtons) to High(FButtons) do
    FButtons[i].Left:=l + 30 * i;
end;

// Show event triggers a reload
procedure TStatistic.FormShow(Sender: TObject);
begin
  Reload;
end;

// Draw the colored cells of the grid
procedure TStatistic.GridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin
  Grid.Canvas.Pen.Color:=Colors.Front;

  // Last color is the selected option
  if aCol = 6 then begin
    case FStatus of
      SS_NONE: Grid.Canvas.Brush.Color:=Colors.None;
      SS_ETERNAL: Grid.Canvas.Brush.Color:=Colors.Eternal;
      SS_RED: Grid.Canvas.Brush.Color:=Colors.Red;
      SS_BLUE: Grid.Canvas.Brush.Color:=Colors.Blue;
      SS_GREEN: Grid.Canvas.Brush.Color:=Colors.Green;
    end;
    Grid.Canvas.Rectangle(aRect.Left, 0, aRect.Right, Grid.Height);
  end else if aCol = 0 then begin
    // Draw the server color
    case aRow of
      1: Grid.Canvas.Brush.Color:=Colors.Red;
      2: Grid.Canvas.Brush.Color:=Colors.Blue;
      3: Grid.Canvas.Brush.Color:=Colors.Green;
    else
      Exit;
    end;

    with aRect do
      Grid.Canvas.Rectangle(Left, Top, Right, Bottom);
  end;
end;

procedure TStatistic.GridGetCellHint(Sender: TObject; ACol, ARow: Integer;
  var HintText: String);
begin
  if ACol in [1..5] then
    HintText:=StatisticString[ACol][Language]
  else
    HintText:='';
end;

// The cells are not selectable
procedure TStatistic.GridSelectCell(Sender: TObject; aCol, aRow: Integer;
  var CanSelect: Boolean);
begin
  CanSelect:=False;
end;

// Reload the data in the grid
procedure TStatistic.ReloadGrid;
var sc       :TServerColor;
    cmatch   :TAPIMatchup;
    cmap     :TAPIMap;
    tmp      :String;
    k        :Integer;
begin
  cmatch:=Nil;
  cmap:=Nil;

  // Check if the api is loaded and the state points on a map
  if FWebAPI <> Nil then
    cmatch:=FWebAPI.CurrentMatch;

  if (cmatch <> Nil) and (FStatus <> SS_NONE) then
    // Chose the map
    case FStatus of
      SS_RED: cmap:=cmatch.Maps[MT_RED];
      SS_BLUE: cmap:=cmatch.Maps[MT_BLUE];
      SS_GREEN: cmap:=cmatch.Maps[MT_GREEN];
      SS_ETERNAL: cmap:=cmatch.Maps[MT_ETERNAL];
    end;

  Grid.Cells[1, 0]:=StatisticString[1][Language];
  Grid.Cells[2, 0]:=StatisticString[2][Language];
  Grid.Cells[3, 0]:=StatisticString[3][Language];
  Grid.Cells[4, 0]:=StatisticString[4][Language];
  if FStatus <> SS_NONE then
    Grid.Cells[5, 0]:=''
  else
    Grid.Cells[5, 0]:=StatisticString[5][Language];

  // Iterate over the server colors
  for sc in TServerColor do begin
    tmp:='';
    if cmatch <> Nil then
      for k:=0 to MAX_WORLDS do
        if cmatch.Worlds[sc][k] > 0 then begin
          if tmp <> '' then
            tmp:=tmp + ', ';
          tmp:=tmp + FWebAPI.WorldName[cmatch.Worlds[sc][k], Language];
        end;

    // If the match and webapi is valid
    if (cmap <> Nil) and (FStatus <> SS_NONE) then begin
      // Get the server name
      Grid.Cells[1, Integer(sc) + 1]:=tmp;
      Grid.Cells[2, Integer(sc) + 1]:=Format('%D', [cmap.Scores[sc]]);
      Grid.Cells[3, Integer(sc) + 1]:=Format('+%D', [cmap.Ticks[sc]]);
      Grid.Cells[4, Integer(sc) + 1]:=Format('%.2F', [cmap.Kills[sc] / cmap.Deaths[sc]]);
      Grid.Cells[5, Integer(sc) + 1]:='';
    end else if (cmatch <> Nil) and (FStatus = SS_NONE) then begin
      // Get the server name
      Grid.Cells[1, Integer(sc) + 1]:=tmp;
      Grid.Cells[2, Integer(sc) + 1]:=Format('%D', [cmatch.Scores[sc]]);
      Grid.Cells[3, Integer(sc) + 1]:=Format('+%D', [cmatch.Ticks[sc]]);
      Grid.Cells[4, Integer(sc) + 1]:=Format('%.2F', [cmatch.Kills[sc] / cmatch.Deaths[sc]]);
      Grid.Cells[5, Integer(sc) + 1]:=Format('%D', [cmatch.Victories[sc]]);
    end else begin
      Grid.Cells[1, Integer(sc) + 1]:='Loading...';
      Grid.Cells[2, Integer(sc) + 1]:='';
      Grid.Cells[3, Integer(sc) + 1]:='';
      Grid.Cells[4, Integer(sc) + 1]:='';
      Grid.Cells[5, Integer(sc) + 1]:='';
    end;
  end;

  Grid.AutoSizeColumns;
  Grid.Color:=Colors.Back;
end;

// Reloading procedure
procedure TStatistic.Reload;
var i        :Integer;
begin
  ReloadGrid;

  // Update the window size
  ClientWidth:=Max(100, Grid.GridWidth);
  ClientHeight:=TopBox.Height + max(25, Grid.GridHeight);

  // Update the server names
  for i:=1 to 4 do
    FButtons[i].Hint:=HUDString[STRING_MAPS+i][Language];

  Color:=Colors.Back;
  Font.Color:=Colors.Font;
end;

// Set the web api and reload
procedure TStatistic.Settings(WebAPI:TWebAPI);
begin
  if WebAPI <> Nil then
    FWebAPI:=WebAPI;

  Reload;
end;

end.

