unit uReminder;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  fgl, uForms, uPanels, windows, mmsystem, uData,
  uOption, IniFiles;

type

  { TReminder }
  TPanelList    =specialize TFPGObjectList<TSpecialPanel>;

  TReminder     = class(TDragForm)
    TopBox      :TPanel;
    MainBox     :TPanel;
    procedure   FormCreate(Sender: TObject);
    procedure   FormDestroy(Sender: TObject);
  private
    FTimer      :TTimer;
    FPanels     :TPanelList;
    FVolume     :Word;
    FhWO        :HWAVEOUT;
    FWaveF      :TWAVEFORMATEX;

    procedure   AddReminder(Sender:TObject);
    procedure   Alarm;
    procedure   DeletePanel(Sender:TObject);
    procedure   Reorder;
    procedure   Timeout(Sender:TObject);
    procedure   setVolume(vol:Word);
  public
    { public declarations }
    procedure   Start;
    procedure   Pause;

    procedure   Save(Ini:TIniFile);
    procedure   Load(Ini:TIniFile);

    procedure   Reload;

    property    Volume:Word read FVolume write setVolume;
  end;

implementation

{$R *.lfm}

const
  SECTION       = 'Reminders';

function CmpPanel(const p1, p2:TSpecialPanel):Integer;
begin
  Result:=p1.Priority - p2.Priority;

  if (Result = 0) and (p1 is TReminderPanel) and (p2 is TReminderPanel) then
    Result:=CompareStr((p1 as TReminderPanel).Name, (p2 as TReminderPanel).Name);
end;

{ TReminder }
procedure TReminder.setVolume(vol:Word);
var v     :DWORD;
begin
  v:=vol + vol shl 16;
  FVolume:=vol;

  // Set volume
  waveOutSetVolume(FhWO, v);
end;

procedure TReminder.Alarm;
begin
  if FileExists('alarm.wav') then
    sndPlaySound(PChar('alarm.wav'), SND_ASYNC or SND_NODEFAULT);
end;

procedure TReminder.DeletePanel(Sender:TObject);
begin
  if Sender is TReminderPanel then
    FPanels.Remove(Sender as TSpecialPanel);

  Reorder;
end;

procedure TReminder.Reload;
begin
  TopBox.Color:=Colors.Back;
  TopBox.Font.Color:=Colors.Font;
  TopBox.Caption:=HUDString[STRING_VISIBLE+2][Language];

  Refresh;
end;

procedure TReminder.AddReminder(Sender:TObject);
var panel :TReminderPanel;
begin
  if (FPanels[0] is TAddPanel) or ((FPanels[0] is TReminderPanel) and
     ((FPanels[0] as TReminderPanel).State <> RS_EDITABLE)) then begin

    panel:=TReminderPanel.Create(Self, @Alarm);
    with panel do begin
      Parent:=MainBox;
      Left:=0;
      OnDelete:=@DeletePanel;
    end;

    FPanels.Add(panel);
  end;

  Reorder;
end;

procedure TReminder.Reorder;
var i, t  :Integer;
begin
  if FPanels.Count > 1 then
    FPanels.Sort(@CmpPanel);

  t:=0;
  for i:=0 to FPanels.Count -1 do begin
    FPanels[i].Top:=t;
    Inc(t, FPanels[i].Height);
  end;

  ClientHeight:=TopBox.Height + t;
end;

procedure TReminder.FormCreate(Sender: TObject);
var panel :TAddPanel;
begin
  TopBox.OnMouseDown:=OnMouseDown;
  TopBox.OnMouseMove:=OnMouseMove;
  TopBox.OnMouseUp:=OnMouseUp;

  FVolume:=$7FFF;

{$IF defined(win32)}
  // Init TWAVEFORMATEX
  FillChar(FWaveF, SizeOf(FWaveF), 0);
  // Open WaveMapper
  waveOutOpen(@FhWO, WAVE_MAPPER, @FWaveF, 0, 0, 0);
{$ELSE}
  // TODO
{$ENDIF}

  FTimer:=TTimer.Create(Self);
  FTimer.Enabled:=False;
  FTimer.Interval:=1000;
  FTimer.OnTimer:=@Timeout;

  panel:=TAddPanel.Create(Self);
  with panel do begin
    Parent:=MainBox;
    Left:=0;
    OnClick:=@AddReminder;
  end;

  FPanels:=TPanelList.Create;
  FPanels.FreeObjects:=True;
  FPanels.Add(panel);

  Reorder;
  Start;

  Logger.log('Reminder created.');
end;

procedure TReminder.FormDestroy(Sender: TObject);
begin
{$IF defined(win32)}
  waveOutClose(FhWO);
{$ENDIF}
  FreeAndNil(FPanels);

  Logger.log('Freed reminder.');
end;

procedure TReminder.Start;
begin
  FTimer.Enabled:=True;
end;

procedure TReminder.Pause;
begin
  FTimer.Enabled:=False;
end;

procedure TReminder.Save(Ini:TIniFile);
var i     :Integer;
begin
  ini.EraseSection(SECTION);

  for i:=0 to FPanels.Count - 1 do
    if FPanels[i] is TReminderPanel then
      with FPanels[i] as TReminderPanel do
        if (Name <> '') and (State <> RS_EDITABLE) then
          ini.WriteInteger(SECTION, Name, Duration);
end;

procedure TReminder.Load(Ini:TIniFile);
var i     :Integer;
    s     :TStringList;
    panel :TSpecialPanel;
begin
  if ini.SectionExists(SECTION) then begin
    FPanels.Clear;

    s:=TStringList.Create;
    ini.ReadSection(SECTION, s);

    // Parse the section
    for i:=0 to s.Count -1 do
      if s[i] <> '' then begin
        panel:=TReminderPanel.Create(Self, @Alarm);
        with panel as TReminderPanel do begin
          Parent:=MainBox;
          Left:=0;
          OnDelete:=@DeletePanel;
          Name:=s[i];
          Duration:=ini.ReadInteger(SECTION, s[i], Duration);
          State:=RS_PAUSE;
        end;

        FPanels.Add(panel);
      end;
    s.Free;

    // Create the default panel for adding
    panel:=TAddPanel.Create(Self);
    with panel do begin
      Parent:=MainBox;
      Left:=0;
      OnClick:=@AddReminder;
    end;
    FPanels.Add(Panel);

    Reorder;
  end;
end;

procedure TReminder.Timeout(Sender:TObject);
var i    :Integer;
begin
  for i:=0 to FPanels.Count - 1 do
    FPanels[i].DecPriority;

  Reorder;
end;

end.

