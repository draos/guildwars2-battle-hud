{
  Created by: draos.9574

  Implements the compass overlay system.
}

unit uCompass;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uSymbols, uMumble, math, Controls, uOption, uTypes;

type
  { TCompassOverlay }
  TCompassOverlay    = class
  private
    // Controls the distance of the directions from the center
    FScale           :Double;
    FParent          :TWinControl;
    // Controls the size of the directions
    FSize            :Integer;
    // Array of the directions
    FDirection       :Array[TDirection] of TDirectionSymbol;
    // Visibility
    FVisible         :Boolean;
    // Mumble reference
    FMumble          :TMumble;
    // screen metrics
    FScreen          :record
      Width, Height  :Integer;
    end;

    function         getScale:Double;

    procedure        setPosition(dir:TDirection;x, y:Single);
    procedure        setScale(Scale:Double);
    procedure        setSize(Size:Integer);
    procedure        setVisible(Visible:Boolean);
  public
    constructor      Create(Mumble:TMumble;Parent:TWinControl);
    destructor       Free;

    procedure        Refresh;

    property         Scale:Double read getScale write setScale;
    property         Size:Integer read FSize write setSize;
    property         Visible:Boolean read FVisible write setVisible;
  end;

implementation

{ TCompassOverlay }
constructor TCompassOverlay.Create(Mumble:TMumble;Parent:TWinControl);
var dir     :TDirection;
begin
  // Set basic settings and references
  FMumble:=Mumble;
  FSize:=48;
  FScale:=0.5;
  FVisible:=True;
  FParent:=Parent;

  // Get screen metrics
  FScreen.Width:=Parent.Width;
  FScreen.Height:=Parent.Height;

  // Initialize the directions on the TOverlay (Parent)
  for dir in TDirection do begin
    FDirection[dir]:=TDirectionSymbol.Create(Parent);
    FDirection[dir].SetSize(FSize);
    FDirection[dir].Direction:=dir;
    FDirection[dir].Visible:=False;
    FDirection[dir].Parent:=Parent;
    FDirection[dir].Color:=Colors.Back;
  end;
end;

destructor TCompassOverlay.Free;
var dir    :TDirection;
begin
  for dir in TDirection do
    FreeAndNil(FDirection[dir]);
end;

// Refresh the compass
procedure TCompassOverlay.Refresh;
var x,y,l :Single;
begin
  // Get screen metrics
  FScreen.Width:=FParent.Width;
  FScreen.Height:=FParent.Height;

  //  if mumble is activates
  if FMumble <> Nil then begin
    // Call the camera direction from mumble
    x:= FMumble.Camera.Front[0];
    y:=-FMumble.Camera.Front[2];

    // Calculate the length of the vector (x, y)
    l:=Sqrt(x*x+y*y);
    if l<>0. then begin
      // Scale the normalized vector (x, y) to the excepted scaling
      x:=x/l*FScale;
      y:=y/l*FScale;

      // Set the new positions of the compass directions
      setPosition(D_NORTH,-x, y);
      setPosition(D_SOUTH, x,-y);
      setPosition(D_EAST, -y,-x);
      setPosition(D_WEST,  y, x);
    end;
  end;
end;

// set the compass directions
procedure TCompassOverlay.setPosition(dir:TDirection;x, y:Single);
begin
  with FDirection[dir] do begin
    // set the positions scaled by the screen metrics
    setPosition(FScreen.Width/2*(1+x), FScreen.Height/2*(1+y));

    // Decide visibility, the one direction with highest Top is invisible
    //  just check the neighbour directions
    if (dir in [D_NORTH, D_SOUTH])and(FVisible) then
      Visible:=(FDirection[D_EAST].Top>Top)or(FDirection[D_WEST].Top>Top)
    else if (dir in [D_EAST, D_WEST])and(FVisible) then
      Visible:=(FDirection[D_SOUTH].Top>Top)or(FDirection[D_NORTH].Top>Top)
    else
      Visible:=False;
  end;
end;

// Getter
function TCompassOverlay.getScale:Double;
begin
  Result:=2.0 * (FScale - 0.25);
end;

// Setter, always with Refresh
procedure TCompassOverlay.setScale(Scale:Double);
begin
  if Scale > 1.0 then
    FScale:=0.75
  else if Scale < 0.0 then
    FScale:=0.25
  else
    FScale:=0.5*Scale+0.25;
  Refresh;
end;

procedure TCompassOverlay.setSize(Size:Integer);
var dir   :TDirection;
begin
  FSize:=Max(32, Min(64, Size));
  for dir in TDirection do
    FDirection[dir].SetSize(Size);
  Refresh;
end;

procedure TCompassOverlay.setVisible(Visible:Boolean);
begin
  FVisible:=Visible;
  Refresh;
end;

end.

