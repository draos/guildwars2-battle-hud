unit u2dmap;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  uTypes, uData, math, uOption, windows, uSymbols, uWebAPI;

type

  { TMap2DOverlay }
  TMap2DOverlay      = class(TForm)
    BackAlpine       ,
    BackEternal      :TImage;
    Back             :TPaintBox;
    BackDesert: TImage;
    procedure        DragMouseMove(Sender:TObject;{%H-}Shift:TShiftState;X,Y:Integer);
    procedure        DragMouseUp(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;{%H-}X,{%H-}Y:Integer);
    procedure        DragMouseDown(Sender:TObject;{%H-}Button:TMouseButton;{%H-}Shift:TShiftState;X,Y:Integer);
    procedure        BackPaint(Sender:TObject);
    procedure        FormCreate(Sender:TObject);
    procedure        FormDestroy(Sender: TObject);
    procedure        FormResize(Sender: TObject);
    procedure        FormShow(Sender: TObject);
  private
    // List of objects
    FObjects         :Array of TObjectSymbol;
    FCount           :Integer;
    // TMatch for using the data (only reference)
    FWebAPI          :TWebAPI;
    // List of the objectiv icons (only reference)
    FImageList       :TImageList;
    FLoaded          :Boolean;
    // Mouse settings for moving and resizing
    FMouse           :record
      X,Y            :Integer;
      Move           :Boolean;
      Resize         :Boolean;
      Transparent    :Boolean;
    end;
    // Window screen
    FScreen          :record
      Left, Top      :Integer;
      Width, Height  :Integer;
    end;
    // Size settings
    FSize            :record
      Width, Height  :Integer;
      Scale          :Single;
    end;
    // Visibility of different types of structures
    FStructVisible  :Array[TWvWObjectiveType] of Boolean;
    // Update time, used over a callback from every object to minimized time calls
    FTime           :TDateTime;
    // Track bar settings
    FTrack           :record
      Left, Top      :Integer;
      Right, Bottom  :Integer;
      Alpha          :Single;
      Drag           :Boolean;
    end;

    FGuild           :TImage;

    // MapType
    FType            :TWvWMapType;
    FMap             :Integer;
    // Visible form or not
    FDisplay         :Boolean;
    // 2nd window style for toogling the mouse transparency
    FWindowStyle     :LONG;

    // Procedures
    procedure        Move(X, Y:Integer);
    procedure        SortObjects;
    procedure        ResetObjects;
    procedure        AddObject(Obj:TAPIMObjective);
    procedure        ReloadConstraints;
    procedure        UpdateObjects;

    // Getter
    function         getAlpha:Single;
    function         getMouse:Boolean;
    function         getTime:TDateTime;
    function         getSize:Single;
    function         getSVisible(Typ:TWvWObjectiveType):Boolean;

    // Setter
    procedure        setAlpha(Value:Single);
    procedure        setDisplay(Value:Boolean);
    procedure        setMouse(Value:Boolean);
    procedure        setSize(Size:Single);
    procedure        setSVisible(Typ:TWvWObjectiveType;Value:Boolean);
  public
    // Manipulation of the settings
    procedure        Settings(Map:TWvWMapType;WebAPI:TWebAPI=NIL;ImageList:TImageList=NIL);
    procedure        ToogleMouse;

    // Refresh procedure
    procedure        Reload;

    // Properties
    property         Alpha:Single read getAlpha write setAlpha;
    property         Display:Boolean read FDisplay write SetDisplay;
    property         Guild:TImage read FGuild write FGuild;
    property         Mouse:Boolean read getMouse write SetMouse;
    property         Size:Single read getSize write setSize;
    property         Structures[Typ:TWvWObjectiveType]:Boolean read getSVisible write setSVisible;
  end;

implementation

{$R *.lfm}

{ TMap2DOverlay }
// Paint the background with map, track bar and border
procedure TMap2DOverlay.BackPaint(Sender: TObject);
var r     :TRect;
begin
  r.Left:=5;
  r.Top:=5;
  r.Right:=ClientWidth-5;
  r.Bottom:=ClientHeight-5;

  // Draw stretched maps
  case FMap of
    38:
      Back.Canvas.StretchDraw(r, BackEternal.Picture.PNG);
    94, 95, 96:
      Back.Canvas.StretchDraw(r, BackAlpine.Picture.PNG);
    1102, 1099, 1143:
      Back.Canvas.StretchDraw(r, BackDesert.Picture.PNG);
  end;

  // Draw a colored border
  Back.Canvas.Brush.Style:=bsClear;
  case FType of
    MT_RED:    Back.Canvas.Pen.Color:=Colors.Red;
    MT_GREEN:  Back.Canvas.Pen.Color:=Colors.Green;
    MT_BLUE:   Back.Canvas.Pen.Color:=Colors.Blue;
    MT_ETERNAL:Back.Canvas.Pen.Color:=Colors.Eternal;
  else
      Back.Canvas.Pen.Color:=clWhite;
  end;

  Back.Canvas.Pen.Width:=10;
  Back.Canvas.Rectangle(0, 0, ClientWidth, ClientHeight);

  // Draw the trackbar for alpha channel
  with Back.Canvas do begin
    Brush.Style:=bsSolid;
    Brush.Color:=clBlack;
    Pen.Color:=clBlack;
    Pen.Width:=2;
    Rectangle(0, 0, ClientWidth, 8);
  end;

  FTrack.Left:=Round(FTrack.Alpha*(ClientWidth-10));
  FTrack.Top:=0;
  FTrack.Right:=FTrack.Left+10;
  FTrack.Bottom:=FTrack.Top+20;

  Back.Canvas.Pen.Color:=Colors.Front;
  Back.Canvas.Brush.Color:=Colors.Back;
  Back.Canvas.Rectangle(FTrack.Left, FTrack.Top, FTrack.Right, FTrack.Bottom);
end;

// Constructor
procedure TMap2DOverlay.FormCreate(Sender: TObject);
var ot    :TWvWObjectiveType;
begin
  FLoaded:=False;
  DoubleBuffered:=True;

  FScreen.Left:=GetSystemMetrics(SM_XVIRTUALSCREEN);
  FScreen.Top:=GetSystemMetrics(SM_YVIRTUALSCREEN);
  FScreen.Width:=GetSystemMetrics(SM_CXVIRTUALSCREEN);
  FScreen.Height:=GetSystemMetrics(SM_CYVIRTUALSCREEN);

  FMap:=0;
  FSize.Scale:=1.0;
  FTrack.Alpha:=1.0;
  FTrack.Drag:=False;
  FMouse.Move:=False;
  FMouse.Resize:=False;
  Mouse:=False;
  Display:=False;

  // Set all except ruins to visible
  for ot in TWvWObjectiveType do
    FStructVisible[ot]:=not (ot in [AOT_GENERIC, AOT_RUINS, AOT_SHRINE]);

  FWindowStyle:=GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED or WS_EX_TRANSPARENT;

  Settings(MT_BLUE, Nil);
end;

// Destructor
procedure TMap2DOverlay.FormDestroy(Sender: TObject);
var i     :Integer;
begin
  for i:=0 to FCount - 1 do
    FreeAndNil(FObjects[i]);
  SetLength(FObjects, 0);
end;

procedure TMap2DOverlay.Move(X, Y:Integer);
var tmp   :Integer;
begin
  tmp:=GetSystemMetrics(SM_XVIRTUALSCREEN);
  X:=Max(tmp, Min(tmp + GetSystemMetrics(SM_CXVIRTUALSCREEN) - Width, X));

  tmp:=GetSystemMetrics(SM_YVIRTUALSCREEN);
  Y:=Max(tmp, Min(tmp + GetSystemMetrics(SM_CYVIRTUALSCREEN) - Height, Y));

  SetBounds(X, Y, Width, Height);
end;

procedure TMap2DOverlay.FormResize(Sender: TObject);
begin
  UpdateObjects;
end;

procedure TMap2DOverlay.FormShow(Sender: TObject);
begin
  Move(Left, Top);
  UpdateObjects;
end;

// Mouse functions for moving, resizing and tracking the track bar
procedure TMap2DOverlay.DragMouseDown(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  if Sender is TObjectSymbol then
    with Sender as TObjectSymbol do begin
      Inc(X, Left);
      Inc(Y, Top);
    end;

  if Button = mbLeft then begin
    if Y <= 20 then
      FTrack.Drag:=True
    else if (Y >= ClientHeight-20)or(X >= ClientWidth-20) then
      FMouse.Resize:=True
    else
      FMouse.Move:=True;

    FMouse.X:=X;
    FMouse.Y:=Y;
  end;
end;

procedure TMap2DOverlay.DragMouseMove(Sender:TObject;Shift:TShiftState;X,Y:Integer);
begin
  if Sender is TObjectSymbol then
    with Sender as TObjectSymbol do begin
      Inc(X, Left);
      Inc(Y, Top);
    end;

  if (FMouse.Y <= 20)and(FTrack.Drag) then
    setAlpha(X/(ClientWidth-10));

  Back.Cursor:=crDefault;

  if (FMouse.X < 0)or(X > ClientWidth)or(Y < 0)or(Y > ClientHeight) then
    Back.Cursor:=crDefault
  else if (X > 20)and(X < ClientWidth - 20)and(Y < ClientHeight - 20) then begin
    if FMouse.Move then
      Back.Cursor:=crSizeAll
    else
      Back.Cursor:=crDefault;
  end else if (Y >= ClientHeight-20)and(X >= ClientWidth-20) then
    Back.Cursor:=crSizeNWSE
  else if (Y >= 20)and(X >= ClientWidth-20) then
    Back.Cursor:=crSizeWE
  else if Y >= ClientHeight-20 then
    Back.Cursor:=crSizeNS
  else
    Back.Cursor:=crDefault;

  if FMouse.Resize then begin
    if FMouse.X >= ClientWidth - 20 then begin
      SetSize(X/FMouse.X*FSize.Scale);
      FMouse.X:=X;
      Back.Cursor:=crSizeWE;
    end else if FMouse.Y >= ClientHeight - 20 then begin
      SetSize(Y/FMouse.Y*FSize.Scale);
      FMouse.Y:=Y;
      Back.Cursor:=crSizeNS;
    end;
  end;

  if FMouse.Move then begin
    Move(Left+X-FMouse.X, Top+Y-FMouse.Y);
    Back.Cursor:=crSizeAll;
  end;

  Reload;
end;

procedure TMap2DOverlay.DragMouseUp(Sender:TObject;Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  FMouse.Move:=False;
  FMouse.Resize:=False;
  FTrack.Drag:=False;
end;

// Add a object
procedure TMap2DOverlay.AddObject(Obj:TAPIMObjective);
begin
  if Obj = nil then
    Exit;

  SetLength(FObjects, FCount+1);
  FObjects[FCount]:=TObjectSymbol.Create(Nil, FImageList, @getTime, Obj);

  with FObjects[FCount] do begin
    Parent:=Self;
    Color:=Colors.Transparent;
    Shield:=FGuild;
    Visible:=False;
    WebAPI:=FWebAPI;
    ForceVisible:=True;
    OnMouseDown:=@DragMouseDown;
    OnMouseMove:=@DragMouseMove;
    OnMouseUp:=@DragMouseUp;
  end;

  Inc(FCount);
end;

// Place the objects on the form
procedure TMap2DOverlay.UpdateObjects;
var i     :Integer;
    coord :TVector;
begin
  if (FWebAPI = Nil) or (FImageList = Nil) then
    Exit;

  for i:=0 to FCount - 1 do begin
    FObjects[i].Visible:=(FObjects[i].Data <> Nil) and FStructVisible[FObjects[i].Data.ObjectiveType];

    if not FObjects[i].Visible then
      Continue;

    coord:=FObjects[i].Data.Coordinate2D;
    coord[0]:=coord[0] * (ClientWidth - 10);
    coord[2]:=coord[2] * (ClientHeight - 10);

    FObjects[i].SetPosition(coord[0] + 5, ClientHeight + 10 - coord[2]);
  end;
end;

// Refresh the 2d map overlay
procedure TMap2DOverlay.Reload;
var i     :Integer;
begin
  Color:=Colors.Back;

  if FWebAPI = Nil then
    Exit;

  if not FLoaded then
    ResetObjects;

  // Set the current time
  FTime:=Now;

  // Update all objects
  for i:=0 to FCount - 1 do
    FObjects[i].Reload;

  Refresh;
end;

// Delete all objects and reinitialize it
procedure TMap2DOverlay.ResetObjects;
var i     :Integer;
    cmatch:TAPIMatchup;
    cmap  :TAPIMap;
label
  FinishForm;
begin
  BeginFormUpdate;
  for i:=0 to FCount - 1 do
    FreeAndNil(FObjects[i]);
  SetLength(FObjects, 0);
  FCount:=0;

  // Check the web api
  if FWebAPI = Nil then
    goto FinishForm;

  // Check the current match
  cmatch:=FWebAPI.CurrentMatch;
  if cmatch = Nil then
    goto FinishForm;

  // Check the current map
  cmap:=cmatch.Maps[FType];
  if cmap = Nil then
    goto FinishForm;

  // Resolve the map id and set the constraints
  FMap:=cmap.ID;
  if FMap <> 0 then begin
    ReloadConstraints;
    // Rescale the image
    setSize(FSize.Scale);
  end;

  // Update the objectives
  for i:=0 to cmap.Objectives.Count - 1 do
    with cmap.Objectives do
      AddObject(Data[i]);

FinishForm:
  FLoaded:=FCount > 0;

  UpdateObjects;
  SortObjects;
  EndFormUpdate;
end;

// Take the map type, a match enviroment and a imagelist for the objectiv icons
procedure TMap2DOverlay.Settings(Map:TWvWMapType;WebAPI:TWebAPI;ImageList:TImageList);
begin
  FType:=Map;
  if WebAPI <> Nil then begin
    FWebAPI:=WebAPI;
    ResetObjects;
  end;

  if ImageList <> Nil then
    FImageList:=ImageList;

  ReloadConstraints;

  SetSize(1.0);
end;

procedure TMap2DOverlay.ReloadConstraints;
begin
  case FMap of
    38:begin
      FSize.Width:=BackEternal.Width;
      FSize.Height:=BackEternal.Height;
      Constraints.MaxWidth:=BackEternal.Width;
      Constraints.MaxHeight:=BackEternal.Height;
    end;
    94, 95, 96:begin
      FSize.Width:=BackAlpine.Width;
      FSize.Height:=BackAlpine.Height;
      Constraints.MaxWidth:=BackAlpine.Width;
      Constraints.MaxHeight:=BackAlpine.Height;
    end;

    1102, 1099, 1143:begin
      FSize.Width:=BackDesert.Width;
      FSize.Height:=BackDesert.Height;
      Constraints.MaxWidth:=BackDesert.Width;
      Constraints.MaxHeight:=BackDesert.Height;
    end;

    else
      FSize.Width:=100;
      FSize.Height:=100;
      Constraints.MaxWidth:=100;
      Constraints.MaxHeight:=100;
  end;

  Constraints.MinWidth:=3 * Constraints.MaxWidth div 5;
  Constraints.MinHeight:=3 * Constraints.MaxHeight div 5;
end;

// Sort objects via y-coordination for solving issues with overlapping objects
procedure TMap2DOverlay.SortObjects;
var Pos   :array of record y:Single;Index:Integer;end;

  // Quicksort implementation
  procedure Quicksort(iLo, iHi:Integer);
  var Lo, Hi, Ti :Integer;
      Mid, Tv    :Single;
  begin
    Lo:=iLo;
    Hi:=iHi;
    Mid:=Pos[(Lo+Hi)div 2].y;
    repeat
      while Pos[Lo].y < Mid do Inc(Lo);
      while Pos[Hi].y > Mid do Dec(Hi);

      if Lo <= Hi then begin
        Ti:=Pos[Lo].Index;
        Pos[Lo].Index:=Pos[Hi].Index;
        Pos[Hi].Index:=Ti;
        Tv:=Pos[Lo].y;
        Pos[Lo].y:=Pos[Hi].y;
        Pos[Hi].y:=Tv;
        Inc(Lo);
        Dec(Hi);
      end;
    until Lo > Hi;

    if Hi > iLo then Quicksort(iLo, Hi);
    if Lo < iHi then Quicksort(Lo, iHi);
  end;

var i     :Integer;
begin
  if FCount = 0 then
    Exit;

  // Create list of the y-coordination and the index
  SetLength(Pos, FCount);
  for i:=0 to FCount - 1 do begin
    if FObjects[i].Visible then
      Pos[i].y:=FObjects[i].Data.Coordinate[2]
    else
      Pos[i].y:=0;
    Pos[i].Index:=i;
  end;

  // Sort in ascending order
  Quicksort(Low(Pos), High(Pos));

  // Send all objects back
  for i:=FCount - 1 downto 0 do
    FObjects[Pos[i].Index].SendToBack;

  // Send the background in the back
  Back.SendToBack;
end;

// Toogle between mouse transparencies
procedure TMap2DOverlay.ToogleMouse;
var tmp   :LONG;
begin
  tmp:=GetWindowLong(Handle, GWL_EXSTYLE);
  SetWindowLong(Handle, GWL_EXSTYLE, FWindowStyle);
  FWindowStyle:=tmp;
end;

// Getter
function TMap2DOverlay.getAlpha:Single;
begin
  Result:=FTrack.Alpha;
end;

function TMap2DOverlay.getMouse:Boolean;
begin
  Result:=FMouse.Transparent;
end;

function TMap2DOverlay.getTime:TDateTime;
begin
  Result:=FTime;
end;

function TMap2DOverlay.getSize:Single;
begin
  Result:=FSize.Scale;
end;

function TMap2DOverlay.getSVisible(Typ:TWvWObjectiveType):Boolean;
begin
  Result:=FStructVisible[Typ];
end;

// Setter
procedure TMap2DOverlay.setAlpha(Value:Single);
begin
  FTrack.Alpha:=math.max(0.0, math.min(1.0, Value));
  AlphaBlendValue:=Round(255*(0.4+0.6*FTrack.Alpha));
  Back.Refresh;
end;

procedure TMap2DOverlay.setDisplay(Value:Boolean);
begin
  FDisplay:=Value;
  Visible:=FDisplay;
end;

procedure TMap2DOverlay.setMouse(Value:Boolean);
begin
  if FMouse.Transparent xor Value then
    ToogleMouse;
  FMouse.Transparent:=Value;
end;

procedure TMap2DOverlay.setSize(Size:Single);
begin
  if Size < 0.6 then
    Size:=0.6
  else if Size > 1.0 then
    Size:=1.0;

  FSize.Scale:=Size;
  SetBounds(Left, Top, Round(FSize.Width * Size), Round(FSize.Height * Size));
end;

procedure TMap2DOverlay.setSVisible(Typ:TWvWObjectiveType;Value:Boolean);
begin
  FStructVisible[Typ]:=Value;

  // Update the objects
  UpdateObjects;
end;

end.

