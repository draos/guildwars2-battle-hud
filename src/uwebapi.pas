{
  Created by: draos.9574

  Implements the web API interface which will be used
}

unit uWebAPI;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, simpleinternet, ExtCtrls, fpjson, jsonparser, fgl, uData,
  uTypes, uOption;

const
  // number of current wvw structures
  OBJECTIV_COUNT   = 76;
  MAX_WORLDS       = 5;

type
  // Callback to get the request times
  TTimeCallback     = function:TDateTime of object;

  // Array to store the scores of each server
  TAPIScore         = Array[TServerColor] of Integer;
  TAPIWorlds        = Array[TServerColor] of Array[0..MAX_WORLDS] of Integer;

  { TAPIGuild }
  TAPIGuild         = class
  private
    FID             :String;
    FName           :String;
    FTag            :String;
  public
    constructor     Create(Json:TJSONObject);

    property        ID:String read FID;
    property        Name:String read FName;
    property        Tag:String read FTag;
  end;

  { TAPIMObjective }
  // Forward declaration
  TAPIMObjective   = class;

  // Events
  TAPIObjEvent     = procedure(const Objective:TAPIMObjective) of object;

  // Class
  TAPIMObjective   = class
  private
    FID             :String; // "<mapID>-<objectID>"
    FType           :TWvWObjectiveType;
    FOwner          :TObjectiveColor;
    FLastFlipped    :TDateTime;
    FClaimedBy      :String; // Guild ID or empty
    FClaimedAt      :TDateTime;
    FOffset         :Integer;
    FMatchID        :String;
    // Events
    FOnFlip         :TAPIObjEvent;
    FOnClaim        :TAPIObjEvent;

    // Setter
    procedure       setClaimed(By:String; At:TDateTime);
    procedure       setOwner(Value:TObjectiveColor);

    // Internal loading functions
    procedure       load(const root:TJSONObject);
    procedure       loadOffset;
  public
    constructor     Create(const root:TJSONObject; const MatchID:String);

    property        ID:String read FID;
    property        MatchID:String read FMatchID;
    property        ObjectiveType:TWvWObjectiveType read FType;
    property        Offset:Integer read FOffset;
    property        Owner:TObjectiveColor read FOwner;
    property        Index:Integer read FOffset;
    property        LastFlipped:TDateTime read FLastFlipped;
    property        ClaimedBy:String read FClaimedBy;
    property        ClaimedAt:TDateTime read FClaimedAt;

    // Events
    property        OnClaim:TAPIObjEvent write FOnClaim;
    property        OnFlip:TAPIObjEvent write FOnFlip;
  end;
  TAPIMObjectives   = specialize TFPGMap<String, TAPIMObjective>;

  { TAPIObjective}
  TAPIObjective    = class
  private
    FID             :String; // "<mapID>-<objectID>"
    FOID            :Integer; // The extracted object ID
    FNames          :TLanguages; // Names in all languages
    FSectorID       :Integer;
    FType           :TWvWObjectiveType;
    FMapType        :TWvWMapType;
    FMapID          :Integer;
    FCoord          :TVector;
    FMarker         :String;

    function        get2DCoord:TVector;
    function        getName(Lang:TLanguage):String;
    // Update function for date from the api
    procedure       load(const root:TJSONObject;const lang:TLanguage);
  public
    // Default constructor
    constructor     Create(const ID:String);
    destructor      Destroy; override;

    // Read-only properties
    property        ID:String read FID;
    property        ObjectiveID:Integer read FOID;
    property        Names[Lang:TLanguage]:String read getName;
    property        SectorID:Integer read FSectorID;
    property        ObjectiveType:TWvWObjectiveType read FType;
    property        MapType:TWvWMapType read FMapType;
    property        MapID:Integer read FMapID;
    property        Coordinate:TVector read FCoord;
    property        Coordinate2D:TVector read get2DCoord;
    property        Marker:String read FMarker;
  end;

  TAPIObjectives   = specialize TFPGMap<String, TAPIObjective>;

  { TAPI2Bonuses }
  TAPIBonus        = record
    Bonus           :String;
    Owner           :TObjectiveColor;
  end;

  { TAPIMap }
  TAPIMap           = class
  private
    // Match ID
    FMatchID        :String;
    // Map specific values
    FID             :Integer;
    FType           :TWvWMapType;
    FScores         :TAPIScore;
    FKills          :TAPIScore;
    FDeaths         :TAPIScore;
    FTicks          :TAPIScore;
    FActive         :Boolean;
    // Bonuses
    FBonuses        :Array of TAPIBonus;
    // Objectives
    FObjectives     :TAPIMObjectives;
    // Events
    FOnFlip         :TAPIObjEvent;
    FOnClaim        :TAPIObjEvent;

    // Getter
    function        getBonusOwner(Bonus:String):TObjectiveColor;

    // Setter
    procedure       setClaimEvent(Event:TAPIObjEvent);
    procedure       setFlipEvent(Event:TAPIObjEvent);

    // Internal loading functions
    procedure       load(const root:TJSONObject);
    procedure       loadBonuses(const root:TJSONArray);
    procedure       loadObjectives(const root:TJSONArray);
  public
    constructor     Create(const root:TJSONObject; const MatchID:String);
    destructor      Destroy;override;

    property        Active:Boolean read FActive write FActive;
    property        ID:Integer read FID;
    property        Objectives:TAPIMObjectives read FObjectives;
    property        MapType:TWvWMapType read FType;
    property        BonusOwner[Bonus:String]:TObjectiveColor read getBonusOwner;
    property        Scores:TAPIScore read FScores;
    property        Kills:TAPIScore read FKills;
    property        Deaths:TAPIScore read FDeaths;
    property        Ticks:TAPIScore read FTicks;
    property        MatchID:String read FMatchID;

    // Events
    property        OnClaim:TAPIObjEvent write setClaimEvent;
    property        OnFlip:TAPIObjEvent write setFlipEvent;
  end;
  TAPIMaps         = specialize TFPGMap<Integer, TAPIMap>;

  { TAPIMatchup }
  // Stores information about matchups
  TAPIMatchup      = class
  private
    // Format "<region>-<tier>"
    FID             :String;
    FStartTime      :TDateTime;
    FEndTime        :TDateTime;
    // Scores
    FScores         :TAPIScore;
    // Ticks
    FTicks          :TAPIScore;
    // Deaths of player
    FDeaths         :TAPIScore;
    // Kills of enemy where each enemy only counts once
    FKills          :TAPIScore;
    // Victory points
    FVictories      :TAPIScore;
    // Worlds in the match
    FWorlds         :TAPIWorlds;
    // Maps
    FMaps           :TAPIMaps;
    // Events
    FOnFlip         :TAPIObjEvent;
    FOnClaim        :TAPIObjEvent;

    function        getMaps(Map:TWvWMapType):TAPIMap;
    function        getMapByID(MapID:Integer):TAPIMap;

    // Setter
    procedure       setClaimEvent(Event:TAPIObjEvent);
    procedure       setFlipEvent(Event:TAPIObjEvent);

    procedure       load(const root:TJSONObject);
    procedure       loadMaps(const root:TJSONArray);
  public
    constructor     Create(const root:TJSONObject);
    destructor      Destroy;override;

    // Checks if the given server is part of the match
    function        isIn(Server:Integer):Boolean;

    // Properties
    property        ID:String read FID;
    property        StartTime:TDateTime read FStartTime;
    property        EndTime:TDateTime read FEndTime;
    property        Maps[Map:TWvWMapType]:TAPIMap read getMaps;
    property        MapByID[MapID:Integer]:TAPIMap read getMapByID;
    property        Scores:TAPIScore read FScores;
    property        Deaths:TAPIScore read FDeaths;
    property        Kills:TAPIScore read FKills;
    property        Ticks:TAPIScore read FTicks;
    property        Victories:TAPIScore read FVictories;
    property        Worlds:TAPIWorlds read FWorlds;

    // Events
    property        OnClaim:TAPIObjEvent write setClaimEvent;
    property        OnFlip:TAPIObjEvent write setFlipEvent;
  end;

  // List to store the guilds
  TAPIGuilds        = specialize TFPGList<TAPIGuild>;
  // Dictionary to store the matchups identifing by the ID
  TAPIMatchups     = specialize TFPGMap<String, TAPIMatchup>;
  // Dictionary to store the names of each server given by ID
  TNameList         = specialize TFPGMap<Integer, String>;
  // To return all world id's as a list of integer
  TIDList           = specialize TFPGList<Integer>;

  { TDownloader }
  TDownloader     = class(TThread)
  private
    // This url will be writable for the main thread and synchronized before downloading with the internal url
    FChangedUrl   :String;
    // External requestable data structure. Will be synchronized with FResponse.
    FData         :TJSONData;
    // Delay until the next download will be started
    FDelay        :Integer;
    // Counter for the successful downloads
    FDownloads    :Integer;
    // Internal used response string. No access from the mainthread.
    FResponse     :String;
    // The last HTTP status code
    FStatus       :Longint;
    // Timestamp for the last getted response.
    FTime         :TDateTime;
    // The current url which will be requested
    FUrl          :String;
  protected
    // Execution method of the thread
    procedure     Execute;override;
    // Synchronize the external data with the response
    procedure     Sync;
  public
    // Constructor
    //// Take the URL of the data to be downloaded
    //// Take the delay between the downloads in seconds
    constructor   Create(Url:String;Delay:Integer);
    // Destructor
    destructor    Free;

    // Properties
    property      Data:TJSONData read FData;
    property      Downloads:Integer read FDownloads;
    property      Status:Integer read FStatus;
    property      Time:TDateTime read FTime;
    property      URL:String read FURL write FChangedUrl;
  end;

  { TRequestLoader }
  TRequestLoader  = class(TThread)
  private
    // List of the request URLs
    FRequests     :TStrings;
    // List of the responses
    FData         :TStrings;
    // Delay if no request is avaiable
    FDelay        :Integer;
    // Number of downloads
    FDownloads    :Integer;
    // Last download (Only writable from thread)
    FResponse     :String;
    // Last HTTP status (Only writable from thread)
    FStatus       :Longint;
    // Last requested url (Only writable from thread)
    FUrl          :String;

    // Extract the first response and return the parsed JSON object
    function      getData:TJSONData;
    // Append the last response to the response list
    procedure     SyncResponse;
    // Get the next requested URL
    procedure     SyncUrl;
  protected
    // Execution method
    procedure     Execute;override;
  public
    // Constructor
    //// Take the delay between the downloads in seconds
    constructor   Create(Delay:Integer);
    destructor    Destroy;override;

    // Return the number of responses
    function      Count:Integer;
    // Append a new url for requests
    procedure     request(Url:String);

    property      Data:TJSONData read getData;
    property      Downloads:Integer read FDownloads;
    property      Status:Integer read FStatus;
  end;

  // Loader structure
  TLoader         = record
    // The internal thread
    Thread        :TDownloader;
    // Will be the last download number to better synchronize.
    Last          :Integer;
  end;

  // Requester structure
  TRequester      = record
    // The internal thread
    Thread        :TRequestLoader;
    // Will be the last download number to better synchronize.
    Last          :Integer;
  end;

  { TWebAPI }
  TWebAPI         = class
    // This class can be used with active updates using the Update-procedure or
    // using the passiv updating system which will trigger if data is requested.
    // The passiv system will only parse required data on this way.
  private
    // Current server
    FServer       :Integer;
    // Current matchup ID
    FMatchup      :String;
    FCurrent      :TAPIMatchup;

    // Data storage from the api
    // Current game build.
    FBuild        :Integer;
    // Guild database
    FGuilds       :TAPIGuilds;
    // All current matches
    FMatches      :TAPIMatchups;
    // All objectives with global informations
    FObjectives   :TAPIObjectives;
    // The score of the current matchup
    FScore        :TAPIScore;
    // The map scores of the current matchup
    FScores       :Array[TWvWMapType] of TAPIScore;
    // The tick of the current matchup
    FTick         :TAPIScore;
    // The map ticks of the current matchup
    FTicks        :Array[TWvWMapType] of TAPIScore;
    // All world names listed by language and ID
    FWorlds       :Array[TLanguage] of TNameList;

    // Events
    FOnClaim      :TAPIObjEvent;
    FOnFlip       :TAPIObjEvent;

    // Downloader classes
    FBuildLoader  :TLoader;
    FGuildLoader  :TRequester;
    FMatchLoader  :TLoader;
    FWorldLoader  :Array[TLanguage] of TLoader;
    FObjLoader    :Array[TLanguage] of TLoader;

    // Getter
    function      getBuild:Integer;
    function      getCurrentMatch:TAPIMatchup;
    function      getGuildByID(ID:String):TAPIGuild;
    function      getMatch(ID:String):TAPIMatchup;
    function      getMatchCount:Integer;
    function      getObjective(ID:String):TAPIObjective;
    function      getScore:TAPIScore;
    function      getScores(Map:TWvWMapType):TAPIScore;
    function      getServerID(Name:String;Lang:TLanguage):Integer;
    function      getTick:TAPIScore;
    function      getTicks(Map:TWvWMapType):TAPIScore;
    function      getWorldCount(Lang:TLanguage):Integer;
    function      getWorldName(ID:Integer;Lang:TLanguage):String;

    // Setter
    procedure     setClaimEvent(Event:TAPIObjEvent);
    procedure     setFlipEvent(Event:TAPIObjEvent);
    procedure     setServer(ID:Integer);

    // Update routines which parse the thread data into the structures
    procedure     updateBuild;
    procedure     updateGuild;
    procedure     updateMatches;
    procedure     updateObjective(Lang:TLanguage);
    procedure     updateWorlds(Lang:TLanguage);
  public
    // Constructor and destructor
    constructor   Create;
    destructor    Destroy;override;

    // External update routine to force the active updates
    procedure     Update;

    // List some informations
    procedure     listMatchIDs(var IDs:TStrings);
    procedure     listStatus(var Status:TStrings);
    procedure     listWorldIDs(var IDs:TIDList;Lang:TLanguage);
    procedure     listWorldNames(var Names:TStrings;Lang:TLanguage);

    // Properties to read or change settings
    property      MatchID:String read FMatchup;
    property      Server:Integer read FServer write setServer;

    // Events
    property      OnClaimObjective:TAPIObjEvent write setClaimEvent;
    property      OnFlipObjective:TAPIObjEvent write setFlipEvent;

    // Properties to read the data. Every read will update the data if possible
    property      Build:Integer read getBuild;
    property      CurrentMatch:TAPIMatchup read getCurrentMatch;
    property      GuildByID[ID:String]:TAPIGuild read getGuildByID;
    property      Match[ID:String]:TAPIMatchup read getMatch;
    property      MatchCount:Integer read getMatchCount;
    property      Objective[ID:String]:TAPIObjective read getObjective;
    property      ServerID[Name:String;Lang:TLanguage]:Integer read getServerID;
    property      WorldCount[Lang:TLanguage]:Integer read getWorldCount;
    property      WorldName[ID:Integer;Lang:TLanguage]:String read getWorldName;
  end;


implementation

const
  // Server and protocol for the requests
  API_URL             = 'https://api.guildwars2.com';

  // If the download of the data fails this timeout will be used (in seconds)
  ERROR_TIMEOUT       = 15;

  // Minimum delay time in seconds
  MINIMUM_DELAY       = 5;


  // Conversion of json dictionaries with integer values
function convert2APIScore(const root:TJSONObject):TAPIScore;
begin
  if root <> Nil then begin
    Result[SC_RED]:=root.Get('red', 0);
    Result[SC_BLUE]:=root.Get('blue', 0);
    Result[SC_GREEN]:=root.Get('green', 0);
  end else begin
    Result[SC_RED]:=0;
    Result[SC_BLUE]:=0;
    Result[SC_GREEN]:=0;
  end;
end;

{ TWebAPI }
constructor TWebAPI.Create;
var lang    :TLanguage;
    mt      :TWvWMapType;
    sc      :TServerColor;
begin
  // Setting of current information
  FServer:=0;
  FMatchup:='';
  FCurrent:=Nil;

  // Default events
  FOnClaim:=Nil;
  FOnFlip:=Nil;

  // Initialize the score
  for mt in TWvWMapType do
    for sc in TServerColor do
      FScores[mt][sc]:=0;

  // Build
  FBuild:=0;

  // Guilds
  FGuilds:=TAPIGuilds.Create;

  // Create the matches v2
  FMatches:=TAPIMatchups.Create;
  FMatches.Sorted:=True;
  FMatches.OnKeyCompare:=@CompareStr;

  // Create the objectives v2
  FObjectives:=TAPIObjectives.Create;
  FObjectives.Sorted:=True;
  FObjectives.OnKeyCompare:=@CompareStr;

  // Name lists
  for lang in TLanguage do
    FWorlds[lang]:=TNameList.Create;

  // Update the build every hour
  with FBuildLoader do begin
    Thread:=TDownloader.Create(API_URL + '/v2/build', 3600);
    Last:=Thread.Downloads;
  end;

  // Create the guild list with 5s idle time
  with FGuildLoader do begin
    Thread:=TRequestLoader.Create(5);
    Last:=Thread.Downloads;
  end;

  // Create the matches 2 loader
  with FMatchLoader do begin
    Thread:=TDownloader.Create(API_URL + '/v2/wvw/matches?ids=all', 10);
    Last:=Thread.Downloads;
  end;

  // Update the world names every 10 minutes
  for lang in TLanguage do begin
    with FWorldLoader[lang] do begin
      Thread:=TDownloader.Create(API_URL + '/v2/worlds?ids=all&lang=' + langToStr(lang), 600);
      Last:=Thread.Downloads;
    end;

    with FObjLoader[lang] do begin
      Thread:=TDownloader.Create(API_URL + '/v2/wvw/objectives?ids=all&lang=' + langToStr(lang), 600);
      Last:=Thread.Downloads;
    end;
  end;

  Logger.log('Created web api.');
end;

// Destructor
destructor TWebAPI.Destroy;
var lang   :TLanguage;
    i      :Integer;
    s      :TStrings;
begin
  Logger.Log('Freeing webapi.');

  // Log the status informations
  s:=TStringList.Create;
  listStatus(s);
  i:=0;
  while i < s.Count do begin
    Logger.log('  ' + s[i]);
    Inc(i);
  end;
  s.Free;

  // All thread will free itself on terminate
  FBuildLoader.Thread.Terminate;
  FGuildLoader.Thread.Terminate;
  FMatchLoader.Thread.Terminate;

  for lang in TLanguage do begin
    FObjLoader[lang].Thread.Terminate;
    FWorldLoader[lang].Thread.Terminate;
  end;

  // Free the data
  FreeAndNil(FGuilds);

  for i:=0 to FMatches.Count - 1 do
    FMatches.Data[i].Free;
  FreeAndNil(FMatches);

  for i:=0 to FObjectives.Count - 1 do
    FObjectives.Data[i].Free;
  FreeAndNil(FObjectives);

  for lang in TLanguage do
    FreeAndNil(FWorlds[lang]);
end;

// Getter
// Returns the current build
function TWebAPI.getBuild:Integer;
begin
  // Check if an update is possible
  if FBuildLoader.Last <> FBuildLoader.Thread.Downloads then
    updateBuild;

  Result:=FBuild;
end;

// Returns the current match
function TWebAPI.getCurrentMatch:TAPIMatchup;
begin
  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  if (FCurrent = Nil) or (FCurrent.ID <> FMatchup) then
    FCurrent:=getMatch(FMatchup);

  Result:=FCurrent;
end;

// Returns a guild object given by ID
function TWebAPI.getGuildByID(ID:String):TAPIGuild;
var i    :Integer;
begin
  // Check if an update is possible
  if FGuildLoader.Last <> FGuildLoader.Thread.Downloads then
    updateGuild;

  // Search the guild
  Result:=Nil;
  i:=0;
  while i < FGuilds.Count do begin
    if ID = FGuilds[i].ID then
      Result:=FGuilds[i];

    Inc(i);
  end;

  // If not found request the guild
  if Result = Nil then
    FGuildLoader.Thread.request(API_URL + '/v1/guild_details.json?guild_id=' + ID);
end;

// Returns the match with the given ID
function TWebAPI.getMatch(ID:String):TAPIMatchup;
var i    :Integer;
begin
  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  // Search the matchup
  if FMatches.Find(ID, i) then
    Result:=FMatches[ID]
  else
    Result:=Nil;
end;

// Returns the number of loaded matches
function TWebAPI.getMatchCount:Integer;
begin
  // Check if a matchup is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check for updates
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  //Result:=FMatches.Count;
  Result:=FMatches.Count;
end;

// Returns the objectiv with the given ID
function TWebAPI.getObjective(ID:String):TAPIObjective;
var k    :Integer;
    lang :TLanguage;
begin
  // Chek if a matchup is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  // Check if an update is possible
  for lang in TLanguage do
    if FObjLoader[lang].Last <> FObjLoader[lang].Thread.Downloads then
      updateObjective(lang);

  // Search the objective
  if FObjectives.Find(ID, k) then
    Result:=FObjectives.Data[k]
  else
    Result:=Nil;
end;

// Returns the score of the matchup
function TWebAPI.getScore:TAPIScore;
begin
  // Check if a server is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  Result:=FScore;
end;

// Returns the score of the given map
function TWebAPI.getScores(Map:TWvWMapType):TAPIScore;
begin
  // Check if a server is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  Result:=FScores[Map];
end;

// Returns the server ID of the given server name
function TWebAPI.getServerID(Name:String;Lang:TLanguage):Integer;
var i    :Integer;
begin
  Result:=0;
  i:=0;
  while i < FWorlds[Lang].Count do begin
    if FWorlds[Lang].Data[i] = Name then
      Result:=FWorlds[Lang].Keys[i];
    Inc(i);
  end;
end;

// Returns the current tick
function TWebAPI.getTick:TAPIScore;
begin
  // Check if a server is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  Result:=FTick;
end;

// Returns the current tick of the given map
function TWebAPI.getTicks(Map:TWvWMapType):TAPIScore;
begin
  // Check if a server is chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  Result:=FTicks[Map];
end;

// Returns the number of loaded worlds
function TWebAPI.getWorldCount(Lang:TLanguage):Integer;
begin
  if FWorldLoader[Lang].Last <> FWorldLoader[Lang].Thread.Downloads then
    updateWorlds(Lang);

  Result:=FWorlds[Lang].Count;
end;

// Returns the name of the world given by ID and language
function TWebAPI.getWorldName(ID:Integer;Lang:TLanguage):String;
var i    :Integer;
begin
  // Check if an update is possible
  if FWorldLoader[Lang].Last <> FWorldLoader[Lang].Thread.Downloads then
    updateWorlds(Lang);

  // Search the world
  Result:='';
  i:=0;
  while i < FWorlds[lang].Count do begin
    if FWorlds[Lang].Keys[i] = ID then
      Result:=FWorlds[Lang].Data[i];

    Inc(i);
  end;
end;

// Setter
procedure TWebAPI.setClaimEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnClaim:=Event;

  // Update the event in every matchup
  for i:=0 to FMatches.Count - 1 do
    FMatches.Data[i].OnClaim:=FOnClaim;
end;

procedure TWebAPI.setFlipEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnFlip:=Event;

  // Update the event in every matchup
  for i:=0 to FMatches.Count - 1 do
    FMatches.Data[i].OnFlip:=FOnFlip;
end;

procedure TWebAPI.setServer(ID:Integer);
var i     :Integer;
    m     :TAPIMatchup;
begin
  m:=getCurrentMatch;

  // Only update if required
  if (FServer <> ID) or (FMatchup = '') or ((m <> Nil) and (not m.isIn(FServer))) then begin
    FServer:=ID;

    // Search the current matchup
    i:=0;
    while i < FMatches.Count do begin
      if FMatches.Data[i].isIn(FServer) then
        FMatchup:=FMatches.Keys[i];
      Inc(i);
    end;
  end;
end;

// Listening methods
// List all match IDs
procedure TWebAPI.listMatchIDs(var IDs:TStrings);
var i     :Integer;
begin
  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  // Append the list with the IDs
  IDs.BeginUpdate;
  IDs.Clear;

  i:=0;
  while i < FMatches.Count do begin
    IDs.Append(FMatches.Keys[i]);
    Inc(i);
  end;

  IDs.EndUpdate;
end;

// Returns some status informations
procedure TWebAPI.listStatus(var Status:TStrings);
var lang  :TLanguage;
begin
  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  // Check if an update is possible
  for lang in TLanguage do
    if FWorldLoader[lang].Last <> FWorldLoader[lang].Thread.Downloads then
      updateWorlds(lang);

  // Append the informations
  Status.BeginUpdate;
  Status.Clear;
  Status.Append('Server:             ' + IntToStr(FServer));
  Status.Append('Match ID:           ' + FMatchup);
  Status.Append('Number of Matches:  ' + IntToStr(FMatches.Count));
  Status.Append('Updates of Matches: ' + IntToStr(FMatchLoader.Last));
  Status.Append('Number of Worlds:   ' + IntToStr(FWorlds[L_ENGLISH].Count));
  Status.Append('Updates of Worlds:  ' + IntToStr(FWorldLoader[L_ENGLISH].Last));
  Status.EndUpdate;
end;

// Returns the world IDs with given language
procedure TWebAPI.listWorldIDs(var IDs:TIDList;Lang:TLanguage);
var i     :Integer;
begin
  // Check if an update is possible
  if FWorldLoader[Lang].Last <> FWorldLoader[Lang].Thread.Downloads then
    updateWorlds(Lang);

  // Fill the list
  IDs.Clear;

  i:=0;
  while i < FWorlds[Lang].Count do begin
    IDs.Add(FWorlds[Lang].Keys[i]);
    Inc(i);
  end;
end;

// Returns all world names with a given language
procedure TWebAPI.listWorldNames(var Names:TStrings;Lang:TLanguage);
var i     :Integer;
begin
  // Check if an update is possible
  if FWorldLoader[Lang].Last <> FWorldLoader[Lang].Thread.Downloads then
    updateWorlds(Lang);

  // Fill the list
  Names.BeginUpdate;
  Names.Clear;

  i:=0;
  while i < FWorlds[Lang].Count do begin
    Names.Append(FWorlds[lang].Data[i]);
    Inc(i);
  end;

  Names.EndUpdate;
end;

// Update the entire data active
procedure TWebAPI.Update;
var lang  :TLanguage;
begin
  // Check if an update is possible
  if FBuildLoader.Last <> FBuildLoader.Thread.Downloads then
    updateBuild;

  // Check if an update is possible
  if FGuildLoader.Last <> FGuildLoader.Thread.Downloads then
    updateGuild;

  // Check if a server and matchup are chosen
  if (FServer <> 0) and (FMatchup = '') then
    setServer(FServer);

  // Check if an update is possible
  if FMatchLoader.Last <> FMatchLoader.Thread.Downloads then
    updateMatches;

  // Check if an update is possible
  for lang in TLanguage do begin
    if FWorldLoader[lang].Last <> FWorldLoader[lang].Thread.Downloads then
      updateWorlds(lang);

    if FObjLoader[lang].Last <> FObjLoader[lang].Thread.Downloads then
      updateObjective(lang);
  end;
end;

// Update the build
procedure TWebAPI.updateBuild;
begin
  with FBuildLoader.Thread do
    if (Data <> Nil) and (Data.JSONType = jtObject) then
      FBuild:=(Data as TJSONObject).Get('id', FBuild);

  FBuildLoader.Last:=FBuildLoader.Thread.Downloads;
end;

// Update the guilds
procedure TWebAPI.updateGuild;
var json  :TJSONData;
    guild :TAPIGuild;
    dub   :Boolean;
    i     :Integer;
begin
  while FGuildLoader.Thread.Count > 0 do begin
    json:=FGuildLoader.Thread.Data;

    // Check if it is an object
    if (json <> Nil) and (json.JSONType = jtObject) then begin
      // Create the guild
      guild:=TAPIGuild.Create(json as TJSONObject);

      // Check for duplicates
      dub:=False;
      i:=0;
      while i < FGuilds.Count do begin
        if FGuilds[i].ID = guild.ID then
          dub:=True;
        Inc(i);
      end;

      // If no duplicate, add to list
      if not dub then
        FGuilds.Add(guild)
      else
        // Else free it
        FreeAndNil(guild);
    end;

    // Free the json data
    json.Free;
  end;

  // Update the counter
  FGuildLoader.Last:=FGuildLoader.Thread.Downloads;
end;

// Updates the matchup list from APIv2 data
procedure TWebAPI.updateMatches;
var i, k  :Integer;
    id    :String;
    root  :TJSONArray;
    tmp   :TAPIMatchup;
begin
  // Check if the loader is valid
  if (FMatchLoader.Thread.Data = Nil) or (FMatchLoader.Thread.Data.JSONType <> jtArray) then
    Exit;

  // Get the data
  root:=FMatchLoader.Thread.Data as TJSONArray;

  // Iterate over the data
  for i:=0 to root.Count - 1 do begin
    // Validate the elements
    if root.Types[i] <> jtObject then
      Continue;

    // And the ID
    id:=root.Objects[i].Get('id', '');
    if id = '' then
      Continue;

    // If the id was found
    if FMatches.Find(id, k) then
      // Update the match
      FMatches.Data[k].load(root.Objects[i])
    else begin
      // otherwise add a new match
      tmp:=TAPIMatchup.Create(root.Objects[i]);
      tmp.OnClaim:=FOnClaim;
      tmp.OnFlip:=FOnFlip;
      FMatches[id]:=tmp;
    end;
  end;
end;

// Update the objective
procedure TWebAPI.updateObjective(Lang:TLanguage);
var root  :TJSONArray;
    i, k  :Integer;
    id    :String;
begin
  if (FObjLoader[lang].Thread.Data = Nil) or (FObjLoader[lang].Thread.Data.JSONType <> jtArray) then
    Exit;

  // Iterate over the downlaoded data
  root:=FObjLoader[lang].Thread.Data as TJSONArray;
  for i:=0 to root.Count - 1 do begin
    // Check the type
    if root.Types[i] <> jtObject then
      Continue;

    // Check the id
    id:=root.Objects[i].Get('id', '');
    if id = '' then
      Continue;

    // Search the objective or add new
    if not FObjectives.Find(id, k) then
      FObjectives[id]:=TAPIObjective.Create(id);

    // Update the data
    if (0 <= k) and (k < FObjectives.Count) then
      FObjectives.Data[k].load(root.Objects[i], lang);
  end;

  // Update the last download number
  FObjLoader[Lang].Last:=FObjLoader[Lang].Thread.Downloads;
end;

// Update the world list with the given language
procedure TWebAPI.updateWorlds(Lang:TLanguage);
var i, id :Integer;
    name  :String;
    root  :TJSONArray;
begin
  with FWorldLoader[lang].Thread do
    if (Data <> Nil) and (Data.JSONType = jtArray) then begin
      root:=Data as TJSONArray;

      // Iterate over the array
      i:=0;
      while i < root.Count do begin
        if root[i].JSONType = jtObject then begin
          id:=(root[i] as TJSONObject).Get('id', 0);
          name:=(root[i] as TJSONObject).Get('name', '');

          if (id <> 0) and (name <> '') then begin
            if FWorlds[lang].IndexOf(id) < 0 then
              FWorlds[lang].Add(id);

            FWorlds[lang][id]:=name;
          end;
        end;
        Inc(i);
      end;

    end;

  // Update the last download number
  FWorldLoader[Lang].Last:=FWorldLoader[Lang].Thread.Downloads;
end;

{ TAPIGuild }
constructor TAPIGuild.Create(Json:TJSONObject);
begin
  FID:=Json.Get('guild_id', '');
  FName:=Json.Get('guild_name', '');
  FTag:=Json.Get('tag', '');
end;

{ TAPIMObjective }
constructor TAPIMObjective.Create(const root:TJSONObject; const MatchID:String);
begin
  FMatchID:=MatchID;
  FID:=root.Get('id', '');
  FType:=strToObjType(root.Get('type', ''));

  FOnClaim:=Nil;
  FOnFlip:=Nil;

  loadOffset;

  load(root);
end;

// Setter
procedure TAPIMObjective.setClaimed(By:String; At:TDateTime);
begin
  if (By <> FClaimedBy) and (By <> '') then begin
    FClaimedBy:=By;
    FClaimedAt:=At;

    if Assigned(FOnClaim) then
      FOnClaim(Self);
  end else begin
    FClaimedAt:=At;
    FClaimedBy:=By;
  end;
end;

procedure TAPIMObjective.setOwner(Value:TObjectiveColor);
begin
  if (Value <> FOwner) and (Value <> OC_NONE) then begin
    FOwner:=Value;

    if Assigned(FOnFlip) then
      FOnFlip(Self);
  end else
    FOwner:=Value;
end;

procedure TAPIMObjective.load(const root:TJSONObject);
begin
  FLastFlipped:=strToDateTime(root.Get('last_flipped', ''));
  setOwner(strToObjColor(root.Get('owner', '')));
  setClaimed(root.Get('claimed_by', ''), strToDateTime(root.Get('claimed_at', '')));
end;

procedure TAPIMObjective.loadOffset;
var mapID, objID  :Integer;
begin
  FOffset:=0;

  // Apply the offset based on the type
  case FType of
    AOT_CAMP:   FOffset:=2;
    AOT_CASTLE: FOffset:=4;
    AOT_KEEP:   FOffset:=5;
    AOT_TOWER:  FOffset:=8;
    AOT_NIL:    Exit;
  end;

  // Return if found
  if FOffset > 0 then
    Exit;

  // Try to read the objective ID
  if SScanf(FID, '%d-%d', [@mapID, @objID]) <> 2 then
    Exit;

  // Apply the offset based on the objective IDs
  case objID of
    63, 70, 75: FOffset:=0;
    64, 69, 74: FOffset:=1;
    66, 67, 72: FOffset:=3;
    65, 68, 73: FOffset:=6;
    62, 71, 76: FOffset:=7;
  end;
end;

{ TAPIObjective }
constructor TAPIObjective.Create(const ID:String);
var lang    :TLanguage;
    i       :Integer;
    mid     :Integer;
begin
  // Get the ID
  FID:=ID;
  FOID:=0;

  // Coordinates
  for i:=0 to 2 do
    FCoord[i]:=0.0;

  // Get the position
  if SScanf(FID, '%d-%d', [@mid, @FOID]) = 2 then
    for i:=Low(ObjectiveData) to High(ObjectiveData) do
      if FOID = ObjectiveData[i].ID then
        FCoord:=ObjectiveData[i].Position;

  // Set the names
  for lang in TLanguage do
    FNames[lang]:='';

  // Get the sector ID
  FSectorID:=0;
  // Get the type
  FType:=AOT_NIL;
  // Get the map type
  FMapType:=MT_NONE;
  FMapID:=0;
  // Marker URL
  FMarker:='';
end;

destructor TAPIObjective.Destroy;
begin
  inherited Destroy;
end;

function TAPIObjective.getName(Lang:TLanguage):String;
var tmp  :String;
begin
  case FOID of
    2, 23: tmp:='BLUE';
    1, 37: tmp:='RED';
    3, 46: tmp:='GREEN';
  else
    Result:=FNames[Lang];
    Exit;
  end;

  case Lang of
    L_ENGLISH: Result:=Format('<%s> %s', [tmp, FNames[Lang]]);
    L_GERMAN: Result:=Format('%s <%s>', [FNames[Lang], tmp]);
    L_FRENCH: Result:=Format('<%s> %s', [tmp, FNames[Lang]]);
    L_SPANISH: Result:=Format('<%s> %s', [tmp, FNames[Lang]]);
  end;
end;

// Returns the 2D coordinate of an objective downscaled to [0; 1]
function TAPIObjective.get2DCoord:TVector;
begin
  Result[0]:=0.0;
  Result[1]:=0.0;
  Result[2]:=0.0;

  // Map ID dependent scaling
  case FMapID of
    38: with EternalBattleground do begin
      Result[0]:=(FCoord[0] - Left) / (Right - Left);
      Result[2]:=(FCoord[2] - Top) / (Bottom - Top);
    end;
    94.. 96: with AlpineBorderlands do begin
      // Eternal
      Result[0]:=(FCoord[0] - Left) / (Right - Left);
      Result[2]:=(FCoord[2] - Top) / (Bottom - Top);
    end;
    1102, 1099, 1143: with DesertBorderlands do begin
      // Eternal
      Result[0]:=(FCoord[0] - Left) / (Right - Left);
      Result[2]:=(FCoord[2] - Top) / (Bottom - Top);
    end;
  end;
end;

// Update the objective with data from the objectives endpoint
procedure TAPIObjective.load(const root:TJSONObject;const lang:TLanguage);
begin
  if root.Get('id', '') <> FID then
    Exit;

  // Set the names
  FNames[lang]:=root.Get('name', '');
  // Get the sector ID
  FSectorID:=root.Get('sector_id', 0);
  // Get the type
  FType:=strToObjType(root.Get('type', ''));
  // Get the map type
  FMapType:=strToMapType(root.Get('map_type', ''));
  // Get map ID
  FMapID:=root.Get('map_id', 0);
  // Get the marker (icon)
  FMarker:=root.Get('marker', '');
end;

{ TAPIMatchup }
constructor TAPIMatchup.Create(const root:TJSONObject);
begin
  FID:=root.Get('id', '');
  FOnClaim:=Nil;
  FOnFlip:=Nil;

  // Create the maps database
  FMaps:=TAPIMaps.Create;
  FMaps.Sorted:=True;
  FMaps.OnKeyCompare:=@CompareInt;

  // Load the data
  load(root);
end;

destructor TAPIMatchup.Destroy;
var i      :Integer;
begin
  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].Free;
  FreeAndNil(FMaps);

  inherited Destroy;
end;

// Returns if the server is playing in the matchup
function TAPIMatchup.isIn(Server:Integer):Boolean;
var sc   :TServerColor;
    k    :Integer;
begin
  Result:=False;
  for sc in TServerColor do
    for k:=Low(FWorlds[sc]) to High(FWorlds[sc]) do
      if FWorlds[sc][k] = Server then
        Result:=True;
end;

// Getter
function TAPIMatchup.getMaps(Map:TWvWMapType):TAPIMap;
var i    :Integer;
begin
  Result:=Nil;
  for i:=0 to FMaps.Count - 1 do
    if FMaps.Data[i].MapType = Map then
      Result:=FMaps.Data[i];
end;

function TAPIMatchup.getMapByID(MapID:Integer):TAPIMap;
var k    :Integer;
begin
  if FMaps.Find(MapID, k) then
    Result:=FMaps.Data[k]
  else
    Result:=Nil;
end;

// Setter
procedure TAPIMatchup.setClaimEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnClaim:=Event;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].OnClaim:=FOnClaim;
end;

procedure TAPIMatchup.setFlipEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnFlip:=Event;

  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].OnFlip:=FOnFlip;
end;

// Load the json data from the api into the structure
procedure TAPIMatchup.load(const root:TJSONObject);
  procedure extract_worlds(const arr:TJSONArray; sc:TServerColor);
  var k     :Integer;
  begin
    for k:=0 to MAX_WORLDS do
      if (arr <> Nil) and (k < arr.Count) and (arr.Types[k] = jtNumber) then
        FWorlds[sc][k]:=arr.Integers[k]
      else
        FWorlds[sc][k]:=0;
  end;

var arr   :TJSONArray;
    obj   :TJSONObject;
    i     :Integer;
    sc    :TServerColor;
begin
  // Load the start and end time
  FStartTime:=strToDateTime(root.Get('start_time', ''));
  FEndTime:=strToDateTime(root.Get('end_time', ''));

  // Update the worlds
  obj:=root.Find('all_worlds', jtObject) as TJSONObject;
  if obj <> Nil then begin
    extract_worlds(obj.Find('red', jtArray) as TJSONArray, SC_RED);
    extract_worlds(obj.Find('blue', jtArray) as TJSONArray, SC_BLUE);
    extract_worlds(obj.Find('green', jtArray) as TJSONArray, SC_GREEN);
  end;

  // Load the scores, worlds, deaths and kills
  FScores:=convert2APIScore(root.Find('scores', jtObject) as TJSONObject);
  FDeaths:=convert2APIScore(root.Find('deaths', jtObject) as TJSONObject);
  FKills:=convert2APIScore(root.Find('kills', jtObject) as TJSONObject);
  FVictories:=convert2APIScore(root.Find('victory_points', jtObject) as TJSONObject);

  FTicks[SC_RED]:=0;
  FTicks[SC_BLUE]:=0;
  FTicks[SC_GREEN]:=0;

  // Try to load the maps
  arr:=root.Find('maps', jtArray) as TJSONArray;
  if arr <> Nil then
    loadMaps(arr);

  for i:=0 to FMaps.Count - 1 do
    for sc in TServerColor do
      Inc(FTicks[sc], FMaps.Data[i].Ticks[sc]);
end;

procedure TAPIMatchup.loadMaps(const root:TJSONArray);
var i, k  :Integer;
    mID   :Integer;
    tmp   :TAPIMap;
begin
  for i:=0 to FMaps.Count - 1 do
    FMaps.Data[i].Active:=False;

  // Update the maps
  for i:=0 to root.Count - 1 do begin
    // Check the JSON type
    if root.Types[i] <> jtObject then
      Continue;

    // Check the id
    mID:=root.Objects[i].Get('id', 0);
    if mID = 0 then
      Continue;

    // Update the map or add new map
    if FMaps.Find(mID, k) then
      FMaps.Data[k].load(root.Objects[i])
    else begin
      tmp:=TAPIMap.Create(root.Objects[i], FID);
      tmp.OnClaim:=FOnClaim;
      tmp.OnFlip:=FOnFlip;
      FMaps[mID]:=tmp;
    end;
  end;

  // Remove non-active maps
  for i:=FMaps.Count - 1 downto 0 do
    if not FMaps.Data[i].Active then begin
      FMaps.Data[i].Free;
      FMaps.Delete(i);
    end;
end;

{ TAPIMap }
constructor TAPIMap.Create(const root:TJSONObject; const MatchID:String);
begin
  FMatchID:=MatchID;
  FID:=root.Get('id', 0);

  FOnFlip:=Nil;
  FOnClaim:=Nil;

  // Create the objectives database
  FObjectives:=TAPIMObjectives.Create;
  FObjectives.Sorted:=True;
  FObjectives.OnKeyCompare:=@CompareStr;

  load(root);
end;

destructor TAPIMap.Destroy;
var i      :Integer;
begin
  SetLength(FBonuses, 0);

  for i:=0 to FObjectives.Count - 1 do
    FObjectives.Data[i].Free;
  FreeAndNil(FObjectives);

  inherited Destroy;
end;

function TAPIMap.getBonusOwner(Bonus:String):TObjectiveColor;
var i    :Integer;
begin
  Result:=OC_NONE;
  for i:=Low(FBonuses) to High(FBonuses) do
    if FBonuses[i].Bonus = Bonus then
      Result:=FBonuses[i].Owner;
end;

// Setter
procedure TAPIMap.setClaimEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnClaim:=Event;

  for i:=0 to FObjectives.Count - 1 do
    FObjectives.Data[i].OnClaim:=FOnClaim;
end;

procedure TAPIMap.setFlipEvent(Event:TAPIObjEvent);
var i     :Integer;
begin
  FOnFlip:=Event;

  for i:=0 to FObjectives.Count - 1 do
    FObjectives.Data[i].OnFlip:=FOnFlip;
end;

procedure TAPIMap.load(const root:TJSONObject);
var arr   :TJSONArray;
begin
  // Update the tick
  FTicks[SC_RED]:=0;
  FTicks[SC_BLUE]:=0;
  FTicks[SC_GREEN]:=0;

  // Set to active map
  FActive:=True;

  FType:=strToMapType(root.Get('type', ''));
  // Read the scores
  FScores:=convert2APIScore(root.Find('scores', jtObject) as TJSONObject);
  FKills:=convert2APIScore(root.Find('kills', jtObject) as TJSONObject);
  FDeaths:=convert2APIScore(root.Find('deaths', jtObject) as TJSONObject);

  arr:=root.Find('objectives', jtArray) as TJSONArray;
  if arr <> Nil then
    loadObjectives(arr);

  // Read the bonuses
  arr:=root.Find('bonuses', jtArray) as TJSONArray;
  if arr = Nil then
    loadBonuses(arr);
end;

procedure TAPIMap.loadBonuses(const root:TJSONArray);
var i     :Integer;
begin
  // Read the bonuses
  SetLength(FBonuses, root.Count);
  for i:=0 to root.Count - 1 do
    if root[i].JSONType = jtObject then
      with root.Objects[i] do begin
        FBonuses[i].Bonus:=Get('type', '');
        FBonuses[i].Owner:=strToObjColor(Get('owner', ''));
      end;
end;

procedure TAPIMap.loadObjectives(const root:TJSONArray);
var i, k  :Integer;
    mID   :String;
    tmp   :TAPIMObjective;
begin
  // Update the maps
  for i:=0 to root.Count - 1 do begin
    // Check the JSON type
    if root.Types[i] <> jtObject then
      Continue;

    // Check the id
    mID:=root.Objects[i].Get('id', '');
    if mID = '' then
      Continue;

    // Update the map or add new map
    if FObjectives.Find(mID, k) then
      FObjectives.Data[k].load(root.Objects[i])
    else begin
      tmp:=TAPIMObjective.Create(root.Objects[i], FMatchID);
      tmp.OnClaim:=FOnClaim;
      tmp.OnFlip:=FOnFlip;
      FObjectives[mID]:=tmp;
    end;

    with FObjectives[mID] do begin
      case Owner of
        OC_GREEN: Inc(FTicks[SC_GREEN], objTypeToPoints(ObjectiveType));
        OC_BLUE: Inc(FTicks[SC_BLUE], objTypeToPoints(ObjectiveType));
        OC_RED: Inc(FTicks[SC_RED], objTypeToPoints(ObjectiveType));
      end;
    end;
  end;
end;

{ TDownloader }
constructor TDownloader.Create(Url:String;Delay:Integer);
begin
  FChangedUrl:=Url;
  FData:=Nil;

  if Delay < MINIMUM_DELAY then
    FDelay:=MINIMUM_DELAY * 1000
  else
    FDelay:=Delay * 1000;

  FDownloads:=0;
  FResponse:='';
  FStatus:=0;
  FUrl:=Url;

  // All threads will be freed on terminate
  FreeOnTerminate:=True;

  // Runs the thread after creating
  inherited Create(false);
end;

// Destructor
destructor TDownloader.Free;
begin
  // Just free the external data
  FreeAndNil(FData);
end;

// Procedure to synchronize the internal data with the external
procedure TDownloader.Sync;
begin
  // The external data have to be json
  with TJSONParser.Create(FResponse) do
    try
      // Free the current data
      if FData <> Nil then
        FreeAndNil(FData);

      // Parse the json
      FData:=Parse;

      // Refresh the timestamp
      FTime:=Now;
    finally
      // Free the parser
      Free;

      // Increment the download counter
      Inc(FDownloads);
    end;
end;

// Procedure that loads the data from the url
procedure TDownloader.Execute;
begin
  // Infinite loop
  while not Terminated do begin
    if FUrl <> FChangedUrl then
      FUrl:=FChangedUrl;

    if FUrl <> '' then
      try
        // Retrieve the data from the url
        FResponse:=retrieve(FUrl);
        // Store the last status code
        FStatus:=defaultInternet.lastHTTPResultCode;

        // Only synchronize if the status is OK
        if FStatus = 200 then
          Synchronize(@Sync);
      except
        // On exception use the error timeout and try to download again
        Sleep(ERROR_TIMEOUT * 1000);
        Continue;
      end;

    // Otherwise sleep for the delay time
    Sleep(FDelay);
  end;
end;

{ TRequestLoader }
constructor TRequestLoader.Create(Delay:Integer);
begin
  FRequests:=TStringList.Create;
  FData:=TStringList.Create;

  if Delay < MINIMUM_DELAY then
    FDelay:=MINIMUM_DELAY * 1000
  else
    FDelay:=Delay * 1000;

  FDownloads:=0;
  FResponse:='';
  FStatus:=0;
  FUrl:='';

  // All threads will be freed on terminate
  FreeOnTerminate:=True;

  // Runs the thread after creating
  inherited Create(False);
end;

// Destructor
destructor TRequestLoader.Destroy;
begin
  FreeAndNil(FRequests);
  // Just free the external data
  FreeAndNil(FData);
end;

// Return the number of responses
function TRequestLoader.Count:Integer;
begin
  Result:=FData.Count;
end;

function TRequestLoader.getData:TJSONData;
var s    :String;
begin
  if FData.Count > 0 then begin
    s:=FData[0];
    FData.Delete(0);
  end else
    s:='';

  // The external data have to be json
  with TJSONParser.Create(s) do
    try
      // Parse the json
      Result:=Parse;
    finally
      // Free the parser
      Free;
    end;
end;

procedure TRequestLoader.request(Url:String);
begin
  // Remove duplicates
  if FRequests.IndexOf(Url) < 0 then
    FRequests.Append(Url);
end;

procedure TRequestLoader.SyncUrl;
begin
  if FRequests.Count > 0 then begin
    FUrl:=FRequests[0];
    FRequests.Delete(0);
  end else
    FUrl:='';
end;

// Procedure to synchronize the internal data with the external
procedure TRequestLoader.SyncResponse;
begin
  // Append the non-empty response to the queue
  if FResponse <> '' then
    FData.Append(FResponse);
  // Increment the download counter
  Inc(FDownloads);
end;

// Procedure that loads the data from the url
procedure TRequestLoader.Execute;
begin
  // Infinite loop
  while not Terminated do begin
    // Get the next requested url
    Synchronize(@SyncUrl);

    if FUrl <> '' then
      try
        // Retrieve the data from the url
        FResponse:=retrieve(FUrl);
        // Store the last status code
        FStatus:=defaultInternet.lastHTTPResultCode;

        // Only synchronize if the status is OK
        if FStatus = 200 then
          Synchronize(@SyncResponse);
      except
        // On exception use the error timeout and try to download again
        FStatus:=980;
        Continue;
      end
    else begin
      FStatus:=999;
      // Otherwise sleep for the delay time
      Sleep(FDelay);
    end;
  end;
end;

end.

