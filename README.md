#GuildWars2 Battle HUD

##Features

**Compass:** Offeres a compass based on the Mumble API of GuildWars2

**2D Maps:** All invulnerability timers of WvW objects are displayed on 2D maps

**Timer:** All invulnerability timers of WvW objects lying in the field of view

**Reminder:** Configurable timer for reminding on something

**Notification:** Notify WvW objects which owner has changed

**Statistic:** Shows some matchup specific statistics in a small form

**TeamSpeak 3 Viewer:** View who is speaking.

**Help System:** If activated you can see hints of mostly all buttons with short explanations

**Multilingual:** Supports all languages of the GuildWars2 API (English, German, French and Spanish)


##Requirement
- GuildWars2 client with "Windowed Fullscreen" resolution
- All files of the archive have to be in the same directory.


##Using

**IDE:** [Lazarus 1.4.0](http://www.lazarus.freepascal.org)

**Compiler:** fpc 2.6.2

**API:** [*GuildWars 2 API*](http://wiki.guildwars2.com/wiki/API:Main), [*Mumble*](http://wiki.guildwars2.com/wiki/Mumble) and *ClientQuery Plugin*

**Libaries:** [*Internet Tools*](http://wiki.lazarus.freepascal.org/Internet_Tools) and [*lNet*](http://wiki.lazarus.freepascal.org/lNet)


##[GuildWars 2 API Terms of Use](http://wiki.guildwars2.com/wiki/API:Main#Guild_Wars_2_API_Terms_of_Use)
These APIs are wholly owned by ArenaNet, LLC ("ArenaNet"). Any use of the APIs must comply with the [Website Terms of Use](https://www.guildwars2.com/en/legal/website-terms-of-use/) and [Content Terms of Use](https://www.guildwars2.com/en/legal/guild-wars-2-content-terms-of-use/), however you may use the APIs to make commercial products so long as they are otherwise compliant and do not compete with ArenaNet. ArenaNet may revoke your right to use the APIs at any time. In addition, ArenaNet may create and/or amend any terms or conditions applicable to the APIs or their use at any time and from time to time. You understand and agree that ArenaNet is in the process of developing a full license agreement for these APIs and ArenaNet will publish it when it is complete. Your continued use of the APIs constitutes acceptance of the full license agreement and any related terms or conditions when they are posted.